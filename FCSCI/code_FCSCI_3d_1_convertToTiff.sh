analysisName="FCSCI_3Dczi_splitChannels"
mainInDir=/home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI
mainTmpDir=/home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI
mainOutDir=/home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI
env='#!/bin/bash'

cd ${mainInDir}


# datasets=("200205_FCSCI" "200206_FCSCI_SC20c1" "200215_FCSCI_sc18c7" "200216_FCSCI_sc20" "200223_FCSCI" "200229_FCSCI" "200230_FCSCI" "200315_FCSCI")
datasets=("201007_FCSCI" )
# datasetName=200215_FCSCI_sc18c7
# cond=sc18c7_Dox24h_3d
# file=/home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/200215_FCSCI_sc18c7/data/sc18c7_Dox24h_3d/200215_FCSCI_sc18c7_Dox24h_3d_pos2.czi
# tmpDir=${mainTmpDir}/${datasetName}/ilastik/$cond
# outName=$(basename $file .czi)

# process all files
for datasetName in "${datasets[@]}"; do
    echo $datasetName
    analysisName=$datasetName

    mkdir ${mainTmpDir}/${datasetName}/ilastik
    
    for condPath in ${datasetName}/data/*3d; do 
        cond=$(basename $condPath)
        echo $cond
        tmpDir=${mainTmpDir}/${datasetName}/ilastik/$cond

        rm -r $tmpDir
        mkdir -p ${tmpDir}/std
        mkdir -p ${tmpDir}/log

        files=$(ls ${mainTmpDir}/${datasetName}/data/$cond/*.czi 2> /dev/null | wc -l)
        if [ "$files" != "0" ]
            then 
                for file in ${mainTmpDir}/${datasetName}/data/$cond/*.czi; do
                    echo $file
                    outName=$(basename $file .czi)
                    
                    job=SplitChannels_Pol2Xist_${outName}
                    if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                        if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                        jobName=${job}...${outName}...${analysisName}
                        jobSplitChannels_Pol2Xist=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 15 \
                            --wrap="${env}
                                date
                                mkdir ${tmpDir}/tiff_Pol2Xist
                                source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_IA
                                python ~/programs/ImageAnalysis/czi_multiChannel_splitChannels.py czi-subset-channels \
                                    -c 1,2 --out_format tiff \
                                    -i $file \
                                    -o ${tmpDir}/tiff_Pol2Xist/${outName}_Pol2Xist
                                    
                                if [ -s ${tmpDir}/tiff_Pol2Xist/${outName}_Pol2Xist.tiff ] ; then 
                                    echo ${job} done! > ${tmpDir}/log/${job}.done
                                else exit 1; fi; date"`
                        jobSplitChannels_Pol2Xist_id=`echo :${jobSplitChannels_Pol2Xist##* }`; jobSplitChannels_Pol2Xist_name=${jobName}
                        if [ -z ${job_all_id} ]; then job_all_id=${jobSplitChannels_Pol2Xist_id}; else job_all_id=${job_all_id}${jobSplitChannels_Pol2Xist_id}; fi    ### add job id to a variable listing all job ids
                        sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobSplitChannels_Pol2Xist_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobSplitChannels_Pol2Xist_id#:}"
                    fi

                    job=SplitChannels_Xist_${outName}
                    if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                        if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                        jobName=${job}...${outName}...${analysisName}
                        jobSplitChannels_Xist=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 15 \
                            --wrap="${env}
                                date
                                mkdir ${tmpDir}/tiff_Xist
                                source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_IA
                                python ~/programs/ImageAnalysis/czi_multiChannel_splitChannels.py czi-subset-channels \
                                    -c 2 --out_format tiff \
                                    -i $file \
                                    -o ${tmpDir}/tiff_Xist/${outName}_Xist
                                    
                                if [ -s ${tmpDir}/tiff_Xist/${outName}_Xist.tiff ] ; then 
                                    echo ${job} done! > ${tmpDir}/log/${job}.done
                                else exit 1; fi; date"`
                        jobSplitChannels_Xist_id=`echo :${jobSplitChannels_Xist##* }`; jobSplitChannels_Xist_name=${jobName}
                        if [ -z ${job_all_id} ]; then job_all_id=${jobSplitChannels_Xist_id}; else job_all_id=${job_all_id}${jobSplitChannels_Xist_id}; fi    ### add job id to a variable listing all job ids
                        sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobSplitChannels_Xist_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobSplitChannels_Xist_id#:}"
                    fi
                done
        fi

### process lsm files
        files=$(ls ${mainTmpDir}/${datasetName}/data/$cond/*.lsm 2> /dev/null | wc -l)
        if [ "$files" != "0" ]
            then 
                for file in ${mainTmpDir}/${datasetName}/data/$cond/*.lsm; do
                    echo $file
                    outName=$(basename $file .lsm)

                    job=SplitChannels_Pol2Xist_${outName}
                    if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                        if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                        jobName=${job}...${outName}...${analysisName}
                        jobSplitChannels_Pol2Xist=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 15 \
                            --wrap="${env}
                                date
                                mkdir ${tmpDir}/tiff_Pol2Xist
                                source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py2.7_IA
                                python ~/programs/ImageAnalysis/lsm_multiChannel_splitChannels.py lsm-subset-channels \
                                    -c 1,2 --out_format tiff \
                                    -i $file \
                                    -o ${tmpDir}/tiff_Pol2Xist/${outName}_Pol2Xist
                                    
                                if [ -s ${tmpDir}/tiff_Pol2Xist/${outName}_Pol2Xist.tiff ] ; then 
                                    echo ${job} done! > ${tmpDir}/log/${job}.done
                                else exit 1; fi; date"`
                        jobSplitChannels_Pol2Xist_id=`echo :${jobSplitChannels_Pol2Xist##* }`; jobSplitChannels_Pol2Xist_name=${jobName}
                        if [ -z ${job_all_id} ]; then job_all_id=${jobSplitChannels_Pol2Xist_id}; else job_all_id=${job_all_id}${jobSplitChannels_Pol2Xist_id}; fi    ### add job id to a variable listing all job ids
                        sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobSplitChannels_Pol2Xist_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobSplitChannels_Pol2Xist_id#:}"
                    fi

                    job=SplitChannels_Xist_${outName}
                    if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                        if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                        jobName=${job}...${outName}...${analysisName}
                        jobSplitChannels_Xist=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 15 \
                            --wrap="${env}
                                date
                                mkdir ${tmpDir}/tiff_Xist
                                source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py2.7_IA
                                python ~/programs/ImageAnalysis/lsm_multiChannel_splitChannels.py lsm-subset-channels \
                                    -c 2 --out_format tiff \
                                    -i $file \
                                    -o ${tmpDir}/tiff_Xist/${outName}_Xist
                                    
                                if [ -s ${tmpDir}/tiff_Xist/${outName}_Xist.tiff ] ; then 
                                    echo ${job} done! > ${tmpDir}/log/${job}.done
                                else exit 1; fi; date"`
                        jobSplitChannels_Xist_id=`echo :${jobSplitChannels_Xist##* }`; jobSplitChannels_Xist_name=${jobName}
                        if [ -z ${job_all_id} ]; then job_all_id=${jobSplitChannels_Xist_id}; else job_all_id=${job_all_id}${jobSplitChannels_Xist_id}; fi    ### add job id to a variable listing all job ids
                        sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobSplitChannels_Xist_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobSplitChannels_Xist_id#:}"
                    fi
                done
        fi
    done
done


