analysisName="FCSCI_3Dczi_splitChannels"
mainInDir=/home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI
mainTmpDir=/home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI
mainOutDir=/home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI
env='#!/bin/bash'

cd ${mainInDir}


# datasets=("200205_FCSCI" "200206_FCSCI_SC20c1" "200215_FCSCI_sc18c7" "200216_FCSCI_sc20" "200223_FCSCI" "200229_FCSCI" "200230_FCSCI" "200315_FCSCI")
datasets=("201007_FCSCI" )
# datasetName=200229_FCSCI
# cond=sc18c7_Aux2hAuxDox24h_3d
# file=/home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/sc18c7_Aux2hAuxDox24h_3d/data/sc18c7_Dox24h_3d/200229_FCSCI_sc18c7_sc18c7_Aux2hAuxDox24h_3d_pos5.czi
# outName=$(basename $file .czi)
# tmpDir=${mainTmpDir}/${datasetName}/ilastik/$cond

# # # rm -r /home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/quants/quants_meanTables
# # # rm -r /home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/quants/quants_pixelTables

# process all files
for datasetName in "${datasets[@]}"; do
    echo $datasetName
    analysisName=$datasetName
    mkdir ${mainTmpDir}/${datasetName}/ilastik
    
    calibrationFile=${mainTmpDir}/${datasetName}/FCSpipeline/info.csv
    
    for condPath in ${datasetName}/data/*3d; do 
        cond=$(basename $condPath)
        echo $cond

        tmpDir=${mainTmpDir}/${datasetName}/ilastik/$cond
        mkdir -p ${tmpDir}/std
        mkdir -p ${tmpDir}/log
        
        rm -r ${tmpDir}/quants
        rm -r ${tmpDir}/log/quant*

        for file in ${mainTmpDir}/${datasetName}/ilastik/$cond/tiff_Pol2Xist/*.tiff; do
            echo $file
            outName=$(basename $file _Pol2Xist.tiff)

            while read var; do unset $var; done < <(( set -o posix ; set ) | grep "job.*_id*" | sed 's#=#\t#g' | cut -f 1)

            job=quantPerComp_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                jobName=${job}...${outName}...${analysisName}
                jobquantPerComp=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 15 \
                    --wrap="${env}
                        date
                        mkdir ${tmpDir}/quants
                        source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_IA
                        python /home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/code_FCSCI_3d_3.1_ilastik_quantPerRegion.py ilastik-3d-xinucpnucl \
                            -i $file \
                            -no ${tmpDir}/ilastik_nuc_obj/${outName}_Pol2Xist_ilastik_results_nuc_obj.h5 \
                            -nlpp ${tmpDir}/ilastik_nucLP_proba/${outName}_Pol2Xist_ilastik_results_nucLP_proba.h5 \
                            -nlo ${tmpDir}/ilastik_nucL_obj/${outName}_Pol2Xist_ilastik_results_nucL_obj.h5 \
                            -npo ${tmpDir}/ilastik_nucP_obj/${outName}_Pol2Xist_ilastik_results_nucP_obj.h5 \
                            -xp ${tmpDir}/ilastik_Xi_proba/${outName}_Xist_ilastik_results_Xi_proba.h5 \
                            -xo ${tmpDir}/ilastik_Xi_obj/${outName}_Xist_ilastik_results_Xi_obj_filter.tiff \
                            -c ${calibrationFile} \
                            -d ${outName} \
                            -o ${tmpDir}/quants/${outName}
                            
                        if [ -s ${tmpDir}/quants/${outName}_quant_region_XiNucpNucl.csv ] ; then 
                            echo ${job} done! > ${tmpDir}/log/${job}.done
                        else exit 1; fi; date"`
                jobquantPerComp_id=`echo :${jobquantPerComp##* }`; jobquantPerComp_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobquantPerComp_id}; else job_all_id=${job_all_id}${jobquantPerComp_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobquantPerComp_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobquantPerComp_id#:}"
            fi


            job=copyMean_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                if [ -z ${jobquantPerComp_id} ]; then dep= ; else dep="--dependency=afterok${jobquantPerComp_id}" ; fi
                jobName=${job}...${outName}...${analysisName}
                jobcopyMean=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 15 ${dep} \
                    --wrap="${env}
                        date
                        mkdir -p /home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/quants/quants_meanTables
                        sed 1d ${tmpDir}/quants/${outName}_quant_region_XiNucpNucl.csv > /home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/quants/quants_meanTables/${outName}_quant_region_XiNucpNucl.csv
                        date"`
                jobcopyMean_id=`echo :${jobcopyMean##* }`; jobcopyMean_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobcopyMean_id}; else job_all_id=${job_all_id}${jobcopyMean_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobcopyMean_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobcopyMean_id#:}"
            fi

            job=copyPixelTable_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                if [ -z ${jobquantPerComp_id} ]; then dep= ; else dep="--dependency=afterok${jobquantPerComp_id}" ; fi
                jobName=${job}...${outName}...${analysisName}
                jobcopyPixelTable=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 15 ${dep} \
                    --wrap="${env}
                        date
                        mkdir -p /home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/quants/quants_pixelTables
                        sed 1d ${tmpDir}/quants/${outName}_quant_pixel_Xi.tsv > /home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/quants/quants_pixelTables/${outName}_quant_pixel_Xi.tsv
                        sed 1d ${tmpDir}/quants/${outName}_quant_pixel_XiNucpNucl.tsv > /home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/quants/quants_pixelTables/${outName}_quant_pixel_XiNucpNucl.tsv
                        date"`
                jobcopyPixelTable_id=`echo :${jobcopyPixelTable##* }`; jobcopyPixelTable_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobcopyPixelTable_id}; else job_all_id=${job_all_id}${jobcopyPixelTable_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobcopyPixelTable_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobcopyPixelTable_id#:}"
            fi

        done
    done
done

### after all is done 
echo -e "dataset_name\tnucID\tXi_Pol2_raw_mean\tXi_Pol2_raw_sd \tXi_Pol2_c_mean\tXi_Pol2_c_sd\tXi_Pol2_c_NucpRatio_mean\tXi_Pol2_c_NucpRatio_sd\tNucP_Pol2_raw_mean\tNucP_Pol2_raw_sd \tNucP_Pol2_c_mean\tNucP_Pol2_c_sd\tNucP_Pol2_c_NucpRatio_mean\tNucP_Pol2_c_NucpRatio_sd\tNucL_Pol2_raw_mean\tNucL_Pol2_raw_sd \tNucL_Pol2_c_mean\tNucL_Pol2_c_sd\tNucL_Pol2_c_NucpRatio_mean\tNucL_Pol2_c_NucpRatio_sd\tXi_Xist_mean\tXi_Xist_sd\tXi_Xist_NucpRatio_mean\tXi_Xist_NucpRatio_sd\tNucP_Xist_mean\tNucP_Xist_sd\tNucP_Xist_NucpRatio_mean\tNucP_Xist_NucpRatio_sd\tNucL_Xist_mean\tNucL_Xist_sd\tNucL_Xist_NucpRatio_mean\tNucL_Xist_NucpRatio_sd\tnucPixelN\tXiPixelN\tnucpPixelN\tnuclPixelN" > /home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/quants/quants_meanTable_all.tsv
cat /home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/quants/quants_meanTables/*_quant_region_XiNucpNucl.csv >> /home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/quants/quants_meanTable_all.tsv
# 
echo -e "Pol2\tPol2_c\tXist\tPol2_c_gauss100\tXist_gauss100\tPol2_c_gauss200\tXist_gauss200\tPol2_c_gauss300\tXist_gauss300\tPol2_c_gauss400\tXist_gauss400\tdist_border_nuc\tdist_border_Xi\tdist_periph_Xi\tdist_border_nucP\tdist_border_nucL\tPol2_c_NucpRatio\tXist_NucpRatio\tPol2_c_gauss100_NucpRatio\tXist_gauss100_NucpRatio\tPol2_c_gauss200_NucpRatio\tXist_gauss200_NucpRatio\tPol2_c_gauss300_NucpRatio\tXist_gauss300_NucpRatio\tPol2_c_gauss400_NucpRatio\tXist_gauss400_NucpRatio\tregion\tdatasetName\tnucID" > /home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/quants/quants_pixelTable_Xi_all.tsv
cat /home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/quants/quants_pixelTables/*_quant_pixel_Xi.tsv >> /home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/quants/quants_pixelTable_Xi_all.tsv


echo -e "Pol2\tPol2_c\tXist\tPol2_c_gauss100\tXist_gauss100\tPol2_c_gauss150\tXist_gauss150\tPol2_c_gauss200\tXist_gauss200\tdist_border_nuc\tdist_border_Xi\tdist_periph_Xi\tdist_border_nucP\tdist_border_nucL\tPol2_c_NucpRatio\tXist_NucpRatio\tPol2_c_gauss100_NucpRatio\tXist_gauss100_NucpRatio\tPol2_c_gauss150_NucpRatio\tXist_gauss150_NucpRatio\tPol2_c_gauss200_NucpRatio\tXist_gauss200_NucpRatio\tregion\tdatasetName\tnucID" > /home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/quants/quants_pixelTable_XiNucpNucl_all.tsv
cat /home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/quants/quants_pixelTables/200229_FCSCI_sc18c7_Dox24h*_quant_pixel_XiNucpNucl.tsv >> /home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/quants/quants_pixelTable_XiNucpNucl_all.tsv
