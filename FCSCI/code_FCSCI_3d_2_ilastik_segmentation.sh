analysisName="FCSCI_3Dczi_splitChannels"
mainInDir=/home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI
mainTmpDir=/home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI
mainOutDir=/home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI
env='#!/bin/bash'

cd ${mainInDir}


# datasets=("200205_FCSCI" "200206_FCSCI_SC20c1" "200215_FCSCI_sc18c7" "200216_FCSCI_sc20" "200223_FCSCI" "200229_FCSCI" "200230_FCSCI" "200315_FCSCI")
datasets=("201007_FCSCI" )
# datasetName=200229_FCSCI
# cond=sc18c7_Dox24h_3d
# file=/home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/200229_FCSCI/data/sc18c7_Dox24h_3d/200229_FCSCI_sc18c7_Dox24h_3D_pos2.czi

# process all files
for datasetName in "${datasets[@]}"; do
    echo $datasetName
    analysisName=$datasetName
    
    for condPath in ${datasetName}/data/*3d; do 
        cond=$(basename $condPath)
        echo $cond
        tmpDir=${mainTmpDir}/${datasetName}/ilastik/$cond

        rm -r ${tmpDir}/ilastik*
        rm -r ${tmpDir}/log/Ilastik*

        for file in ${mainTmpDir}/${datasetName}/ilastik/$cond/tiff_Pol2Xist/*.tiff; do
            echo $file
            outName=$(basename $file _Pol2Xist.tiff)
            
            while read var; do unset $var; done < <(( set -o posix ; set ) | grep "job.*_id*" | sed 's#=#\t#g' | cut -f 1)

            job=Ilastik_nuc_proba_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                if [ -z ${jobSplitChannels_1n3_id} ]; then dep= ; else dep="--dependency=afterok${jobSplitChannels_1n3_id}" ; fi
                jobName=${job}...${outName}...${analysisName}
                jobIlastik_nuc_proba=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 15 ${dep} \
                    --wrap="${env}
                        date
                        mkdir ${tmpDir}/ilastik_nuc_proba
                        source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_IA
                        /home/scollomb/programs/ilastik-1.3.3post2-Linux/run_ilastik.sh --headless \
                            --readonly --input_axes=zcyx \
                            --output_format='hdf5' --export_source 'Probabilities Stage 2' --output_axis_order=zcyx --export_dtype='float32' \
                            --project=/home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/models/200501_FCSCI_nuc_proba.ilp \
                            --output_filename_format=${tmpDir}/ilastik_nuc_proba/{nickname}_ilastik_results_nuc_proba.h5 \
                            ${tmpDir}/tiff_Pol2Xist/${outName}_Pol2Xist.tiff
                        
                        if [ -s ${tmpDir}/ilastik_nuc_proba/${outName}_Pol2Xist_ilastik_results_nuc_proba.h5 ] ; then 
                            echo ${job} done! > ${tmpDir}/log/${job}.done
                        else exit 1; fi; date"`
                jobIlastik_nuc_proba_id=`echo :${jobIlastik_nuc_proba##* }`; jobIlastik_nuc_proba_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobIlastik_nuc_proba_id}; else job_all_id=${job_all_id}${jobIlastik_nuc_proba_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobIlastik_nuc_proba_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobIlastik_nuc_proba_id#:}"
            fi

            job=Ilastik_nuc_obj_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                if [ -z ${jobIlastik_nuc_proba_id} ]; then dep= ; else dep="--dependency=afterok${jobIlastik_nuc_proba_id}" ; fi
                jobName=${job}...${outName}...${analysisName}
                jobIlastik_nuc_obj=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 15 ${dep} \
                    --wrap="${env}
                        date
                        mkdir ${tmpDir}/ilastik_nuc_obj
                        source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_IA
                        /home/scollomb/programs/ilastik-1.3.3post2-Linux/run_ilastik.sh --headless \
                            --readonly --input_axes=zcyx \
                            --output_format='hdf5' --export_source 'Object Identities' --output_axis_order=zcyx --export_dtype='float32' \
                            --project=/home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/models/200501_FCSCI_nuc_obj.ilp \
                            --output_filename_format=${tmpDir}/ilastik_nuc_obj/{nickname}_ilastik_results_nuc_obj.h5 \
                            --raw_data ${tmpDir}/tiff_Pol2Xist/${outName}_Pol2Xist.tiff \
                            --prediction_maps ${tmpDir}/ilastik_nuc_proba/${outName}_Pol2Xist_ilastik_results_nuc_proba.h5
                        
                        if [ -s ${tmpDir}/ilastik_nuc_obj/${outName}_Pol2Xist_ilastik_results_nuc_obj.h5 ] ; then 
                            echo ${job} done! > ${tmpDir}/log/${job}.done
                        else exit 1; fi; date"`
                jobIlastik_nuc_obj_id=`echo :${jobIlastik_nuc_obj##* }`; jobIlastik_nuc_obj_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobIlastik_nuc_obj_id}; else job_all_id=${job_all_id}${jobIlastik_nuc_obj_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobIlastik_nuc_obj_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobIlastik_nuc_obj_id#:}"
            fi


            job=Ilastik_nucLP_proba_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                if [ -z ${jobSplitChannels_1n3_id} ]; then dep= ; else dep="--dependency=afterok${jobSplitChannels_1n3_id}" ; fi
                jobName=${job}...${outName}...${analysisName}
                jobIlastik_nucLP_proba=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 30 ${dep} \
                    --wrap="${env}
                        date
                        mkdir ${tmpDir}/ilastik_nucLP_proba
                        source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_IA
                        /home/scollomb/programs/ilastik-1.3.3post2-Linux/run_ilastik.sh --headless \
                            --readonly --input_axes=zcyx \
                            --output_format='hdf5' --export_source 'Probabilities Stage 2' --output_axis_order=zcyx --export_dtype='float32' \
                            --project=/home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/models/200507_FCSCI_nucLP_proba.ilp \
                            --output_filename_format=${tmpDir}/ilastik_nucLP_proba/{nickname}_ilastik_results_nucLP_proba.h5 \
                            ${tmpDir}/tiff_Pol2Xist/${outName}_Pol2Xist.tiff
                        
                        if [ -s ${tmpDir}/ilastik_nucLP_proba/${outName}_Pol2Xist_ilastik_results_nucLP_proba.h5 ] ; then 
                            echo ${job} done! > ${tmpDir}/log/${job}.done
                        else exit 1; fi; date"`
                jobIlastik_nucLP_proba_id=`echo :${jobIlastik_nucLP_proba##* }`; jobIlastik_nucLP_proba_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobIlastik_nucLP_proba_id}; else job_all_id=${job_all_id}${jobIlastik_nucLP_proba_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobIlastik_nucLP_proba_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobIlastik_nucLP_proba_id#:}"
            fi


            job=Ilastik_nucL_obj_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                if [ -z ${jobIlastik_nucLP_proba_id} ]; then dep= ; else dep="--dependency=afterok${jobIlastik_nucLP_proba_id}" ; fi
                jobName=${job}...${outName}...${analysisName}
                jobIlastik_nucL_obj=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 15 ${dep} \
                    --wrap="${env}
                        date
                        mkdir ${tmpDir}/ilastik_nucL_obj
                        source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_IA
                        /home/scollomb/programs/ilastik-1.3.3post2-Linux/run_ilastik.sh --headless \
                            --readonly --input_axes=zcyx \
                            --output_format='hdf5' --export_source 'Object Identities' --output_axis_order=zcyx --export_dtype='float32' \
                            --project=/home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/models/200507_FCSCI_nucL_obj.ilp \
                            --output_filename_format=${tmpDir}/ilastik_nucL_obj/{nickname}_ilastik_results_nucL_obj.h5 \
                            --raw_data ${tmpDir}/tiff_Pol2Xist/${outName}_Pol2Xist.tiff \
                            --prediction_maps ${tmpDir}/ilastik_nucLP_proba/${outName}_Pol2Xist_ilastik_results_nucLP_proba.h5
                        
                        if [ -s ${tmpDir}/ilastik_nucL_obj/${outName}_Pol2Xist_ilastik_results_nucL_obj.h5 ] ; then 
                            echo ${job} done! > ${tmpDir}/log/${job}.done
                        else exit 1; fi; date"`
                jobIlastik_nucL_obj_id=`echo :${jobIlastik_nucL_obj##* }`; jobIlastik_nucL_obj_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobIlastik_nucL_obj_id}; else job_all_id=${job_all_id}${jobIlastik_nucL_obj_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobIlastik_nucL_obj_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobIlastik_nucL_obj_id#:}"
            fi

            job=Ilastik_nucP_obj_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                if [ -z ${jobIlastik_nucLP_proba_id} ]; then dep= ; else dep="--dependency=afterok${jobIlastik_nucLP_proba_id}" ; fi
                jobName=${job}...${outName}...${analysisName}
                jobIlastik_nucP_obj=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 15 ${dep} \
                    --wrap="${env}
                        date
                        mkdir ${tmpDir}/ilastik_nucP_obj
                        source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_IA
                        /home/scollomb/programs/ilastik-1.3.3post2-Linux/run_ilastik.sh --headless \
                            --readonly --input_axes=zcyx \
                            --output_format='hdf5' --export_source 'Object Identities' --output_axis_order=zcyx --export_dtype='float32' \
                            --project=/home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/models/200507_FCSCI_nucP_obj.ilp \
                            --output_filename_format=${tmpDir}/ilastik_nucP_obj/{nickname}_ilastik_results_nucP_obj.h5 \
                            --raw_data ${tmpDir}/tiff_Pol2Xist/${outName}_Pol2Xist.tiff \
                            --prediction_maps ${tmpDir}/ilastik_nucLP_proba/${outName}_Pol2Xist_ilastik_results_nucLP_proba.h5
                        
                        if [ -s ${tmpDir}/ilastik_nucP_obj/${outName}_Pol2Xist_ilastik_results_nucP_obj.h5 ] ; then 
                            echo ${job} done! > ${tmpDir}/log/${job}.done
                        else exit 1; fi; date"`
                jobIlastik_nucP_obj_id=`echo :${jobIlastik_nucP_obj##* }`; jobIlastik_nucP_obj_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobIlastik_nucP_obj_id}; else job_all_id=${job_all_id}${jobIlastik_nucP_obj_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobIlastik_nucP_obj_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobIlastik_nucP_obj_id#:}"
            fi

            job=Ilastik_Xi_proba_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                if [ -z ${jobSplitChannels_3_id} ]; then dep= ; else dep="--dependency=afterok${jobSplitChannels_3_id}" ; fi
                jobName=${job}...${outName}...${analysisName}
                jobIlastik_Xi_proba=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 15 ${dep} \
                    --wrap="${env}
                        date
                        mkdir ${tmpDir}/ilastik_Xi_proba
                        source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_IA
                        /home/scollomb/programs/ilastik-1.3.3post2-Linux/run_ilastik.sh --headless \
                            --readonly --input_axes=zyx \
                            --output_format='hdf5' --export_source 'Probabilities Stage 2' --output_axis_order=zcyx --export_dtype='float32' \
                            --project=/home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/models/200502_FCSCI_Xi_proba.ilp \
                            --output_filename_format=${tmpDir}/ilastik_Xi_proba/{nickname}_ilastik_results_Xi_proba.h5 \
                            ${tmpDir}/tiff_Xist/${outName}_Xist.tiff
                        
                        if [ -s ${tmpDir}/ilastik_Xi_proba/${outName}_Xist_ilastik_results_Xi_proba.h5 ] ; then 
                            echo ${job} done! > ${tmpDir}/log/${job}.done
                        else exit 1; fi; date"`
                jobIlastik_Xi_proba_id=`echo :${jobIlastik_Xi_proba##* }`; jobIlastik_Xi_proba_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobIlastik_Xi_proba_id}; else job_all_id=${job_all_id}${jobIlastik_Xi_proba_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobIlastik_Xi_proba_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobIlastik_Xi_proba_id#:}"
            fi

            job=Ilastik_Xi_obj_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                if [ -z ${jobIlastik_Xi_proba_id} ]; then dep= ; else dep="--dependency=afterok${jobIlastik_Xi_proba_id}" ; fi
                jobName=${job}...${outName}...${analysisName}
                jobIlastik_Xi_obj=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 15 ${dep} \
                    --wrap="${env}
                        date
                        mkdir ${tmpDir}/ilastik_Xi_obj
                        source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_IA
                        /home/scollomb/programs/ilastik-1.3.3post2-Linux/run_ilastik.sh --headless \
                            --readonly --input_axes=zyx \
                            --output_format='hdf5' --export_source 'Object Identities' --output_axis_order=zcyx --export_dtype='float32' \
                            --project=/home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/Ilastik/models/200502_FCSCI_Xi_obj.ilp \
                            --output_filename_format=${tmpDir}/ilastik_Xi_obj/{nickname}_ilastik_results_Xi_obj.h5 \
                            --raw_data ${tmpDir}/tiff_Xist/${outName}_Xist.tiff \
                            --prediction_maps ${tmpDir}/ilastik_Xi_proba/${outName}_Xist_ilastik_results_Xi_proba.h5
                        
                        if [ -s ${tmpDir}/ilastik_Xi_obj/${outName}_Xist_ilastik_results_Xi_obj.h5 ] ; then 
                            echo ${job} done! > ${tmpDir}/log/${job}.done
                        else exit 1; fi; date"`
                jobIlastik_Xi_obj_id=`echo :${jobIlastik_Xi_obj##* }`; jobIlastik_Xi_obj_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobIlastik_Xi_obj_id}; else job_all_id=${job_all_id}${jobIlastik_Xi_obj_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobIlastik_Xi_obj_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobIlastik_Xi_obj_id#:}"
            fi

            job=Ilastik_Xi_obj_filterBorder_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                if [ -z ${jobIlastik_Xi_obj_id} ]; then dep= ; else dep="--dependency=afterok${jobIlastik_Xi_obj_id}" ; fi
                jobName=${job}...${outName}...${analysisName}
                jobIlastik_Xi_obj_filterBorder=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 15 ${dep} \
                    --wrap="${env}
                        date
                        mkdir ${tmpDir}/ilastik_Xi_obj
                        source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_IA
                        python /home/scollomb/Lab_Heard/project_SC_XiComp/FCSCI/code_FCSCI_3d_2.1_ilastik_segmentation_Xi_filter.py ilastik-xi-obj-filterborder \
                        -i ${tmpDir}/ilastik_Xi_obj/${outName}_Xist_ilastik_results_Xi_obj.h5 \
                        -o ${tmpDir}/ilastik_Xi_obj/${outName}_Xist_ilastik_results_Xi_obj_filter
                        
                        if [ -s ${tmpDir}/ilastik_Xi_obj/${outName}_Xist_ilastik_results_Xi_obj_filter.tiff ] ; then 
                            echo ${job} done! > ${tmpDir}/log/${job}.done
                        else exit 1; fi; date"`
                jobIlastik_Xi_obj_filterBorder_id=`echo :${jobIlastik_Xi_obj_filterBorder##* }`; jobIlastik_Xi_obj_filterBorder_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobIlastik_Xi_obj_filterBorder_id}; else job_all_id=${job_all_id}${jobIlastik_Xi_obj_filterBorder_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobIlastik_Xi_obj_filterBorder_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobIlastik_Xi_obj_filterBorder_id#:}"
            fi

        done
    done
done



