## source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_IA ; python
import click

### order subcommand in help by their order in the code, not alphabetically 
class NaturalOrderGroup(click.Group):
    # def __init__(self, name=None, commands=None, **attrs):
    #     if commands is None:
    #         commands = OrderedDict()
    #     elif not isinstance(commands, OrderedDict):
    #         commands = OrderedDict(commands)
    #     click.Group.__init__(self, name=name,
    #                          commands=commands,
    #                          **attrs)
    
    def list_commands(self, ctx):
        return self.commands.keys()


@click.group(cls=NaturalOrderGroup)
def main():
    '''
    '''
@main.command()
@click.option('-i', '--input_ilastik_xi_obj', type = str, required=True, help = '')
@click.option('-o', '--out_prefix', type = str, required=True,  help = '')
def ilastik_xi_obj_filterborder(
    input_ilastik_xi_obj,
    out_prefix
):
    '''
    .     
    '''
    import sys
    from czifile import CziFile
    from PIL import Image
    import skimage.io as skio
    import numpy as np
    import h5py
    import scipy as scp
    import scipy.stats
    from matplotlib import pyplot as plt
    import seaborn as sns
    import pandas as pd
    import statsmodels.robust.scale

    ## import data
    ilastik_Xi_obj = h5py.File(input_ilastik_xi_obj, 'r')['exported_data'][()][:,0,:,:]

    dz=ilastik_Xi_obj.shape[0]
    dy=ilastik_Xi_obj.shape[1]
    dx=ilastik_Xi_obj.shape[2]


    Xi_obj_id = [int(i) for i in np.unique(ilastik_Xi_obj)[1:]]

    ilastik_Xi_obj_filter = ilastik_Xi_obj.copy()

    for i in Xi_obj_id:
        ilastik_Xi_obj_xi = ilastik_Xi_obj.copy()
        ilastik_Xi_obj_xi[ilastik_Xi_obj_xi[:]!=i] = 0
        ilastik_Xi_obj_xi_com = scp.ndimage.measurements.center_of_mass(ilastik_Xi_obj_xi)
        if ilastik_Xi_obj_xi_com[0]<2 or ilastik_Xi_obj_xi_com[0]>(dz-4) or ilastik_Xi_obj_xi_com[1]<11 or ilastik_Xi_obj_xi_com[1]>(dy-12) or ilastik_Xi_obj_xi_com[2]<11 or ilastik_Xi_obj_xi_com[2]>(dx-12):
            ilastik_Xi_obj_filter[ilastik_Xi_obj_filter[:]==i]=0

    skio.imsave(out_prefix  + '.tiff', ilastik_Xi_obj_filter, plugin='tifffile',imagej=True, append=False)



if __name__ == "__main__":
    main()
