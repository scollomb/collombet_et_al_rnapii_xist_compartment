## source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_IA ; python
import click

### order subcommand in help by their order in the code, not alphabetically 
class NaturalOrderGroup(click.Group):
    # def __init__(self, name=None, commands=None, **attrs):
    #     if commands is None:
    #         commands = OrderedDict()
    #     elif not isinstance(commands, OrderedDict):
    #         commands = OrderedDict(commands)
    #     click.Group.__init__(self, name=name,
    #                          commands=commands,
    #                          **attrs)
    
    def list_commands(self, ctx):
        return self.commands.keys()


@click.group(cls=NaturalOrderGroup)
def main():
    '''
    '''
@main.command()
@click.option('-i', '--input_rawdata', type = str, required=True, help = 'czi file')
@click.option('-no', '--input_ilastik_nuc_obj', type = str, required=True,  help = '')
@click.option('-nlpp', '--input_ilastik_nuclp_proba', type = str, required=True, help = '')
@click.option('-nlo', '--input_ilastik_nucl_obj', type = str, required=True, help = '')
@click.option('-npo', '--input_ilastik_nucp_obj', type = str, required=True,  help = '')
@click.option('-xp', '--input_ilastik_xi_proba', type = str, required=True, help = '')
@click.option('-xo', '--input_ilastik_xi_obj', type = str, required=True, help = '')
@click.option('-c', '--calibration_file', type = str, required=True, help = '')
@click.option('-d', '--dataset_name', type = str, required=True,  help = '')
@click.option('-o', '--out_prefix', type = str, required=True,  help = '')
def ilastik_3D_XinucPnucL(
    input_rawdata,
    input_ilastik_nuc_obj,
    input_ilastik_nuclp_proba,
    input_ilastik_nucl_obj,
    input_ilastik_nucp_obj,
    input_ilastik_xi_proba,
    input_ilastik_xi_obj,
    calibration_file,
    dataset_name,
    out_prefix
):
    '''
    .     
    '''
    import sys
    from czifile import CziFile
    from PIL import Image
    import skimage.io as skio
    import numpy as np
    import h5py
    import scipy as scp
    import scipy.stats 
    from matplotlib import pyplot as plt
    import seaborn as sns
    import pandas as pd
    import statsmodels.robust.scale
    
    pxSize_x=0.0991362
    pxSize_z=0.4754503
    
    c_rawdata_Xist=1
    c_rawdata_Pol2=0
    c_probsXi_Xi=2
    c_probsNuc_nucP=1
    c_probsNuc_nucL=2
    
    ## import data
    print('---- loading data')
    rawdata = skio.imread(input_rawdata)
    ilastik_nuc_obj = h5py.File(input_ilastik_nuc_obj, 'r')['exported_data'][()][:,0,:,:]
    ilastik_nucLP_proba = h5py.File(input_ilastik_nuclp_proba, 'r')['exported_data'][()]
    ilastik_nucL_obj = h5py.File(input_ilastik_nucl_obj, 'r')['exported_data'][()][:,0,:,:]
    ilastik_nucP_obj = h5py.File(input_ilastik_nucp_obj, 'r')['exported_data'][()][:,0,:,:]
    ilastik_Xi_proba = h5py.File(input_ilastik_xi_proba, 'r')['exported_data'][()]
    ilastik_Xi_obj = skio.imread(input_ilastik_xi_obj)[:,:,:]
    calibration=pd.read_csv(calibration_file)
    calibration=calibration.rename(columns={"slope, nM per intensity unit": "slope", "intercept, nM": "intercept"})
    
    c_rawdata_Xist=1
    c_rawdata_Pol2=0
    c_Xiproba_Xi=2
    c_nucLPproba_nucP=1
    c_nucLPproba_nucL=2
    
    dz=rawdata.shape[0]
    dy=rawdata.shape[2]
    dx=rawdata.shape[3]
    
    def confidence_interval(data, confidence=0.95):
        a = 1.0 * np.array(data)
        n = len(a)
        m, se = np.mean(a), scipy.stats.sem(a)
        h = se * scipy.stats.t.ppf((1 + confidence) / 2., n-1)
        return h
    
    def calibrated_c(x):
        c=x*calibration['slope'][0]+calibration['intercept'][0]
        return c
    
    rawdata_Pol2 = rawdata[:,c_rawdata_Pol2,:,:]
    rawdata_Pol2_c = np.vectorize(calibrated_c)(rawdata_Pol2)
    rawdata_Pol2_c_gaussianS100nm = scipy.ndimage.gaussian_filter(rawdata_Pol2_c, sigma=[0.1/pxSize_z, 0.1/pxSize_x, 0.1/pxSize_x])
    rawdata_Pol2_c_gaussianS200nm = scipy.ndimage.gaussian_filter(rawdata_Pol2_c, sigma=[0.2/pxSize_z, 0.2/pxSize_x, 0.2/pxSize_x])
    rawdata_Pol2_c_gaussianS300nm = scipy.ndimage.gaussian_filter(rawdata_Pol2_c, sigma=[0.3/pxSize_z, 0.3/pxSize_x, 0.3/pxSize_x])
    rawdata_Pol2_c_gaussianS400nm = scipy.ndimage.gaussian_filter(rawdata_Pol2_c, sigma=[0.4/pxSize_z, 0.4/pxSize_x, 0.4/pxSize_x])
    
    rawdata_Xist = rawdata[:,c_rawdata_Xist,:,:]
    rawdata_Xist_gaussianS100nm = scipy.ndimage.gaussian_filter(rawdata_Xist, sigma=[0.1/pxSize_z, 0.1/pxSize_x, 0.1/pxSize_x])
    rawdata_Xist_gaussianS200nm = scipy.ndimage.gaussian_filter(rawdata_Xist, sigma=[0.2/pxSize_z, 0.2/pxSize_x, 0.2/pxSize_x])
    rawdata_Xist_gaussianS300nm = scipy.ndimage.gaussian_filter(rawdata_Xist, sigma=[0.3/pxSize_z, 0.3/pxSize_x, 0.3/pxSize_x])
    rawdata_Xist_gaussianS400nm = scipy.ndimage.gaussian_filter(rawdata_Xist, sigma=[0.4/pxSize_z, 0.4/pxSize_x, 0.4/pxSize_x])
    
    ### distances to borders of compartments
    print('---- processing compartment mask')
    ### 
    ## nuc
    # convert object to mask
    ilastik_nuc_obj_mask=ilastik_nuc_obj.copy()
    ilastik_nuc_obj_mask[ilastik_nuc_obj_mask[:]>0] =1
    #convert to distance in pixel 
    ilastik_nuc_obj_mask_dist=scipy.ndimage.morphology.distance_transform_edt(ilastik_nuc_obj_mask).astype(np.float32)
    #convert to border
    ilastik_nuc_obj_mask_dist[ilastik_nuc_obj_mask_dist[:]>1] = 0
    #convert : border=0 and rest =1
    ilastik_nuc_obj_mask_dist[:] = -(ilastik_nuc_obj_mask_dist[:]-1)
    #convert to distance to border in um
    ilastik_nuc_obj_mask_dist=scipy.ndimage.morphology.distance_transform_edt(ilastik_nuc_obj_mask_dist, sampling=[pxSize_z, pxSize_x, pxSize_x])
    ilastik_nuc_obj_mask_dist=ilastik_nuc_obj_mask_dist.astype(np.float32)
    
    ### XI
    Xi_minProba=0.75
    # convert proba to mask
    ilastik_Xi_obj_mask = ilastik_Xi_obj.copy()
    # filter also on proba
    ilastik_Xi_proba_th = ilastik_Xi_proba.copy()[:,2,:,:]
    ilastik_Xi_proba_th[ilastik_Xi_proba_th[:]<Xi_minProba] = 0
    ilastik_Xi_obj_mask = ilastik_Xi_obj_mask.copy() * ilastik_Xi_proba_th
    ilastik_Xi_obj_mask[ilastik_Xi_obj_mask[:]>0] =1
    # then same as above
    ilastik_Xi_obj_mask_dist=scipy.ndimage.morphology.distance_transform_edt(ilastik_Xi_obj_mask).astype(np.float32)
    ilastik_Xi_obj_mask_dist[ilastik_Xi_obj_mask_dist[:]>1] = 0
    ilastik_Xi_obj_mask_dist[:] = -(ilastik_Xi_obj_mask_dist[:]-1)
    ilastik_Xi_obj_mask_dist=scipy.ndimage.morphology.distance_transform_edt(ilastik_Xi_obj_mask_dist, sampling=[pxSize_z, pxSize_x, pxSize_x])
    ilastik_Xi_obj_mask_dist=ilastik_Xi_obj_mask_dist.astype(np.float32)
    ## Xi distance to periphery, only inside
    ilastik_Xi_periph_dist=scipy.ndimage.morphology.distance_transform_edt(ilastik_Xi_obj_mask, sampling=[pxSize_z, pxSize_x, pxSize_x])
    
    ## nucL
    ilastik_nucL_obj_mask=ilastik_nucL_obj.copy()
    ilastik_nucL_proba_th = ilastik_nucLP_proba.copy()[:,c_nucLPproba_nucL,:,:]
    ilastik_nucL_proba_th[ilastik_nucL_proba_th[:]<0.75] = 0
    ilastik_nucL_obj_mask = ilastik_nucL_obj_mask.copy() * ilastik_nucL_proba_th
    ilastik_nucL_obj_mask[ilastik_nucL_obj_mask[:]>0] =1
    # exclude Xi 
    ilastik_nucL_obj_mask[ilastik_Xi_obj[:]==1] = 0
    # then same as above
    ilastik_nucL_obj_mask_dist=scipy.ndimage.morphology.distance_transform_edt(ilastik_nucL_obj_mask).astype(np.float32)
    ilastik_nucL_obj_mask_dist[ilastik_nucL_obj_mask_dist[:]>1] = 0
    ilastik_nucL_obj_mask_dist[:] = -(ilastik_nucL_obj_mask_dist[:]-1)
    ilastik_nucL_obj_mask_dist=scipy.ndimage.morphology.distance_transform_edt(ilastik_nucL_obj_mask_dist, sampling=[pxSize_z, pxSize_x, pxSize_x])
    ilastik_nucL_obj_mask_dist=ilastik_nucL_obj_mask_dist.astype(np.float32)
    
    ## nucP
    ilastik_nucP_obj_mask=ilastik_nucP_obj.copy()
    ilastik_nucP_proba_th = ilastik_nucLP_proba.copy()[:,c_nucLPproba_nucP,:,:]
    ilastik_nucP_proba_th[ilastik_nucP_proba_th[:]<0.75] = 0
    ilastik_nucP_obj_mask = ilastik_nucP_obj_mask.copy() * ilastik_nucP_proba_th
    ilastik_nucP_obj_mask[ilastik_nucP_obj_mask[:]>0] =1
    # exclude Xi 
    ilastik_nucP_obj_mask[ilastik_Xi_obj[:]==1] = 0
    # exclude nucL
    ilastik_nucP_obj_mask[ilastik_nucL_obj[:]==1] = 0
    ## then same as above
    ilastik_nucP_obj_mask_dist=scipy.ndimage.morphology.distance_transform_edt(ilastik_nucP_obj_mask).astype(np.float32)
    ilastik_nucP_obj_mask_dist[ilastik_nucP_obj_mask_dist[:]>1] = 0
    ilastik_nucP_obj_mask_dist[:] = -(ilastik_nucP_obj_mask_dist[:]-1)
    ilastik_nucP_obj_mask_dist=scipy.ndimage.morphology.distance_transform_edt(ilastik_nucP_obj_mask_dist, sampling=[pxSize_z, pxSize_x, pxSize_x])
    ilastik_nucP_obj_mask_dist=ilastik_nucP_obj_mask_dist.astype(np.float32)
    
    ## get nuclei id
    nucs = [int(i) for i in np.unique(ilastik_nuc_obj)[1:]]
    print('---- nuclei in the images: ' + str(nucs))
    nucs_Xi = []
    
    ## create a dictionary which will contains the lists of pixel values for each class
    print('---- parsing pixels')
    nucs_signals={}
    for nuc in nucs:
        nucs_signals['nuc'+str(nuc)]={}
        nucs_signals['nuc'+str(nuc)]['nuc_pixelN']=[0]
        nucs_signals['nuc'+str(nuc)]['Xi']={}
        nucs_signals['nuc'+str(nuc)]['Xi_pixelN']=[0]
        nucs_signals['nuc'+str(nuc)]['Xi']['Pol2']=[]
        nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c']=[]
        nucs_signals['nuc'+str(nuc)]['Xi']['Xist']=[]
        nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c_gauss100']=[]
        nucs_signals['nuc'+str(nuc)]['Xi']['Xist_gauss100']=[]
        nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c_gauss200']=[]
        nucs_signals['nuc'+str(nuc)]['Xi']['Xist_gauss200']=[]
        nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c_gauss300']=[]
        nucs_signals['nuc'+str(nuc)]['Xi']['Xist_gauss300']=[]
        nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c_gauss400']=[]
        nucs_signals['nuc'+str(nuc)]['Xi']['Xist_gauss400']=[]
        nucs_signals['nuc'+str(nuc)]['Xi']['dist_border_nuc']=[]
        nucs_signals['nuc'+str(nuc)]['Xi']['dist_border_Xi']=[]
        nucs_signals['nuc'+str(nuc)]['Xi']['dist_periph_Xi']=[]
        nucs_signals['nuc'+str(nuc)]['Xi']['dist_border_nucP']=[]
        nucs_signals['nuc'+str(nuc)]['Xi']['dist_border_nucL']=[]
        nucs_signals['nuc'+str(nuc)]['nucP']={}
        nucs_signals['nuc'+str(nuc)]['nucP_pixelN']=[0]
        nucs_signals['nuc'+str(nuc)]['nucP']['Pol2']=[]
        nucs_signals['nuc'+str(nuc)]['nucP']['Pol2_c']=[]
        nucs_signals['nuc'+str(nuc)]['nucP']['Xist']=[]
        nucs_signals['nuc'+str(nuc)]['nucP']['Pol2_c_gauss100']=[]
        nucs_signals['nuc'+str(nuc)]['nucP']['Xist_gauss100']=[]
        nucs_signals['nuc'+str(nuc)]['nucP']['Pol2_c_gauss300']=[]
        nucs_signals['nuc'+str(nuc)]['nucP']['Xist_gauss300']=[]
        nucs_signals['nuc'+str(nuc)]['nucP']['Pol2_c_gauss200']=[]
        nucs_signals['nuc'+str(nuc)]['nucP']['Xist_gauss200']=[]
        nucs_signals['nuc'+str(nuc)]['nucP']['Pol2_c_gauss400']=[]
        nucs_signals['nuc'+str(nuc)]['nucP']['Xist_gauss400']=[]
        nucs_signals['nuc'+str(nuc)]['nucP']['dist_border_nuc']=[]
        nucs_signals['nuc'+str(nuc)]['nucP']['dist_border_Xi']=[]
        nucs_signals['nuc'+str(nuc)]['nucP']['dist_periph_Xi']=[]
        nucs_signals['nuc'+str(nuc)]['nucP']['dist_border_nucP']=[]
        nucs_signals['nuc'+str(nuc)]['nucP']['dist_border_nucL']=[]
        nucs_signals['nuc'+str(nuc)]['nucL']={}
        nucs_signals['nuc'+str(nuc)]['nucL_pixelN']=[0]
        nucs_signals['nuc'+str(nuc)]['nucL']['Pol2']=[]
        nucs_signals['nuc'+str(nuc)]['nucL']['Pol2_c']=[]
        nucs_signals['nuc'+str(nuc)]['nucL']['Xist']=[]
        nucs_signals['nuc'+str(nuc)]['nucL']['dist_border_nuc']=[]
        nucs_signals['nuc'+str(nuc)]['nucL']['dist_border_Xi']=[]
        nucs_signals['nuc'+str(nuc)]['nucL']['dist_periph_Xi']=[]
        nucs_signals['nuc'+str(nuc)]['nucL']['dist_border_nucP']=[]
        nucs_signals['nuc'+str(nuc)]['nucL']['dist_border_nucL']=[]
    
    for z in range(dz):
        for x in range(dx):
            for y in range(dy):
                if ilastik_nuc_obj_mask[z,y,x]>0:
                    nuc=int(ilastik_nuc_obj[z,y,x])
                    nucs_signals['nuc'+str(nuc)]['nuc_pixelN'][0]+=1
                    if ilastik_Xi_obj_mask[z,y,x]>0:
                        nucs_signals['nuc'+str(nuc)]['Xi_pixelN'][0]+=1
                        nucs_signals['nuc'+str(nuc)]['Xi']['Pol2'].append(rawdata_Pol2[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c'].append(rawdata_Pol2_c[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['Xi']['Xist'].append(rawdata_Xist[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c_gauss100'].append(rawdata_Pol2_c_gaussianS100nm[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['Xi']['Xist_gauss100'].append(rawdata_Xist_gaussianS100nm[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c_gauss200'].append(rawdata_Pol2_c_gaussianS200nm[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['Xi']['Xist_gauss200'].append(rawdata_Xist_gaussianS200nm[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c_gauss300'].append(rawdata_Pol2_c_gaussianS300nm[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['Xi']['Xist_gauss300'].append(rawdata_Xist_gaussianS300nm[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c_gauss400'].append(rawdata_Pol2_c_gaussianS400nm[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['Xi']['Xist_gauss400'].append(rawdata_Xist_gaussianS400nm[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['Xi']['dist_border_nuc'].append(ilastik_nuc_obj_mask_dist[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['Xi']['dist_border_Xi'].append(ilastik_Xi_obj_mask_dist[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['Xi']['dist_periph_Xi'].append(ilastik_Xi_periph_dist[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['Xi']['dist_border_nucP'].append(ilastik_nucP_obj_mask_dist[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['Xi']['dist_border_nucL'].append(ilastik_nucL_obj_mask_dist[z,y,x])
                        if nuc not in nucs_Xi:
                            nucs_Xi.append(nuc)
                    elif ilastik_nucL_obj_mask[z,y,x]>0:
                        nucs_signals['nuc'+str(nuc)]['nucL_pixelN'][0]+=1
                        nucs_signals['nuc'+str(nuc)]['nucL']['Pol2'].append(rawdata_Pol2[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucL']['Pol2_c'].append(rawdata_Pol2_c[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucL']['Xist'].append(rawdata_Xist[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucL']['dist_border_nuc'].append(ilastik_nuc_obj_mask_dist[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucL']['dist_border_Xi'].append(ilastik_Xi_obj_mask_dist[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucL']['dist_periph_Xi'].append(ilastik_Xi_periph_dist[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucL']['dist_border_nucP'].append(ilastik_nucP_obj_mask_dist[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucL']['dist_border_nucL'].append(ilastik_nucL_obj_mask_dist[z,y,x])
                    elif ilastik_nucP_obj_mask[z,y,x]>0:
                        nucs_signals['nuc'+str(nuc)]['nucP_pixelN'][0]+=1
                        nucs_signals['nuc'+str(nuc)]['nucP']['Pol2'].append(rawdata_Pol2[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucP']['Pol2_c'].append(rawdata_Pol2_c[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucP']['Xist'].append(rawdata_Xist[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucP']['Pol2_c_gauss100'].append(rawdata_Pol2_c_gaussianS100nm[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucP']['Xist_gauss100'].append(rawdata_Xist_gaussianS100nm[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucP']['Pol2_c_gauss200'].append(rawdata_Pol2_c_gaussianS200nm[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucP']['Xist_gauss200'].append(rawdata_Xist_gaussianS200nm[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucP']['Pol2_c_gauss300'].append(rawdata_Pol2_c_gaussianS300nm[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucP']['Xist_gauss300'].append(rawdata_Xist_gaussianS300nm[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucP']['Pol2_c_gauss400'].append(rawdata_Pol2_c_gaussianS400nm[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucP']['Xist_gauss400'].append(rawdata_Xist_gaussianS400nm[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucP']['dist_border_nuc'].append(ilastik_nuc_obj_mask_dist[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucP']['dist_border_Xi'].append(ilastik_Xi_obj_mask_dist[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucP']['dist_periph_Xi'].append(ilastik_Xi_periph_dist[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucP']['dist_border_nucP'].append(ilastik_nucP_obj_mask_dist[z,y,x])
                        nucs_signals['nuc'+str(nuc)]['nucP']['dist_border_nucL'].append(ilastik_nucL_obj_mask_dist[z,y,x])
    
    print('---- Calculating nucP normalized signal')
    nucs_Xi_final=[]
    for nuc in nucs_Xi:
        print(nucs_signals['nuc'+str(nuc)]['nucP_pixelN'])
        if nucs_signals['nuc'+str(nuc)]['nucP_pixelN'][0]>0:
            nucs_Xi_final.append(nuc)
            nucP_Pol2_c_mean=statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['nucP']['Pol2_c'])[0]
            nucP_Xist_mean=statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['nucP']['Xist'])[0]
            nucP_Pol2_c_gauss100_mean=statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['nucP']['Pol2_c_gauss100'])[0]
            nucP_Xist_gauss100_mean=statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['nucP']['Xist_gauss100'])[0]
            nucP_Pol2_c_gauss200_mean=statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['nucP']['Pol2_c_gauss200'])[0]
            nucP_Xist_gauss200_mean=statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['nucP']['Xist_gauss200'])[0]
            nucP_Pol2_c_gauss300_mean=statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['nucP']['Pol2_c_gauss300'])[0]
            nucP_Xist_gauss300_mean=statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['nucP']['Xist_gauss300'])[0]
            nucP_Pol2_c_gauss400_mean=statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['nucP']['Pol2_c_gauss400'])[0]
            nucP_Xist_gauss400_mean=statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['nucP']['Xist_gauss400'])[0]
            nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c_NucpRatio']=nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c']/nucP_Pol2_c_mean
            nucs_signals['nuc'+str(nuc)]['Xi']['Xist_NucpRatio']=nucs_signals['nuc'+str(nuc)]['Xi']['Xist']/nucP_Xist_mean
            nucs_signals['nuc'+str(nuc)]['nucP']['Pol2_c_NucpRatio']=nucs_signals['nuc'+str(nuc)]['nucP']['Pol2_c']/nucP_Pol2_c_mean
            nucs_signals['nuc'+str(nuc)]['nucP']['Xist_NucpRatio']=nucs_signals['nuc'+str(nuc)]['nucP']['Xist']/nucP_Xist_mean
            nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c_gauss100_NucpRatio']=nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c_gauss100']/nucP_Pol2_c_mean
            nucs_signals['nuc'+str(nuc)]['Xi']['Xist_gauss100_NucpRatio']=nucs_signals['nuc'+str(nuc)]['Xi']['Xist_gauss100']/nucP_Xist_mean
            nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c_gauss200_NucpRatio']=nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c_gauss200']/nucP_Pol2_c_mean
            nucs_signals['nuc'+str(nuc)]['Xi']['Xist_gauss200_NucpRatio']=nucs_signals['nuc'+str(nuc)]['Xi']['Xist_gauss200']/nucP_Xist_mean
            nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c_gauss300_NucpRatio']=nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c_gauss300']/nucP_Pol2_c_mean
            nucs_signals['nuc'+str(nuc)]['Xi']['Xist_gauss300_NucpRatio']=nucs_signals['nuc'+str(nuc)]['Xi']['Xist_gauss300']/nucP_Xist_mean
            nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c_gauss400_NucpRatio']=nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c_gauss400']/nucP_Pol2_c_mean
            nucs_signals['nuc'+str(nuc)]['Xi']['Xist_gauss400_NucpRatio']=nucs_signals['nuc'+str(nuc)]['Xi']['Xist_gauss400']/nucP_Xist_mean
            if nucs_signals['nuc'+str(nuc)]['nucL_pixelN'][0]>0:
                nucs_signals['nuc'+str(nuc)]['nucL']['Pol2_c_NucpRatio']=nucs_signals['nuc'+str(nuc)]['nucL']['Pol2_c']/nucP_Pol2_c_mean
                nucs_signals['nuc'+str(nuc)]['nucL']['Xist_NucpRatio']=nucs_signals['nuc'+str(nuc)]['nucL']['Xist']/nucP_Xist_mean
            else:
                for key in nucs_signals['nuc'+str(nuc)]['nucL'].keys():
                    nucs_signals['nuc'+str(nuc)]['nucL'][key]=[-1000]
                nucs_signals['nuc'+str(nuc)]['nucL']['Pol2_c_NucpRatio']=[-1000]
                nucs_signals['nuc'+str(nuc)]['nucL']['Xist_NucpRatio']=[-1000]
    
    print('---- nuclei with an Xi and nucleoplasm defined: ' + str(nucs_Xi_final))
    
    ## write output
    print('---- writing csv of average value per region per cell')
    f= open(str(out_prefix)+'_quant_region_XiNucpNucl.csv',"w+")
    f.write("dataset_name\tnucID\t"+
            "Xi_Pol2_raw_mean\tXi_Pol2_raw_sd \tXi_Pol2_c_mean\tXi_Pol2_c_sd\tXi_Pol2_c_NucpRatio_mean\tXi_Pol2_c_NucpRatio_sd\t"+
            "NucP_Pol2_raw_mean\tNucP_Pol2_raw_sd \tNucP_Pol2_c_mean\tNucP_Pol2_c_sd\tNucP_Pol2_c_NucpRatio_mean\tNucP_Pol2_c_NucpRatio_sd\t"+
            "NucL_Pol2_raw_mean\tNucL_Pol2_raw_sd \tNucL_Pol2_c_mean\tNucL_Pol2_c_sd\tNucL_Pol2_c_NucpRatio_mean\tNucL_Pol2_c_NucpRatio_sd\t"+
            "Xi_Xist_mean\tXi_Xist_sd\tXi_Xist_NucpRatio_mean\tXi_Xist_NucpRatio_sd\t"+
            "NucP_Xist_mean\tNucP_Xist_sd\tNucP_Xist_NucpRatio_mean\tNucP_Xist_NucpRatio_sd\t"+
            "NucL_Xist_mean\tNucL_Xist_sd\tNucL_Xist_NucpRatio_mean\tNucL_Xist_NucpRatio_sd\t"+
            "nucPixelN\tXiPixelN\tnucpPixlN\tnuclPixelN\n")
    for nuc in nucs_Xi_final:
        print(nuc)
        f.write("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(
            dataset_name, nuc,
            statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['Xi']['Pol2'])[0], np.std(nucs_signals['nuc'+str(nuc)]['Xi']['Pol2']),
            statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c'])[0], np.std(nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c']),
            statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c_NucpRatio'])[0], np.std(nucs_signals['nuc'+str(nuc)]['Xi']['Pol2_c_NucpRatio']),
            statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['nucP']['Pol2'])[0], np.std(nucs_signals['nuc'+str(nuc)]['nucP']['Pol2']),
            statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['nucP']['Pol2_c'])[0], np.std(nucs_signals['nuc'+str(nuc)]['nucP']['Pol2_c']),
            statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['nucP']['Pol2_c_NucpRatio'])[0], np.std(nucs_signals['nuc'+str(nuc)]['nucP']['Pol2_c_NucpRatio']),
            statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['nucL']['Pol2'])[0], np.std(nucs_signals['nuc'+str(nuc)]['nucL']['Pol2']),
            statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['nucL']['Pol2_c'])[0], np.std(nucs_signals['nuc'+str(nuc)]['nucL']['Pol2_c']),
            statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['nucL']['Pol2_c_NucpRatio'])[0], np.std(nucs_signals['nuc'+str(nuc)]['nucL']['Pol2_c_NucpRatio']),
            statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['Xi']['Xist'])[0], np.std(nucs_signals['nuc'+str(nuc)]['Xi']['Xist']),
            statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['Xi']['Xist_NucpRatio'])[0], np.std(nucs_signals['nuc'+str(nuc)]['Xi']['Xist_NucpRatio']),
            statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['nucP']['Xist'])[0], np.std(nucs_signals['nuc'+str(nuc)]['nucP']['Xist']),
            statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['nucP']['Xist_NucpRatio'])[0], np.std(nucs_signals['nuc'+str(nuc)]['nucP']['Xist_NucpRatio']),
            statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['nucL']['Xist'])[0], np.std(nucs_signals['nuc'+str(nuc)]['nucL']['Xist']),
            statsmodels.robust.scale.huber(nucs_signals['nuc'+str(nuc)]['nucL']['Xist_NucpRatio'])[0], np.std(nucs_signals['nuc'+str(nuc)]['nucL']['Xist_NucpRatio']),
            nucs_signals['nuc'+str(nuc)]['nuc_pixelN'][0], nucs_signals['nuc'+str(nuc)]['Xi_pixelN'][0], nucs_signals['nuc'+str(nuc)]['nucP_pixelN'][0], nucs_signals['nuc'+str(nuc)]['nucL_pixelN'][0]
        ))
    
    f.close
    
    ## convert pixel infos to dataframe and save as csv
    print('---- writing value per pixel per cell')
    nuc=nucs_Xi_final[0]
    print(nuc)
    nuc_px_df_Xi = pd.DataFrame(nucs_signals['nuc'+str(nuc)]['Xi'])
    nuc_px_df_Xi['region']='Xi'
    nuc_px_df_Xi['datasetName']=dataset_name
    nuc_px_df_Xi['nucID']=nuc
    px_df_Xi=nuc_px_df_Xi
    nuc_px_df_nucP = pd.DataFrame(nucs_signals['nuc'+str(nuc)]['nucP'])
    nuc_px_df_nucP['region']='nucP'
    nuc_px_df_nucP['datasetName']=dataset_name
    nuc_px_df_nucP['nucID']=nuc
    nuc_px_df_nucL = pd.DataFrame(nucs_signals['nuc'+str(nuc)]['nucL'])
    nuc_px_df_nucL['region']='nucL'
    nuc_px_df_nucL['datasetName']=dataset_name
    nuc_px_df_nucL['nucID']=nuc
    nuc_px_df=pd.concat([nuc_px_df_Xi,nuc_px_df_nucP,nuc_px_df_nucL])
    px_df=nuc_px_df
    if len(nucs_Xi_final)>1:
        for nuc in nucs_Xi_final[1:]:
            print(nuc)
            nuc_px_df_Xi = pd.DataFrame(nucs_signals['nuc'+str(nuc)]['Xi'])
            nuc_px_df_Xi['region']='Xi'
            nuc_px_df_Xi['datasetName']=dataset_name
            nuc_px_df_Xi['nucID']=nuc
            px_df_Xi=pd.concat([px_df_Xi,nuc_px_df_Xi])
            nuc_px_df_nucP = pd.DataFrame(nucs_signals['nuc'+str(nuc)]['nucP'])
            nuc_px_df_nucP['region']='nucP'
            nuc_px_df_nucP['datasetName']=dataset_name
            nuc_px_df_nucP['nucID']=nuc
            nuc_px_df_nucL = pd.DataFrame(nucs_signals['nuc'+str(nuc)]['nucL'])
            nuc_px_df_nucL['region']='nucL'
            nuc_px_df_nucL['datasetName']=dataset_name
            nuc_px_df_nucL['nucID']=nuc
            nuc_px_df=pd.concat([nuc_px_df_Xi,nuc_px_df_nucP,nuc_px_df_nucL])
            px_df=pd.concat([px_df,nuc_px_df])
    
    px_df_Xi.to_csv(str(out_prefix)+'_quant_pixel_Xi.tsv', encoding='utf-8', sep='\t', index=False)
    px_df.to_csv(str(out_prefix)+'_quant_pixel_XiNucpNucl.tsv', encoding='utf-8', sep='\t', index=False)





if __name__ == "__main__":
    main()
