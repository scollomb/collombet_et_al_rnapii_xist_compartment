analysisName="SPT_sc18c7_Polr2aHalo_Dox24h_PAJF646"
mainTmpDir=/home/scollomb/Lab_Heard/project_SC_XiComp/SPT/Polr2a/sc18c7/Dox24h_PAJF646
mainOutDir=/home/scollomb/Lab_Heard/project_SC_XiComp/SPT/Polr2a/sc18c7/Dox24h_PAJF646
env='#!/bin/bash'

cd ${mainTmpDir}

pixelSize=0.106
timeBtwnFrame=0.005477

tmpDir=${mainTmpDir}/SpotOn
rm -r ${tmpDir}
mkdir -p ${tmpDir}/std
mkdir -p ${tmpDir}/log

####################################################################################
############ SpontOn
####################################################################################
job=SpotOnMerged_ROE_any_ROI_none
if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
    if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
    jobName=${job}...${analysisName}
    jobSpotOnMerged_ROE_any_ROI_none=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 10 \
        --wrap="${env}
            date
            mkdir -p ${mainTmpDir}/SpotOn
            source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
            SpotOn fit-and-plot-2states \
                --input_format mat --time_between_frames ${timeBtwnFrame} --gaps_allowed 0 --localisation_error 0.028 \
                --weight_delta_t True --model_fit CDF --max_jump_length 2 --max_jumps_per_traj 3 --max_delta_t 6 --diffusion_bound_range 0,0.02 --diffusion_free_range 0.1,20 \
                --y_max 0.15 --x_max=0.6 --cmap Blues \
                -i ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_none \
                -o ${tmpDir}/ROE_any_ROI_none

            if [ -s ${tmpDir}/ROE_any_ROI_none_fit_2states.pdf ] ; then 
                echo ${job} done! > ${tmpDir}/log/${job}.done
            else exit 1; fi; date"`
    jobSpotOnMerged_ROE_any_ROI_none_id=`echo :${jobSpotOnMerged_ROE_any_ROI_none##* }`; jobSpotOnMerged_ROE_any_ROI_none_name=${jobName}
    if [ -z ${job_all_id} ]; then job_all_id=${jobSpotOnMerged_ROE_any_ROI_none_id}; else job_all_id=${job_all_id}${jobSpotOnMerged_ROE_any_ROI_none_id}; fi    ### add job id to a variable listing all job ids
    sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobSpotOnMerged_ROE_any_ROI_none_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobSpotOnMerged_ROE_any_ROI_none_id#:}"
fi

job=SpotOnMerged_ROE_any_ROI_any
if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
    if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
    jobName=${job}...${analysisName}
    jobSpotOnMerged_ROE_any_ROI_any=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 10 \
        --wrap="${env}
            date
            mkdir -p ${mainTmpDir}/SpotOn
            source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
            SpotOn fit-and-plot-2states \
                --input_format mat --time_between_frames ${timeBtwnFrame} --gaps_allowed 0 --localisation_error 0.028 \
                --weight_delta_t True --model_fit CDF --max_jump_length 2 --max_jumps_per_traj 3 --max_delta_t 6 --diffusion_bound_range 0,0.02 --diffusion_free_range 0.1,20 \
                --y_max 0.15 --x_max=0.6 --cmap Reds \
                -i ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any \
                -o ${tmpDir}/ROE_any_ROI_any

            if [ -s ${tmpDir}/ROE_any_ROI_any_fit_2states.pdf ] ; then 
                echo ${job} done! > ${tmpDir}/log/${job}.done
            else exit 1; fi; date"`
    jobSpotOnMerged_ROE_any_ROI_any_id=`echo :${jobSpotOnMerged_ROE_any_ROI_any##* }`; jobSpotOnMerged_ROE_any_ROI_any_name=${jobName}
    if [ -z ${job_all_id} ]; then job_all_id=${jobSpotOnMerged_ROE_any_ROI_any_id}; else job_all_id=${job_all_id}${jobSpotOnMerged_ROE_any_ROI_any_id}; fi    ### add job id to a variable listing all job ids
    sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobSpotOnMerged_ROE_any_ROI_any_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobSpotOnMerged_ROE_any_ROI_any_id#:}"
fi


job=Merged_ROE_any_ROI_none_bootstrap
if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
    if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
    if [ -z ${jobTraj_ROE_any_ROI_none_copyForMergedSpotOn_allids} ]; then dep= ; else dep="--dependency=afterok${jobTraj_ROE_any_ROI_none_copyForMergedSpotOn_allids}" ; fi
    jobName=${job}...${analysisName}
    jobMerged_ROE_any_ROI_none_bootstrap=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 \
        --wrap="${env}
            date
            mkdir -p ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_none_bootstrap
            source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
            SpotOn subsample-trajs \
                --input_format mat --min_traj_len 2 --bootstrap_n 50 --subsampling_n 3000 --subsampling_f 0.5 \
                -i ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_none \
                -o ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_none_bootstrap/traj_ROE_any_ROI_none_bootstrap

            if [ -s ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_none_bootstrap/traj_ROE_any_ROI_none_bootstrap_sub50_Tracked.mat ] ; then 
                echo ${job} done! > ${tmpDir}/log/${job}.done
            else exit 1; fi; date"`
    jobMerged_ROE_any_ROI_none_bootstrap_id=`echo :${jobMerged_ROE_any_ROI_none_bootstrap##* }`; jobMerged_ROE_any_ROI_none_bootstrap_name=${jobName}
    if [ -z ${job_all_id} ]; then job_all_id=${jobMerged_ROE_any_ROI_none_bootstrap_id}; else job_all_id=${job_all_id}${jobMerged_ROE_any_ROI_none_bootstrap_id}; fi    ### add job id to a variable listing all job ids
    sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobMerged_ROE_any_ROI_none_bootstrap_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobMerged_ROE_any_ROI_none_bootstrap_id#:}"
fi

for i in {1..50}; do 
    job=SpotOnMerged_ROE_any_ROI_none_bootstrap_sub${i}
    if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
        if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
        if [ -z ${jobMerged_ROE_any_ROI_none_bootstrap_id} ]; then dep= ; else dep="--dependency=afterok${jobMerged_ROE_any_ROI_none_bootstrap_id}" ; fi
        jobName=${job}...${analysisName}
        jobSpotOnMerged_ROE_any_ROI_none_bootstrap_sub=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep}\
            --wrap="${env}
                date
                mkdir -p ${mainTmpDir}/SpotOn/traj_ROE_any_ROI_none_jumpLength_bootstrap
                source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
                SpotOn fit-2states \
                    --input_format mat --time_between_frames ${timeBtwnFrame} --gaps_allowed 0 --localisation_error 0.028 \
                    --weight_delta_t True --model_fit CDF --max_jump_length 2 --max_jumps_per_traj 3 --max_delta_t 6 --diffusion_bound_range 0,0.02 --diffusion_free_range 0.1,20  \
                    -i ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_none_bootstrap/traj_ROE_any_ROI_none_bootstrap_sub${i}_Tracked.mat \
                    -o ${mainTmpDir}/SpotOn/traj_ROE_any_ROI_none_jumpLength_bootstrap/traj_ROE_any_ROI_none_bootstrap_sub${i}

                if [ -s ${mainInDir}/SpotOn/ROE_any_ROI_none_fit_2states.pdf ] ; then 
                    echo ${job} done! > ${tmpDir}/log/${job}.done
                else exit 1; fi; date"`
        jobSpotOnMerged_ROE_any_ROI_none_bootstrap_sub_id=`echo :${jobSpotOnMerged_ROE_any_ROI_none_bootstrap_sub##* }`; jobSpotOnMerged_ROE_any_ROI_none_bootstrap_sub_name=${jobName}
        if [ -z ${job_all_id} ]; then job_all_id=${jobSpotOnMerged_ROE_any_ROI_none_bootstrap_sub_id}; else job_all_id=${job_all_id}${jobSpotOnMerged_ROE_any_ROI_none_bootstrap_sub_id}; fi    ### add job id to a variable listing all job ids
        sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobSpotOnMerged_ROE_any_ROI_none_bootstrap_sub_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobSpotOnMerged_ROE_any_ROI_none_bootstrap_sub_id#:}"
    fi
done


job=Merged_ROE_any_ROI_any_bootstrap
if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
    if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
    if [ -z ${jobTraj_ROE_any_ROI_any_copyForMergedSpotOn_allids} ]; then dep= ; else dep="--dependency=afterok${jobTraj_ROE_any_ROI_any_copyForMergedSpotOn_allids}" ; fi
    jobName=${job}...${analysisName}
    jobMerged_ROE_any_ROI_any_bootstrap=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 \
        --wrap="${env}
            date
            mkdir -p ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_bootstrap
            source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
            SpotOn subsample-trajs \
                --input_format mat --min_traj_len 2 --bootstrap_n 50 --subsampling_n 3000 --subsampling_f 0.5 \
                -i ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any \
                -o ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_bootstrap/traj_ROE_any_ROI_any_bootstrap

            if [ -s ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_bootstrap/traj_ROE_any_ROI_any_bootstrap_sub50_Tracked.mat ] ; then 
                echo ${job} done! > ${tmpDir}/log/${job}.done
            else exit 1; fi; date"`
    jobMerged_ROE_any_ROI_any_bootstrap_id=`echo :${jobMerged_ROE_any_ROI_any_bootstrap##* }`; jobMerged_ROE_any_ROI_any_bootstrap_name=${jobName}
    if [ -z ${job_all_id} ]; then job_all_id=${jobMerged_ROE_any_ROI_any_bootstrap_id}; else job_all_id=${job_all_id}${jobMerged_ROE_any_ROI_any_bootstrap_id}; fi    ### add job id to a variable listing all job ids
    sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobMerged_ROE_any_ROI_any_bootstrap_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobMerged_ROE_any_ROI_any_bootstrap_id#:}"
fi

for i in {1..50}; do 
    job=SpotOnMerged_ROE_any_ROI_any_bootstrap_sub${i}
    if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
        if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
        if [ -z ${jobMerged_ROE_any_ROI_any_bootstrap_id} ]; then dep= ; else dep="--dependency=afterok${jobMerged_ROE_any_ROI_any_bootstrap_id}" ; fi
        jobName=${job}...${analysisName}
        jobSpotOnMerged_ROE_any_ROI_any_bootstrap_sub=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep}\
            --wrap="${env}
                date
                mkdir -p ${mainTmpDir}/SpotOn/traj_ROE_any_ROI_any_jumpLength_bootstrap
                source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
                SpotOn fit-2states \
                    --input_format mat --time_between_frames ${timeBtwnFrame} --gaps_allowed 0 --localisation_error 0.028 \
                    --weight_delta_t True --model_fit CDF --max_jump_length 2 --max_jumps_per_traj 3 --max_delta_t 6 --diffusion_bound_range 0,0.02 --diffusion_free_range 0.1,20  \
                    -i ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_bootstrap/traj_ROE_any_ROI_any_bootstrap_sub${i}_Tracked.mat \
                    -o ${mainTmpDir}/SpotOn/traj_ROE_any_ROI_any_jumpLength_bootstrap/traj_ROE_any_ROI_any_bootstrap_sub${i}

                if [ -s ${mainInDir}/SpotOn/ROE_any_ROI_any_fit_2states.pdf ] ; then 
                    echo ${job} done! > ${tmpDir}/log/${job}.done
                else exit 1; fi; date"`
        jobSpotOnMerged_ROE_any_ROI_any_bootstrap_sub_id=`echo :${jobSpotOnMerged_ROE_any_ROI_any_bootstrap_sub##* }`; jobSpotOnMerged_ROE_any_ROI_any_bootstrap_sub_name=${jobName}
        if [ -z ${job_all_id} ]; then job_all_id=${jobSpotOnMerged_ROE_any_ROI_any_bootstrap_sub_id}; else job_all_id=${job_all_id}${jobSpotOnMerged_ROE_any_ROI_any_bootstrap_sub_id}; fi    ### add job id to a variable listing all job ids
        sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobSpotOnMerged_ROE_any_ROI_any_bootstrap_sub_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobSpotOnMerged_ROE_any_ROI_any_bootstrap_sub_id#:}"
    fi
done





job=SpotOnMerged_ROE_any_ROI_any_angles_MSRDmin200
if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
    if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
    if [ -z ${Merged_ROE_any_ROI_any_bootstrap} ]; then dep= ; else dep="--dependency=afterok${Merged_ROE_any_ROI_any_bootstrap}" ; fi
    jobName=${job}...${analysisName}
    jobSpotOnMerged_ROE_any_ROI_anyangles_MSRDmin200=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 \
        --wrap="${env}
            date
            mkdir -p ${mainTmpDir}/SpotOn
            source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
            SpotOn plot-jumps-angle-circular \
                --input_format mat --gaps_allowed 0 --min_1dt_jump_length 0.2 --max_1dt_jump_length 3 \
                --max_jumps_per_traj 100 --delta_t 1 --bin_width 10 \
                -i ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any \
                -o ${mainTmpDir}/SpotOn/ROE_any_ROI_any_1dt_MSRDmin200

            if [ -s ${mainInDir}/SpotOn/ROE_any_ROI_any_jumpsAnglesDistrib_scaleY.pdf ] ; then 
                echo ${job} done! > ${tmpDir}/log/${job}.done
            else exit 1; fi; date"`
    jobSpotOnMerged_ROE_any_ROI_anyangles_MSRDmin200_id=`echo :${jobSpotOnMerged_ROE_any_ROI_anyangles_MSRDmin200##* }`; jobSpotOnMerged_ROE_any_ROI_anyangles_MSRDmin200_name=${jobName}
    if [ -z ${job_all_id} ]; then job_all_id=${jobSpotOnMerged_ROE_any_ROI_anyangles_MSRDmin200_id}; else job_all_id=${job_all_id}${jobSpotOnMerged_ROE_any_ROI_anyangles_MSRDmin200_id}; fi    ### add job id to a variable listing all job ids
    sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobSpotOnMerged_ROE_any_ROI_anyangles_MSRDmin200_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobSpotOnMerged_ROE_any_ROI_anyangles_MSRDmin200_id#:}"
fi

for i in {1..50}; do 
    job=Merged_ROE_any_ROI_any_angles_MSRDmin200_bootstrap_sub${i}
    if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
        if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
        if [ -z ${jobMerged_ROE_any_ROI_any_bootstrap_id} ]; then dep= ; else dep="--dependency=afterok${jobMerged_ROE_any_ROI_any_bootstrap_id}" ; fi
        jobName=${job}...${analysisName}
        jobMerged_ROE_any_ROI_any_angles_MSRDmin200_bootstrap_sub=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep}\
            --wrap="${env}
                date
                mkdir -p ${mainTmpDir}/SpotOn/traj_ROE_any_ROI_any_angles_bootstrap
                source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
                SpotOn jumps-angle-circular-countonly \
                    --input_format mat --gaps_allowed 0 --min_1dt_jump_length 0.2 --max_1dt_jump_length 3 \
                    --max_jumps_per_traj 100 --delta_t 1 --bin_width 10 \
                    -i ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_bootstrap/traj_ROE_any_ROI_any_bootstrap_sub${i}_Tracked.mat \
                    -o ${mainTmpDir}/SpotOn/traj_ROE_any_ROI_any_angles_bootstrap/traj_ROE_any_ROI_any_bootstrap_sub${i}

                if [ -s ${mainInDir}/SpotOn/traj_ROE_any_ROI_any_angles_bootstrap/traj_ROE_any_ROI_any_bootstrap_sub${i}_jumpsAngles.csv ] ; then 
                    echo ${job} done! > ${tmpDir}/log/${job}.done
                else exit 1; fi; date"`
        jobMerged_ROE_any_ROI_any_angles_MSRDmin200_bootstrap_sub_id=`echo :${jobMerged_ROE_any_ROI_any_angles_MSRDmin200_bootstrap_sub##* }`; jobMerged_ROE_any_ROI_any_angles_MSRDmin200_bootstrap_sub_name=${jobName}
        if [ -z ${job_all_id} ]; then job_all_id=${jobMerged_ROE_any_ROI_any_angles_MSRDmin200_bootstrap_sub_id}; else job_all_id=${job_all_id}${jobMerged_ROE_any_ROI_any_angles_MSRDmin200_bootstrap_sub_id}; fi    ### add job id to a variable listing all job ids
        sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobMerged_ROE_any_ROI_any_angles_MSRDmin200_bootstrap_sub_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobMerged_ROE_any_ROI_any_angles_MSRDmin200_bootstrap_sub_id#:}"
    fi
done


job=SpotOnMerged_ROE_any_ROI_none_angles_MSRDmin200
if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
    if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
    if [ -z ${Merged_ROE_any_ROI_none_bootstrap} ]; then dep= ; else dep="--dependency=afterok${Merged_ROE_any_ROI_none_bootstrap}" ; fi
    jobName=${job}...${analysisName}
    jobSpotOnMerged_ROE_any_ROI_noneangles_MSRDmin200=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 \
        --wrap="${env}
            date
            mkdir -p ${mainTmpDir}/SpotOn
            source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
            SpotOn plot-jumps-angle-circular \
                --input_format mat --gaps_allowed 0 --min_1dt_jump_length 0.2 --max_1dt_jump_length 3 \
                --max_jumps_per_traj 100 --delta_t 1 --bin_width 10 \
                -i ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_none \
                -o ${mainTmpDir}/SpotOn/ROE_any_ROI_none_1dt_MSRDmin200

            if [ -s ${mainInDir}/SpotOn/ROE_any_ROI_none_jumpsAnglesDistrib_scaleY.pdf ] ; then 
                echo ${job} done! > ${tmpDir}/log/${job}.done
            else exit 1; fi; date"`
    jobSpotOnMerged_ROE_any_ROI_noneangles_MSRDmin200_id=`echo :${jobSpotOnMerged_ROE_any_ROI_noneangles_MSRDmin200##* }`; jobSpotOnMerged_ROE_any_ROI_noneangles_MSRDmin200_name=${jobName}
    if [ -z ${job_all_id} ]; then job_all_id=${jobSpotOnMerged_ROE_any_ROI_noneangles_MSRDmin200_id}; else job_all_id=${job_all_id}${jobSpotOnMerged_ROE_any_ROI_noneangles_MSRDmin200_id}; fi    ### add job id to a variable listing all job ids
    sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobSpotOnMerged_ROE_any_ROI_noneangles_MSRDmin200_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobSpotOnMerged_ROE_any_ROI_noneangles_MSRDmin200_id#:}"
fi
   
for i in {1..50}; do 
    job=Merged_ROE_any_ROI_none_angles_MSRDmin200_bootstrap_sub${i}
    if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
        if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
        if [ -z ${jobMerged_ROE_any_ROI_none_bootstrap_id} ]; then dep= ; else dep="--dependency=afterok${jobMerged_ROE_any_ROI_none_bootstrap_id}" ; fi
        jobName=${job}...${analysisName}
        jobMerged_ROE_any_ROI_none_angles_MSRDmin200_bootstrap_sub=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep}\
            --wrap="${env}
                date
                mkdir -p ${mainTmpDir}/SpotOn/traj_ROE_any_ROI_none_angles_bootstrap
                source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
                SpotOn jumps-angle-circular-countonly \
                    --input_format mat --gaps_allowed 0 --min_1dt_jump_length 0.2 --max_1dt_jump_length 3 \
                    --max_jumps_per_traj 100 --delta_t 1 --bin_width 10 \
                    -i ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_none_bootstrap/traj_ROE_any_ROI_none_bootstrap_sub${i}_Tracked.mat \
                    -o ${mainTmpDir}/SpotOn/traj_ROE_any_ROI_none_angles_bootstrap/traj_ROE_any_ROI_none_bootstrap_sub${i}

                if [ -s ${mainInDir}/SpotOn/traj_ROE_any_ROI_none_angles_bootstrap/traj_ROE_any_ROI_none_bootstrap_sub${i}_jumpsAngles.csv ] ; then 
                    echo ${job} done! > ${tmpDir}/log/${job}.done
                else exit 1; fi; date"`
        jobMerged_ROE_any_ROI_none_angles_MSRDmin200_bootstrap_sub_id=`echo :${jobMerged_ROE_any_ROI_none_angles_MSRDmin200_bootstrap_sub##* }`; jobMerged_ROE_any_ROI_none_angles_MSRDmin200_bootstrap_sub_name=${jobName}
        if [ -z ${job_all_id} ]; then job_all_id=${jobMerged_ROE_any_ROI_none_angles_MSRDmin200_bootstrap_sub_id}; else job_all_id=${job_all_id}${jobMerged_ROE_any_ROI_none_angles_MSRDmin200_bootstrap_sub_id}; fi    ### add job id to a variable listing all job ids
        sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobMerged_ROE_any_ROI_none_angles_MSRDmin200_bootstrap_sub_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobMerged_ROE_any_ROI_none_angles_MSRDmin200_bootstrap_sub_id#:}"
    fi
done
 
