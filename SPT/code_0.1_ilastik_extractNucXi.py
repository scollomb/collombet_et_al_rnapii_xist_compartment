import click

### order subcommand in help by their order in the code, not alphabetically 
class NaturalOrderGroup(click.Group):
    # def __init__(self, name=None, commands=None, **attrs):
    #     if commands is None:
    #         commands = OrderedDict()
    #     elif not isinstance(commands, OrderedDict):
    #         commands = OrderedDict(commands)
    #     click.Group.__init__(self, name=name,
    #                          commands=commands,
    #                          **attrs)
    
    def list_commands(self, ctx):
        return self.commands.keys()


@click.group(cls=NaturalOrderGroup)
def main():
    '''
    '''
@main.command()
@click.option('-n', '--input_ilastik_nuc_obj', type = str, required=True,  help = '')
@click.option('-x', '--input_ilastik_xi_obj', type = str, required=True, help = '')
@click.option('-d', '--dataset', type = str, required=True, help = '')
@click.option('-s', '--sample', type = str, required=True, help = '')
@click.option('-o', '--out_prefix', type = str, required=True,  help = '')
def ilastik_filter_nucXi(
    input_ilastik_nuc_obj,
    input_ilastik_xi_obj,
    dataset,
    sample,
    out_prefix
):
    '''
    .     
    '''
    import sys
    from czifile import CziFile
    from PIL import Image
    import skimage.io as skio
    import numpy as np
    import h5py
    import scipy as scp
    import scipy.stats 
    from matplotlib import pyplot as plt
    import seaborn as sns
    import pandas as pd
    
    ilastik_nuc_obj = h5py.File(input_ilastik_nuc_obj, 'r')['exported_data'][()][:,0,:,:]
    ilastik_nucXi_obj = h5py.File(input_ilastik_xi_obj, 'r')['exported_data'][()][:,0,:,:]
    Xi_all=np.unique(ilastik_nucXi_obj)[np.unique(ilastik_nucXi_obj)>1]
    counter=0
    print(Xi_all)
    if len(Xi_all)>0:
        for n in np.arange(0,len(Xi_all)):
            counter+=1
            Xi_n=Xi_all[n]
            nuc_n=np.unique(ilastik_nuc_obj[ilastik_nucXi_obj==Xi_n])[0]
            ## nuc mask
            nuc_mask=ilastik_nuc_obj.copy()
            nuc_mask[nuc_mask!=nuc_n] = 0
            nuc_mask[nuc_mask!=0] = 255
            skio.imsave(out_prefix + "_BEFORE_nuc" +str(counter) + ".tif", nuc_mask[0,:,:], plugin='tifffile',imagej=True, append=False)
            skio.imsave(out_prefix + "_AFTER_nuc" +str(counter) + ".tif", nuc_mask[1,:,:], plugin='tifffile',imagej=True, append=False)
            ## Xi masks
            Xi_mask=ilastik_nucXi_obj.copy()
            Xi_mask[Xi_mask!=Xi_n] = 0
            Xi_mask[Xi_mask!=0] = 255
            skio.imsave(out_prefix + "_BEFORE_nuc" +str(counter) + "_Xi.tif", Xi_mask[0,:,:], plugin='tifffile',imagej=True, append=False)
            skio.imsave(out_prefix + "_AFTER_nuc" +str(counter) + "_Xi.tif", Xi_mask[1,:,:], plugin='tifffile',imagej=True, append=False)
            ### write info
            f= open(str(out_prefix)+'.csv',"a+")
            f.write("{}\t{}\t{}\n".format(dataset,sample,'nuc'+str(counter)))
            f.close

    

if __name__ == "__main__":
    main()
