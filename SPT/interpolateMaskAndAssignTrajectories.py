'''
interpolateMaskAndAssignTrajectories.py

Given two masks defined at two frames in a tracking movie,
interpolate the shape of the mask at every intermediate frame.
Then for a file containing trajectories, assign each trajectory
to the mask if the trajectory coincides with the interpolated
mask for that frame.

'''
from interpolate_mask_utilities_sam import interpolateMaskTIF, assign_trajectories_to_mask
import os
import argparse
import time
import sys
from scipy import io as sio 
from pyspaz import spazio 
import numpy as np

def pdistmean(m):
    dists=[]
    for n in range(len(m)-1):
        dists.append(((m[n,0]-m[n+1,0])**2+(m[n,1]-m[n+1,1])**2)**0.5)
    return np.mean(dists)

def FilterAsignedTrajectories(
    trajectory_file,
    AssigmentTable,
    olap_rule,
    olap_fracMin = 0,
    olap_fracMax = 1,
    min_msrd=999,
    outFile=None
):
    sys.stdout.write(time.strftime("-"*50 + "\n------ %Y-%m-%d %H:%M:%S Filtering Trajectories.\n"))
    sys.stdout.flush()
    trajs, metadata, traj_cols = spazio.load_trajs(trajectory_file)
        
    ### select on fracMin
    trajs_selected = trajs[AssigmentTable['Frac_inMask']>=olap_fracMin]
    AssigmentTable_selected = AssigmentTable[AssigmentTable['Frac_inMask']>=olap_fracMin]
    ### select on fracMax
    trajs_selected = trajs_selected[AssigmentTable_selected['Frac_inMask']>=olap_fracMin]
    AssigmentTable_selected = AssigmentTable_selected[AssigmentTable_selected['Frac_inMask']>=olap_fracMin]
    if olap_rule == "all":
        trajs_selected = trajs_selected[AssigmentTable_selected['All_inMask']==1]
        AssigmentTable_selected = AssigmentTable_selected[AssigmentTable_selected['All_inMask']==1]
    if olap_rule == "any":
        trajs_selected = trajs_selected[AssigmentTable_selected['Any_inMask']==1]
        AssigmentTable_selected = AssigmentTable_selected[AssigmentTable_selected['Any_inMask']==1]
    if olap_rule == "first":
        trajs_selected = trajs_selected[AssigmentTable_selected['First_inMask']==1]
        AssigmentTable_selected = AssigmentTable_selected[AssigmentTable_selected['First_inMask']==1]
    if olap_rule == "last":
        trajs_selected = trajs_selected[AssigmentTable_selected['Last_inMask']==1]
        AssigmentTable_selected = AssigmentTable_selected[AssigmentTable_selected['Last_inMask']==1]
    if olap_rule == "none":
        trajs_selected = trajs_selected[AssigmentTable_selected['Any_inMask']==0]
        AssigmentTable_selected = AssigmentTable_selected[AssigmentTable_selected['Any_inMask']==0]
    if olap_rule == "enter":
        trajs_selected = trajs_selected[(AssigmentTable_selected['First_inMask']==0) & (AssigmentTable_selected['Any_inMask']==1)]
        AssigmentTable_selected = AssigmentTable_selected[(AssigmentTable_selected['First_inMask']==0) & (AssigmentTable_selected['Any_inMask']==1)]
    if olap_rule == "exit":
        trajs_selected = trajs_selected[(AssigmentTable_selected['First_inMask']==1) & (AssigmentTable_selected['All_inMask']==0)]
        AssigmentTable_selected = AssigmentTable_selected[(AssigmentTable_selected['First_inMask']==1) & (AssigmentTable_selected['All_inMask']==0)]
    ## save files
    print(str(len(trajs_selected)) + ' trajectories assigned')
    if outFile:
        trajs_selected_MSRD_list=[]
        for i in range(len(trajs_selected)):
            trajs_selected_MSRD_list.append(pdistmean(trajs_selected[i][0]))
        
        trajs_selected_minMSRD= trajs_selected[np.array(trajs_selected_MSRD_list)>=min_msrd]
        
        if len(trajs_selected_minMSRD)>0:
            sys.stdout.write(time.strftime("-"*50 + "\n----- %Y-%m-%d %H:%M:%S exporting selected trajectories.\n"))
            sys.stdout.flush()
            out_dict = {
                'trackedPar' : trajs_selected_minMSRD,
                'metadata' : spazio.format_metadata_out(metadata),
                'traj_cols' : traj_cols,
            }
            sio.savemat(outFile, out_dict)
    #
    return trajs_selected_minMSRD

def interpolateMaskAndAssignTrajectories(
    mask_0_tif,
    mask_1_tif,
    mask_0_frame_index,
    mask_1_frame_index,
    trajectory_file,
    outPrefix = None,
    plot_mask = False,
    save_table = False,
    pixel_subsampling_factor = 1,
    pixel_size_um=0.16
):
    if plot_mask == True:
        plot_file = outPrefix + '_mask_interpolator.gif'
    else:
        plot_file = None
    
    if save_table == True:
        csv_file = outPrefix + '_mask_assignTable.csv'
    else:
        csv_file = None
    
    #### interpolate mask through frames
    interpolator = interpolateMaskTIF(
        mask_0_tif,
        mask_1_tif,
        mask_0_frame_index,
        mask_1_frame_index,
        plot_file = plot_file,
        pixel_subsampling_factor = pixel_subsampling_factor,
        pixel_size_um=pixel_size_um
    )
    #### assign trajectories to interpolated mask
    trajs_olapMask_df = assign_trajectories_to_mask(
        trajectory_file,
        interpolator,
        csv_file = csv_file,
        pixel_size_um=pixel_size_um
    )
    return trajs_olapMask_df

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description = 'assign trajectories to a two-dimensional interpolated mask'
    )
    parser.add_argument(
        "-i",
        '--traj_file_or_dir',
        type = str,
        help = '*Tracked.mat file with trajectories, or directory containing *Tracked.mat files',
    )
    parser.add_argument(
        '--mask_0_tif',
        type = str,
        help = 'TIF containing the first mask',
    )
    parser.add_argument(
        '--mask_1_tif',
        type = str,
        help = 'TIF containing the second mask',
    )
    parser.add_argument(
        '--mask_0_frame_index',
        type = int,
        help = 'frame index corresponding to the first mask',
    )
    parser.add_argument(
        '--mask_1_frame_index',
        type = int,
        help = 'frame index corresponding to the second mask'
    )
    parser.add_argument(
        '--olap_rule',
        type = str,
        choices = ['all','any','frac', 'first', 'last', 'none','enter','exit'],
        help = 'Rule to select trajectories filtering.',
    )
    parser.add_argument(
        '--olap_fracMin',
        type = float,
        help = 'Fraction of trajectory\'s localisations in the mask is higher or equal to fracMin. Only apply if olap_rule if frac',
        default = 0
    )
    parser.add_argument(
        '--olap_fracMax',
        type = float,
        help = 'Fraction of trajectory\'s localisations in the mask is lower or equal to fracMax. Only apply if olap_rule if frac',
        default = 1
    )
    parser.add_argument(
        "-o",
        '--outPrefix',
        type = str,
        help = 'Path and prefix to which output files will be saved. by default only the filtered trajectories will be saved as <outPrefix>.mat',
    )
    parser.add_argument(
        '-s',
        '--pixel_subsampling_factor',
        type = int,
        help = 'pixel density of the mask relative to the trajectory movie, for masks defined on the subpixel level. Default 1 (each pixel in the trajectory corresponds to one pixel in the mask)',
        default = 1
    )
    parser.add_argument(
        '--pixel_size_um',
        type = float,
        help = 'size of pixels in um',
        default = 0.16
    )
    parser.add_argument(
        '--save_table',
        action = 'store_true',
        help = 'Wheter to save the table of trajectories assignment table to <outPrefix>.csv . Each line correspond to each trajectories in the original trajectory files, in the same order',
        default = True
    )
    parser.add_argument(
        '-p',
        '--plot_mask',
        action = 'store_true',
        help = 'plot the 2D interpolated mask at every frame. Only feasible for < 200 frames total',
        default = False
    )
    parser.add_argument(
        '--min_msrd',
        type = float,
        help = 'minimum mean suqare root displacement (in microns) of trajectories to keep. ',
        default = 999
    )
    args = parser.parse_args()
    #if os.path.isdir(args.traj_file_or_dir):
        #trajectory_files = ['%s/%s' % (args.traj_file_or_dir, i) for i in os.listdir(args.traj_file_or_dir)]
        #for traj_file_idx, trajectory_file in enumerate(trajectory_files):
    #elif os.path.isfile(args.traj_file_or_dir):
        #trajectory_files = [args.traj_file_or_dir]
    #else:
        #print('Could not find trajectory file/directory %s' % args.traj_file_or_dir)
        #exit(1)
    trajectory_file = args.traj_file_or_dir

    sys.stdout.write(args.outPrefix)
    sys.stdout.flush()

    mask_0_tif = args.mask_0_tif
    mask_1_tif = args.mask_1_tif
    mask_0_frame_index = args.mask_0_frame_index
    mask_1_frame_index = args.mask_1_frame_index
    trajectory_file = trajectory_file
    outPrefix = args.outPrefix
    pixel_subsampling_factor = args.pixel_subsampling_factor
    save_table = args.save_table
    plot_mask = args.plot_mask
    pixel_size_um=args.pixel_size_um
    min_msrd=args.min_msrd



    AssigmentTable = interpolateMaskAndAssignTrajectories(
        mask_0_tif = mask_0_tif,
        mask_1_tif = mask_1_tif,
        mask_0_frame_index = mask_0_frame_index,
        mask_1_frame_index = mask_1_frame_index,
        trajectory_file = trajectory_file,
        outPrefix = outPrefix,
        pixel_subsampling_factor = pixel_subsampling_factor,
        save_table = save_table,
        plot_mask = plot_mask,
        pixel_size_um = pixel_size_um
    )
    FilterAsignedTrajectories(
        trajectory_file = trajectory_file,
        AssigmentTable=AssigmentTable,
        olap_rule=args.olap_rule,
        olap_fracMin = args.olap_fracMin,
        olap_fracMax = args.olap_fracMax,
        min_msrd=min_msrd,
        outFile= args.outPrefix + "_" + str(args.olap_rule) + "_Tracked.mat"
    )
