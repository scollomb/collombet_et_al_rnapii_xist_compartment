## source activate ~/programs/condaEnv/r3.4.2 ; R
setwd("~/Lab_Heard/project_SC_XiComp/SPT/Polr2a/sc18c7/Dox24h_PAJF646")
library(reshape2)
library(RColorBrewer)
library(ggplot2)
library(gridExtra)
library(rtracklayer)
library(Hmisc)
library(lmtest)
library(fitdistrplus)
library(tidyr)

dir.create('plots')

XinXout_colours = c('Xi'="#b2182b",'nuc'='#2166ac')
XinShift_colours = c('Xi'="#b2182b",'shift'='#2166ac')


### dsitributions of counts
Xi_enter_shift_MSRD <- read.table('analysis_shiftXi/traj_ROE_any_ROI_enter_shiftMask_MSRD_all.tsv', header=T, sep='\t', stringsAsFactors=F)
colnames(Xi_enter_shift_MSRD)[5] <- 'mask'
Xi_enter_shift_MSRD$ID=paste0(Xi_enter_shift_MSRD$datasetName, "_", Xi_enter_shift_MSRD$sample, "_", Xi_enter_shift_MSRD$ROE, "_", Xi_enter_shift_MSRD$mask)
Xi_enter_shift_MSRD$cell=paste0(Xi_enter_shift_MSRD$datasetName, "_", Xi_enter_shift_MSRD$sample, "_", Xi_enter_shift_MSRD$ROE)
Xi_enter_shift_MSRD$regionType<-'shift'
Xi_enter_shift_MSRD$regionType[Xi_enter_shift_MSRD$mask=='original'] <- 'Xi'

Xi_enter_shift_MSRD_clean <- Xi_enter_shift_MSRD[,c('ID','cell','mask','regionType','MSRDsup200nm')]
colnames(Xi_enter_shift_MSRD_clean) <- c('ID','cell','mask','regionType','count')
write.table(Xi_enter_shift_MSRD_clean, file='Xi_enter_shift_MSRD_clean.txt', quote=F, sep="\t",row.names = F)

with(Xi_enter_shift_MSRD_clean, tapply(count, regionType, function(x) {
    sprintf("mean=%1.2f - var=%1.2f", mean(x), var(x))
}))


fitNB_Xi <- fitdist(Xi_enter_shift_MSRD_clean$count[Xi_enter_shift_MSRD_clean$regionType=='Xi'],"nbinom")
fitNB_shift <- fitdist(Xi_enter_shift_MSRD_clean$count[Xi_enter_shift_MSRD_clean$regionType=='shift'],"nbinom")

Xi_enter_shift_MSRD_clean[Xi_enter_shift_MSRD_clean$mask=='original',]

plot <- ggplot(data=Xi_enter_shift_MSRD_clean)
plot <- plot + geom_histogram(aes(y=..density.., fill=regionType, group=regionType, x=count),position="identity", alpha=0.5, binwidth=2)
plot <- plot + scale_fill_manual(values=XinShift_colours)
plot <- plot + stat_function(data=data.frame(x=c(0, 50)), aes(x),n=length(0:50),  fun=dnbinom, args=list(size=fitNB_Xi$estimate['size'], mu=fitNB_Xi$estimate['mu']), colour=XinShift_colours['Xi'], geom='line')
plot <- plot + stat_function(data=data.frame(x=c(0, 50)), aes(x),n=length(0:50),  fun=dnbinom, args=list(size=fitNB_shift$estimate['size'], mu=fitNB_shift$estimate['mu']), colour=XinShift_colours['shift'], geom='line')
plot <- plot + coord_cartesian(ylim=c(0,0.15), xlim=c(0,40), expand=F)
plot <- plot + theme(axis.text.x = element_text(angle = 90, hjust = 1),
    plot.title = element_text(size = 6),
	axis.line.x=element_line(colour="black"), axis.line.y=element_line(colour="black"),panel.background=element_blank(), panel.grid.major=element_blank(), panel.grid.minor=element_blank())
ggsave(plot, file=paste0('plots/Xi_enter_shift_distributions_MSRDsup200.pdf'), useDingbats=FALSE)


plot <- ggplot(data=Xi_enter_shift_MSRD_clean)
plot <- plot + stat_ecdf(geom = "step", aes(colour=regionType, group=regionType, x=count), lwd=1)
plot <- plot + scale_colour_manual(values=XinShift_colours)
plot <- plot + stat_function(data=data.frame(x=c(0, 50)), aes(x),n=length(0:50),  fun=cumsum(dnbinom), args=list(size=fitNB_Xi$estimate['size'], mu=fitNB_Xi$estimate['mu']), colour=XinShift_colours['Xi'], geom='line')
plot <- plot + stat_function(data=data.frame(x=c(0, 50)), aes(x),n=length(0:50),  fun=cumsum(dnbinom), args=list(size=fitNB_shift$estimate['size'], mu=fitNB_shift$estimate['mu']), colour=XinShift_colours['shift'], geom='line')
plot <- plot + coord_cartesian(ylim=c(0,1), xlim=c(0,40), expand=F)
plot <- plot + theme(axis.text.x = element_text(angle = 90, hjust = 1),
    plot.title = element_text(size = 6),
	axis.line.x=element_line(colour="black"), axis.line.y=element_line(colour="black"),panel.background=element_blank(), panel.grid.major=element_blank(), panel.grid.minor=element_blank())
ggsave(plot, file=paste0('plots/Xi_enter_shift_CDF_MSRDsup200.pdf'), useDingbats=FALSE)

ks.test(Xi_enter_shift_MSRD_clean[Xi_enter_shift_MSRD_clean$regionType=='Xi','count'], Xi_enter_shift_MSRD_clean[Xi_enter_shift_MSRD_clean$regionType=='shift','count'])

## bootstrap distrib
fc=function(d,cell,region,c){
    cells=unique(d$cell)[cell]
    regions=1.0*length(d[d$regionType==region & d$cell %in% cells,'count'])
    counts=1.0*sum(d[d$regionType==region & d$cell %in% cells,'count']==c)
    return(counts/regions)
}
bootcorr <- boot(Xi_enter_shift_MSRD_clean, fc, R=500, region='shift',c=5)
bootcorr$t0
mean(bootcorr$t)
a=boot.ci(boot.out = bootcorr, type="norm")
a


#### GLM
glm_nb <- glm.nb(count ~ cell + regionType, data = Xi_enter_shift_MSRD_clean)
glm_nb_ct <- update(glm_nb, . ~ . - regionType)
anova(glm_nb, glm_nb_ct)


###### shift masks - trajectory entering
### mean counts of shifts
sum(Xi_enter_shift_table[,'all'])
Xi_enter_shift_table <- read.table('analysis_shiftXi/traj_ROE_any_ROI_enter_shiftMask_MSRD_meanOfShifts_all.tsv', header=T, sep='\t')
Xi_enter_shift_table$ID=paste0(Xi_enter_shift_table$datasetName, "_", Xi_enter_shift_table$sample, "_", Xi_enter_shift_table$ROE)

Xi_enter_shift_table_ori_melt <- melt(Xi_enter_shift_table[,c('ID','all','MSRDinf100nm','MSRD100to200nm','MSRDsup200nm')], id=c("ID"))
Xi_enter_shift_table_ori_melt$region= 'Xi'
Xi_enter_shift_table_shift_melt <- melt(Xi_enter_shift_table[,c('ID','shiftall','shiftMSRDinf100nm','shiftMSRD100to200nm','shiftMSRDsup200nm')], id=c("ID"))
Xi_enter_shift_table_shift_melt$region= 'nuc'
Xi_enter_shift_table_shift_melt$variable = Xi_enter_shift_table_ori_melt$variable
Xi_enter_shift_table_melt=rbind(Xi_enter_shift_table_ori_melt,Xi_enter_shift_table_shift_melt)
Xi_enter_shift_table_melt$class=paste0(Xi_enter_shift_table_melt$variable, "_",Xi_enter_shift_table_melt$region)

Xi_enter_shift_table_melt[Xi_enter_shift_table_melt$class=='MSRDsup200nm_Xi','value']
Xi_enter_shift_table_melt[Xi_enter_shift_table_melt$class=='MSRDsup200nm_nuc','value']
wilcox.test(Xi_enter_shift_table_melt[Xi_enter_shift_table_melt$class=='MSRDsup200nm_Xi','value'], Xi_enter_shift_table_melt[Xi_enter_shift_table_melt$class=='MSRDsup200nm_nuc','value'], paired=T)

plot <- ggplot(data=Xi_enter_shift_table, aes(y=MSRDsup200nm, x=shiftMSRDsup200nm, colour=datasetName))
plot <- plot + geom_point()
plot <- plot + geom_abline()
plot <- plot + theme(axis.text.x = element_text(angle = 90, hjust = 1), aspect.ratio=1,
    plot.title = element_text(size = 6),
	axis.line.x=element_line(colour="black"), axis.line.y=element_line(colour="black"),panel.background=element_blank(), panel.grid.major=element_blank(), panel.grid.minor=element_blank())
ggsave(plot, file=paste0('plots/Xi_enter_shift_vs_Xi.pdf'), useDingbats=FALSE)






Xi_enter_shiftMoreCenter_table <- read.table('analysis_shiftXi/traj_ROE_any_ROI_enter_shiftMask_moreCenter_MSRD_meanOfShifts_all.tsv', header=T, sep='\t')
Xi_enter_shiftMoreCenter_table$ID=paste0(Xi_enter_shiftMoreCenter_table$datasetName, "_", Xi_enter_shiftMoreCenter_table$sample, "_", Xi_enter_shiftMoreCenter_table$ROE)

Xi_enter_shiftMoreCenter_table_ori_melt <- melt(Xi_enter_shiftMoreCenter_table[,c('ID','all','MSRDinf100nm','MSRD100to200nm','MSRDsup200nm')], id=c("ID"))
Xi_enter_shiftMoreCenter_table_ori_melt$region= 'Xi'
Xi_enter_shiftMoreCenter_table_shiftMoreCenter_melt <- melt(Xi_enter_shiftMoreCenter_table[,c('ID','shiftall','shiftMSRDinf100nm','shiftMSRD100to200nm','shiftMSRDsup200nm')], id=c("ID"))
Xi_enter_shiftMoreCenter_table_shiftMoreCenter_melt$region= 'nuc'
Xi_enter_shiftMoreCenter_table_shiftMoreCenter_melt$variable = Xi_enter_shiftMoreCenter_table_ori_melt$variable
Xi_enter_shiftMoreCenter_table_melt=rbind(Xi_enter_shiftMoreCenter_table_ori_melt,Xi_enter_shiftMoreCenter_table_shiftMoreCenter_melt)
Xi_enter_shiftMoreCenter_table_melt$class=paste0(Xi_enter_shiftMoreCenter_table_melt$variable, "_",Xi_enter_shiftMoreCenter_table_melt$region)

plot <- ggplot(data=Xi_enter_shiftMoreCenter_table_melt[Xi_enter_shiftMoreCenter_table_melt$variable!='all',], aes(y=value, x=class, colour=region))
plot <- plot + geom_hline(yintercept=0, linetype='dashed')
plot <- plot + geom_boxplot(fill=NA, outlier.colour=NA)
plot <- plot + scale_colour_manual(values=XinXout_colours)
plot <- plot + geom_jitter(aes(fill=region),width=0.2, pch=21, colour='white')
plot <- plot + scale_fill_manual(values=XinXout_colours)
# plot <- plot + coord_cartesian(ylim=c(-1,1))
plot <- plot + theme(axis.text.x = element_text(angle = 90, hjust = 1),
    plot.title = element_text(size = 6),
	axis.line.x=element_line(colour="black"), axis.line.y=element_line(colour="black"),panel.background=element_blank(), panel.grid.major=element_blank(), panel.grid.minor=element_blank())
ggsave(plot, file=paste0('plots/Xi_enter_shiftMoreCenter_trajCounts_boxplot.pdf'), useDingbats=FALSE)

plot <- ggplot(data=Xi_enter_shiftMoreCenter_table, aes(y=shiftMSRDsup200nm, x=shiftN, colour=datasetName))
plot <- plot + geom_point()
plot <- plot + theme(axis.text.x = element_text(angle = 90, hjust = 1),
    plot.title = element_text(size = 6),
	axis.line.x=element_line(colour="black"), axis.line.y=element_line(colour="black"),panel.background=element_blank(), panel.grid.major=element_blank(), panel.grid.minor=element_blank())
ggsave(plot, file=paste0('plots/Xi_enter_shiftMoreCenter_shiftN_vs_trajMsrdSup200.pdf'), useDingbats=FALSE)

plot <- ggplot(data=Xi_enter_shiftMoreCenter_table, aes(y=shiftMSRDsup200nm, x=MSRDsup200nm, colour=datasetName))
plot <- plot + geom_point()
plot <- plot + geom_abline()
plot <- plot + theme(axis.text.x = element_text(angle = 90, hjust = 1), aspect.ratio=1,
    plot.title = element_text(size = 6),
	axis.line.x=element_line(colour="black"), axis.line.y=element_line(colour="black"),panel.background=element_blank(), panel.grid.major=element_blank(), panel.grid.minor=element_blank())
ggsave(plot, file=paste0('plots/Xi_enter_shiftMoreCenter_vs_Xi.pdf'), useDingbats=FALSE)

