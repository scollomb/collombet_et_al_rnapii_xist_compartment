'''
interpolateMaskTiffAndAssignTrajectories.py

Given two masks defined at two frames in a tracking movie,
interpolate the shape of the mask at every intermediate frame.
Then for a file containing trajectories, assign each trajectory
to the mask if the trajectory coincides with the interpolated
mask for that frame.

'''
from interpolate_mask_utilities_sam import interpolateMaskNpArray, interpolateMaskTIF, assign_trajectories_to_mask
import os
import argparse
import time
import sys
import scipy
from scipy import ndimage
from scipy import io as sio 
from pyspaz import spazio 
import numpy as np 
from numpy import random
import matplotlib.pyplot as plt 
from matplotlib.path import Path
import pandas as pd 
from PIL import Image 
import time
import cv2
import skimage.io as skio

def pdistmean(m):
    dists=[]
    for n in range(len(m)-1):
        dists.append(((m[n,0]-m[n+1,0])**2+(m[n,1]-m[n+1,1])**2)**0.5)
    return np.mean(dists)

def FilterAsignedTrajectories(
    trajectory_file,
    AssigmentTable,
    olap_rule,
    olap_fracMin = 0,
    olap_fracMax = 1,
    min_msrd=999,
    outFile=None,
    verbose=0
):
    if verbose>0:
        sys.stdout.write(time.strftime("-"*50 + "\n------ %Y-%m-%d %H:%M:%S Filtering Trajectories.\n")); sys.stdout.flush()
    trajs, metadata, traj_cols = spazio.load_trajs(trajectory_file)
    
    ### select on fracMin
    trajs_selected = trajs[AssigmentTable['Frac_inMask']>=olap_fracMin]
    AssigmentTable_selected = AssigmentTable[AssigmentTable['Frac_inMask']>=olap_fracMin]
    ### select on fracMax
    trajs_selected = trajs_selected[AssigmentTable_selected['Frac_inMask']>=olap_fracMin]
    AssigmentTable_selected = AssigmentTable_selected[AssigmentTable_selected['Frac_inMask']>=olap_fracMin]
    if olap_rule == "all":
        trajs_selected = trajs_selected[AssigmentTable_selected['All_inMask']==1]
        AssigmentTable_selected = AssigmentTable_selected[AssigmentTable_selected['All_inMask']==1]
    if olap_rule == "any":
        trajs_selected = trajs_selected[AssigmentTable_selected['Any_inMask']==1]
        AssigmentTable_selected = AssigmentTable_selected[AssigmentTable_selected['Any_inMask']==1]
    if olap_rule == "first":
        trajs_selected = trajs_selected[AssigmentTable_selected['First_inMask']==1]
        AssigmentTable_selected = AssigmentTable_selected[AssigmentTable_selected['First_inMask']==1]
    if olap_rule == "last":
        trajs_selected = trajs_selected[AssigmentTable_selected['Last_inMask']==1]
        AssigmentTable_selected = AssigmentTable_selected[AssigmentTable_selected['Last_inMask']==1]
    if olap_rule == "none":
        trajs_selected = trajs_selected[AssigmentTable_selected['Any_inMask']==0]
        AssigmentTable_selected = AssigmentTable_selected[AssigmentTable_selected['Any_inMask']==0]
    if olap_rule == "enter":
        trajs_selected = trajs_selected[(AssigmentTable_selected['First_inMask']==0) & (AssigmentTable_selected['Any_inMask']==1)]
        AssigmentTable_selected = AssigmentTable_selected[(AssigmentTable_selected['First_inMask']==0) & (AssigmentTable_selected['Any_inMask']==1)]
    if olap_rule == "exit":
        trajs_selected = trajs_selected[(AssigmentTable_selected['First_inMask']==1) & (AssigmentTable_selected['All_inMask']==0)]
        AssigmentTable_selected = AssigmentTable_selected[(AssigmentTable_selected['First_inMask']==1) & (AssigmentTable_selected['All_inMask']==0)]
    ## save files
    ## save files
    print(str(len(trajs_selected)) + ' trajectories assigned')

    if len(trajs_selected)>0:
        trajs_selected_MSRD_list=[]
        for i in range(len(trajs_selected)):
            trajs_selected_MSRD_list.append(pdistmean(trajs_selected[i][0]))
        
        trajs_selected_minMSRD = trajs_selected[np.array(trajs_selected_MSRD_list)>=min_msrd]
        
        if outFile:
            if len(trajs_selected_minMSRD)>0:
                sys.stdout.write(time.strftime("-"*50 + "\n----- %Y-%m-%d %H:%M:%S exporting selected trajectories.\n"))
                sys.stdout.flush()
                out_dict = {
                    'trackedPar' : trajs_selected_minMSRD,
                    'metadata' : spazio.format_metadata_out(metadata),
                    'traj_cols' : traj_cols,
                }
                sio.savemat(outFile, out_dict)
        #
        return trajs_selected_minMSRD

def pdistmean(m):
    dists=[]
    for n in range(len(m)-1):
        dists.append(((m[n,0]-m[n+1,0])**2+(m[n,1]-m[n+1,1])**2)**0.5)
    return np.mean(dists)


def interpolateMaskAndAssignTrajectories(
    mask_0,
    mask_1,
    mask_0_frame_index,
    mask_1_frame_index,
    trajectory_file,
    outPrefix = None,
    plot_mask = False,
    save_table = False,
    pixel_subsampling_factor = 1,
    pixel_size_um=0.16
):
    if plot_mask == True:
        plot_file = outPrefix + '_mask_interpolator.gif'
    else:
        plot_file = None
    
    if save_table == True:
        csv_file = outPrefix + '_mask_assignTable.csv'
    else:
        csv_file = None
    
    #### interpolate mask through frames
    interpolator = interpolateMaskNpArray(
        mask_0,
        mask_1,
        mask_0_frame_index,
        mask_1_frame_index,
        plot_file = plot_file,
        pixel_subsampling_factor = pixel_subsampling_factor,
        pixel_size_um=pixel_size_um,
        verbose=0
    )
    #### assign trajectories to interpolated mask
    trajs_olapMask_df = assign_trajectories_to_mask(
        trajectory_file,
        interpolator,
        pixel_size_um=pixel_size_um,
        verbose=0
    )
    return trajs_olapMask_df

def center_mask(mask):
    mask_8bit = np.uint8(mask*255)
    height, width = mask_8bit.shape
    x,y,w,h = cv2.boundingRect(mask_8bit)
    # Create new blank image and shift ROI to new coordinates
    mask_8bit_center = np.zeros(mask_8bit.shape, dtype=np.uint8)
    ROI = mask_8bit[y:y+h, x:x+w]
    x = width//2 - ROI.shape[0]//2 
    y = height//2 - ROI.shape[1]//2 
    mask_8bit_center[y:y+h, x:x+w] = ROI
    mask_center=np.float32(mask_8bit_center)
    mask_center[np.where(mask_center>0)] = 1
    return(mask_center)

def rotate_mask(mask, angle):
    mask_8bit = np.uint8(mask*255)
    height, width = mask_8bit.shape
    x,y,w,h = cv2.boundingRect(mask_8bit)
    
    # Create new blank image and shift ROI to new coordinates
    mask_8bit_center = np.zeros(mask_8bit.shape, dtype=np.uint8)
    ROI = mask_8bit[y:y+h, x:x+w]
    x = width//2 - ROI.shape[0]//2 
    y = height//2 - ROI.shape[1]//2 
    mask_8bit_center[y:y+h, x:x+w] = ROI
    
    mask_8bit_center_im=Image.fromarray(mask_8bit_center)
    mask_8bit_center_im_rotate = mask_8bit_center_im.rotate(angle)
    mask_8bit_center_rotate=np.float32(mask_8bit_center_im_rotate)
    return(mask_8bit_center_rotate)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description = 'assign trajectories to a two-dimensional interpolated mask'
    )
    parser.add_argument(
        "-i",
        '--traj_file_or_dir',
        type = str,
        help = '*Tracked.mat file with trajectories, or directory containing *Tracked.mat files',
    )
    parser.add_argument(
        '--mask_ROE_0_file',
        type = str,
        help = 'TIF containing the first mask',
    )
    parser.add_argument(
        '--mask_ROE_1_file',
        type = str,
        help = 'TIF containing the second mask',
    )
    parser.add_argument(
        '--mask_ROI_0_file',
        type = str,
        help = 'TIF containing the first mask',
    )
    parser.add_argument(
        '--mask_ROI_1_file',
        type = str,
        help = 'TIF containing the second mask',
    )
    parser.add_argument(
        '--mask_0_frame_index',
        type = int,
        help = 'frame index corresponding to the first mask',
    )
    parser.add_argument(
        '--mask_1_frame_index',
        type = int,
        help = 'frame index corresponding to the second mask'
    )
    parser.add_argument(
        '--olap_rule',
        type = str,
        choices = ['all','any','frac', 'first', 'last', 'none', 'enter', 'exit'],
        help = 'Rule to select trajectories filtering.',
    )
    parser.add_argument(
        '--olap_fracMin',
        type = float,
        help = 'Fraction of trajectory\'s localisations in the mask is higher or equal to fracMin. Only apply if olap_rule if frac',
        default = 0
    )
    parser.add_argument(
        '--olap_fracMax',
        type = float,
        help = 'Fraction of trajectory\'s localisations in the mask is lower or equal to fracMax. Only apply if olap_rule if frac',
        default = 1
    )
    parser.add_argument(
        "-o",
        '--outPrefix',
        type = str,
        help = 'Path and prefix to which output files will be saved. by default only the filtered trajectories will be saved as <outPrefix>.mat',
    )
    parser.add_argument(
        '-s',
        '--pixel_subsampling_factor',
        type = int,
        help = 'pixel density of the mask relative to the trajectory movie, for masks defined on the subpixel level. Default 1 (each pixel in the trajectory corresponds to one pixel in the mask)',
        default = 1
    )
    parser.add_argument(
        '--pixel_size_um',
        type = float,
        help = 'size of pixels in um',
        default = 0.16
    )
    parser.add_argument(
        '--datasetName',
        type = str,
        help = '',
    )
    parser.add_argument(
        '--sample',
        type = str,
        help = '',
    )
    parser.add_argument(
        '--ROE',
        type = str,
        help = '',
    )
    parser.add_argument(
        '--ROI',
        type = str,
        help = '',
    )
    parser.add_argument(
        '--maxiters',
        type = int,
        help = 'maximum number of iterations',
        default = 10000
    )
    parser.add_argument(
        '--maxmasks',
        type = int,
        help = 'maximum number of masks',
        default = 100
    )
    parser.add_argument(
        '--maxMasksOlap',
        type = float,
        help = '',
        default =0.2 
    )
    parser.add_argument(
        '--maxMaskFracOutsideROE',
        type = float,
        help = '',
        default = 0.01
    )
    parser.add_argument(
        '--minMaskDistRoeDifFrac',
        type = float,
        help = '',
        default = -0.1
    )
    parser.add_argument(
        '--maxMaskDistRoeDifFrac',
        type = float,
        help = '',
        default = 0.1
    )
    parser.add_argument(
        '--min_msrd',
        type = float,
        help = 'minimum mean suqare root displacement (in microns) of trajectories to keep. ',
        default = 999
    )
    
    args = parser.parse_args()
    #if os.path.isdir(args.traj_file_or_dir):
        #trajectory_files = ['%s/%s' % (args.traj_file_or_dir, i) for i in os.listdir(args.traj_file_or_dir)]
        #for traj_file_idx, trajectory_file in enumerate(trajectory_files):
    #elif os.path.isfile(args.traj_file_or_dir):
        #trajectory_files = [args.traj_file_or_dir]
    #else:
        #print('Could not find trajectory file/directory %s' % args.traj_file_or_dir)
        #exit(1)
    trajectory_file = args.traj_file_or_dir

    sys.stdout.write(args.outPrefix)
    sys.stdout.flush()
    
    mask_ROE_0_file = args.mask_ROE_0_file
    mask_ROE_1_file = args.mask_ROE_1_file
    mask_ROI_0_file = args.mask_ROI_0_file
    mask_ROI_1_file = args.mask_ROI_1_file
    mask_0_frame_index = args.mask_0_frame_index
    mask_1_frame_index = args.mask_1_frame_index
    trajectory_file = trajectory_file
    outPrefix = args.outPrefix
    pixel_subsampling_factor = args.pixel_subsampling_factor
    pixel_size_um=args.pixel_size_um
    olap_rule=args.olap_rule
    olap_fracMin=args.olap_fracMin
    olap_fracMax=args.olap_fracMax
    datasetName=args.datasetName
    sample=args.sample
    ROE=args.ROE
    ROI=args.ROI
    maxiters=args.maxiters
    maxmasks=args.maxmasks
    max_mask_olap=args.maxMasksOlap
    max_mask_outsideROE=args.maxMaskFracOutsideROE
    min_dist_ROE_difFrac=args.minMaskDistRoeDifFrac
    max_dist_ROE_difFrac=args.maxMaskDistRoeDifFrac
    min_msrd=args.min_msrd
        
    #mask_ROE_0_file='/home/scollomb/Lab_Heard/project_SC_XiComp/SPT/Polr2a/sc18c7/Dox24h_PAJF646/191204_SPT_sc18c7_Dox24h_PAJF646_50nM_30min/masks/pos10_Xist_BEFORE_nuc1.tif'
    #mask_ROE_1_file='/home/scollomb/Lab_Heard/project_SC_XiComp/SPT/Polr2a/sc18c7/Dox24h_PAJF646/191204_SPT_sc18c7_Dox24h_PAJF646_50nM_30min/masks/pos10_Xist_AFTER_nuc1.tif'
    #mask_ROI_0_file='/home/scollomb/Lab_Heard/project_SC_XiComp/SPT/Polr2a/sc18c7/Dox24h_PAJF646/191204_SPT_sc18c7_Dox24h_PAJF646_50nM_30min/masks/pos10_Xist_BEFORE_nuc1_Xi.tif'
    #mask_ROI_1_file='/home/scollomb/Lab_Heard/project_SC_XiComp/SPT/Polr2a/sc18c7/Dox24h_PAJF646/191204_SPT_sc18c7_Dox24h_PAJF646_50nM_30min/masks/pos10_Xist_AFTER_nuc1_Xi.tif'
    #trajectory_file='/home/scollomb/Lab_Heard/project_SC_XiComp/SPT/Polr2a/sc18c7/Dox24h_PAJF646/191204_SPT_sc18c7_Dox24h_PAJF646_50nM_30min/pyspaz/Tracking/pos10_Tracked.mat'
    #outPrefix = '/home/scollomb/Lab_Heard/project_SC_XiComp/SPT/test'
    #mask_0_frame_index = 0
    #mask_1_frame_index = 20000
    #pixel_subsampling_factor = 1
    #pixel_size_um=0.106
    #olap_rule=any
    #olap_fracMin=0
    #olap_fracMax=1
    #datasetName='test'
    #sample='pos10'
    #ROE='nuc1'
    #ROI='Xi'

    print('----- ROI mask ----- load data')
    ## loading masks
    mask_ROI_0_binary = np.asarray(Image.open(mask_ROI_0_file)).astype(np.float32)
    mask_ROI_0_binary[mask_ROI_0_binary>0] =1
    mask_ROI_1_binary = np.asarray(Image.open(mask_ROI_1_file)).astype(np.float32)
    mask_ROI_1_binary[mask_ROI_1_binary>0] =1
    mask_ROE_0_binary = np.asarray(Image.open(mask_ROE_0_file)).astype(np.float32)
    mask_ROE_0_binary[mask_ROE_0_binary>0] =1
    mask_ROE_1_binary = np.asarray(Image.open(mask_ROE_1_file)).astype(np.float32)
    mask_ROE_1_binary[mask_ROE_1_binary>0] =1
    
    ### assigning traj to ROI mask followign the rule of olap and 
    print('----- ROI mask ----- make mask')
    ori_AssigmentTable = interpolateMaskAndAssignTrajectories(
        mask_0 = mask_ROI_0_binary,
        mask_1 = mask_ROI_1_binary,
        mask_0_frame_index = mask_0_frame_index,
        mask_1_frame_index = mask_1_frame_index,
        trajectory_file = trajectory_file,
        outPrefix = outPrefix + 'ori',
        pixel_subsampling_factor = pixel_subsampling_factor,
        save_table = False,
        plot_mask = False,
        pixel_size_um = pixel_size_um
    )
    print('----- ROI mask ----- assign trajectories')
    ori_trajs=FilterAsignedTrajectories(
        trajectory_file = trajectory_file,
        AssigmentTable=ori_AssigmentTable,
        olap_rule=olap_rule,
        olap_fracMin = olap_fracMin,
        olap_fracMax = olap_fracMax,
        min_msrd=min_msrd
    )
    ### Number of traj with MSRD >100nm, 200nm etc...
    print('----- ROI mask ----- calculate MSRD')
    min_traj_len=2
    ori_trajs_2p = ori_trajs[np.vectorize(len)(ori_trajs[:,1])>=min_traj_len]
    ori_traj_2p_MSRD_list=[]
    for i in range(len(ori_trajs_2p)):
        ori_traj_2p_MSRD_list.append(pdistmean(ori_trajs_2p[i][0]))
    
    ori_traj_2p_MSRD=np.array(ori_traj_2p_MSRD_list)
    ori_traj_2p_MSRD_cat_list=['original',
                               len(ori_traj_2p_MSRD[np.array(ori_traj_2p_MSRD)>0.000]),
                               len(ori_traj_2p_MSRD[np.array(ori_traj_2p_MSRD)<=0.100]),
                               len(ori_traj_2p_MSRD[(np.array(ori_traj_2p_MSRD)>0.100) & (np.array(ori_traj_2p_MSRD)<0.200)]),
                               len(ori_traj_2p_MSRD[np.array(ori_traj_2p_MSRD)>0.200])
    ]
    #ori_traj_2p_MSRD_cat_list_cat=ori_traj_2p_MSRD_cat_list + (np.array(ori_traj_2p_MSRD_cat_list[1:])/np.array(ori_traj_2p_MSRD_cat_list[1:])).tolist()
    #traj_2p_MSRD_cat_df_all = pd.DataFrame([ori_traj_2p_MSRD_cat_list_cat], columns=['id','all','MSRDinf100nm','MSRD100to200nm','MSRDsup200nm','DiffRatioall','DiffRatioMSRDinf100nm','DiffRatioMSRD100to200nm','DiffRatioMSRDsup200nm'])
    traj_2p_MSRD_cat_df_all = pd.DataFrame([ori_traj_2p_MSRD_cat_list], columns=['id','all','MSRDinf100nm','MSRD100to200nm','MSRDsup200nm'])
    
    
    print('----- shifted masks ----- calculate some stuff before starting')
    ### calculation of max shift for mask 0
    
    mask_ROI_0_binary_center = center_mask(mask_ROI_0_binary)
    mask_ROI_1_binary_center = center_mask(mask_ROI_1_binary)
    
    mask_ROE_0_binary_inv =mask_ROE_0_binary.copy().astype(np.float32)
    mask_ROE_0_binary_inv = -(mask_ROE_0_binary_inv-1)
    mask_sum_ROI_ROEinv =mask_ROE_0_binary_inv+mask_ROI_0_binary_center
    ROE_0_ymin=np.min(np.where(mask_ROE_0_binary>0)[0])
    ROE_0_ymax=np.max(np.where(mask_ROE_0_binary>0)[0])
    ROE_0_xmin=np.min(np.where(mask_ROE_0_binary>0)[1])
    ROE_0_xmax=np.max(np.where(mask_ROE_0_binary>0)[1])
    
    ### calculation of max shift for mask 1
    mask_ROE_1_binary_inv =mask_ROE_1_binary.copy().astype(np.float32)
    mask_ROE_1_binary_inv = -(mask_ROE_1_binary_inv-1)
    mask_sum_ROI_ROEinv =mask_ROE_1_binary_inv+mask_ROI_1_binary_center
    ROE_1_ymin=np.min(np.where(mask_ROE_1_binary>0)[0])
    ROE_1_ymax=np.max(np.where(mask_ROE_1_binary>0)[0])
    ROE_1_xmin=np.min(np.where(mask_ROE_1_binary>0)[1])
    ROE_1_xmax=np.max(np.where(mask_ROE_1_binary>0)[1])
    
    ### Size of the mask, to calculate fraction of overlap
    ROI_0_px=len(np.where(mask_ROI_0_binary>0)[0])
    ROI_1_px=len(np.where(mask_ROI_1_binary>0)[0])
    
    ## convert ROE to distance to border 
    mask_ROE_0_binary_dist=scipy.ndimage.morphology.distance_transform_edt(mask_ROE_0_binary).astype(np.float32)
    mask_ROE_1_binary_dist=scipy.ndimage.morphology.distance_transform_edt(mask_ROE_0_binary).astype(np.float32)
    ## mean distance for Xi
    ROI_0_meanDist_ROE= np.mean(mask_ROE_0_binary_dist[np.where(mask_ROI_0_binary>0)])
    ROI_1_meanDist_ROE= np.mean(mask_ROE_1_binary_dist[np.where(mask_ROI_1_binary>0)])
    
    ### initial an overlay for the different masks
    mask_ROI_0_binary_center_add=np.zeros(mask_ROI_0_binary.shape, dtype=np.float32)
    mask_ROI_0_binary_center_add[np.where(mask_ROI_0_binary>0)]=1
    mask_ROI_1_binary_center_add=np.zeros(mask_ROI_1_binary.shape, dtype=np.float32)
    mask_ROI_1_binary_center_add[np.where(mask_ROI_1_binary>0)]=1
    
    mask_ROI_0_binary_center_add_id=np.zeros(mask_ROI_0_binary.shape, dtype=np.float32)
    mask_ROI_0_binary_center_add_id[np.where(mask_ROI_0_binary>0)]=1
    mask_ROI_1_binary_center_add_id=np.zeros(mask_ROI_1_binary.shape, dtype=np.float32)
    mask_ROI_1_binary_center_add_id[np.where(mask_ROI_1_binary>0)]=1
    
    print('----- shifted masks ----- random shifting')
    ### random shifting
    max_iters=maxiters
    max_masks=maxmasks
    valid=0
    n_iters=0
    n_masks=0
    while valid==0:
        if n_iters==max_iters:
            print("----- shifted masks ----- ", max_iters, 'iterations reached...')
            valid=1
        elif n_masks==max_masks:
            print("----- shifted masks ----- ", max_masks, 'shifted mask found ...')
            valid=1
        else:
            n_iters+=1
            if n_iters % 5000 == 0 :
                print()
                sys.stdout.write("----- shifted masks ----- {} iterations done ...\n".format(n_iters))
                sys.stdout.flush()
            ## randomly rotate mask
            dr=random.randint(0,360)
            mask_ROI_0_binary_center_rotated = rotate_mask(mask_ROI_0_binary_center, dr)
            mask_ROI_1_binary_center_rotated = rotate_mask(mask_ROI_1_binary_center, dr)
            ## calculate max dx and dy
            ROI_0_ymin=np.min(np.where(mask_ROI_0_binary_center_rotated>0)[0])
            ROI_0_ymax=np.max(np.where(mask_ROI_0_binary_center_rotated>0)[0])
            ROI_0_xmin=np.min(np.where(mask_ROI_0_binary_center_rotated>0)[1])
            ROI_0_xmax=np.max(np.where(mask_ROI_0_binary_center_rotated>0)[1])
            ROI_1_ymin=np.min(np.where(mask_ROI_1_binary_center_rotated>0)[0])
            ROI_1_ymax=np.max(np.where(mask_ROI_1_binary_center_rotated>0)[0])
            ROI_1_xmin=np.min(np.where(mask_ROI_1_binary_center_rotated>0)[1])
            ROI_1_xmax=np.max(np.where(mask_ROI_1_binary_center_rotated>0)[1])
            ### calculation of max shift for both masks, +/- 10 to have some margin when rotating 
            dymin=np.max([ROE_0_ymin-ROI_0_ymin, ROE_1_ymin-ROI_1_ymin])
            dymax=np.min([ROE_0_ymax-ROI_0_ymax, ROE_1_ymax-ROI_1_ymax])
            dxmin=np.max([ROE_0_xmin-ROI_0_xmin, ROE_1_xmin-ROI_1_xmin])
            dxmax=np.min([ROE_0_xmax-ROI_0_xmax, ROE_1_xmax-ROI_1_xmax])
            # random shift in the authorized limit
            dx=range(dxmin,dxmax)[np.random.randint(0,dxmax-dxmin)]
            dy=range(dymin,dymax)[np.random.randint(0,dymax-dymin)]
            # shifted masks
            mask_ROI_0_binary_center_rotated_shift = np.zeros(mask_ROI_0_binary_center_rotated.shape, dtype=np.float32)
            mask_ROI_0_binary_center_rotated_shift[(np.array(np.where(mask_ROI_0_binary_center_rotated>0)[0]+dy),np.array(np.where(mask_ROI_0_binary_center_rotated>0)[1]+dx))]=1
            mask_ROI_1_binary_center_rotated_shift = np.zeros(mask_ROI_1_binary_center_rotated.shape, dtype=np.float32)
            mask_ROI_1_binary_center_rotated_shift[(np.array(np.where(mask_ROI_1_binary_center_rotated>0)[0]+dy),np.array(np.where(mask_ROI_1_binary_center_rotated>0)[1]+dx))]=1
            # fraction of shifted masks that is outside the ROE
            px_outside_ROE_0_frac=len(np.where(mask_ROI_0_binary_center_rotated_shift+mask_ROE_0_binary_inv>1)[0])/ROI_0_px
            px_outside_ROE_1_frac=len(np.where(mask_ROI_1_binary_center_rotated_shift+mask_ROE_1_binary_inv>1)[0])/ROI_1_px
            if px_outside_ROE_0_frac<=max_mask_outsideROE and px_outside_ROE_1_frac<=max_mask_outsideROE :
                # fraction of shifted mask that overlap other masks
                px_olap_ROIshift_0_frac=len(np.where(mask_ROI_0_binary_center_rotated_shift+mask_ROI_0_binary_center_add>1)[0])/ROI_0_px
                px_olap_ROIshift_1_frac=len(np.where(mask_ROI_1_binary_center_rotated_shift+mask_ROI_1_binary_center_add>1)[0])/ROI_1_px
                if px_olap_ROIshift_0_frac<=max_mask_olap and px_olap_ROIshift_1_frac<=max_mask_olap :
                    # distance to ROE of shifted mask relative to the distance of the original mask
                    shiftOk=False
                    ROIshift_0_meanDist_ROE= np.mean(mask_ROE_0_binary_dist[np.where(mask_ROI_0_binary_center_rotated_shift>0)])
                    ROIshift_1_meanDist_ROE= np.mean(mask_ROE_1_binary_dist[np.where(mask_ROI_1_binary_center_rotated_shift>0)])
                    ROIshift_0_meanDist_ROE_difFrac=(ROIshift_0_meanDist_ROE-ROI_0_meanDist_ROE)/ROI_0_meanDist_ROE
                    ROIshift_1_meanDist_ROE_difFrac=(ROIshift_1_meanDist_ROE-ROI_1_meanDist_ROE)/ROI_1_meanDist_ROE
                    if ROIshift_0_meanDist_ROE_difFrac<=max_dist_ROE_difFrac and ROIshift_1_meanDist_ROE_difFrac<=max_dist_ROE_difFrac and ROIshift_0_meanDist_ROE_difFrac>=min_dist_ROE_difFrac and ROIshift_1_meanDist_ROE_difFrac>=min_dist_ROE_difFrac :
                        print([ROIshift_0_meanDist_ROE_difFrac, ROIshift_0_meanDist_ROE_difFrac/ROI_0_meanDist_ROE, ROIshift_1_meanDist_ROE_difFrac, ROIshift_1_meanDist_ROE_difFrac/ROI_1_meanDist_ROE])
                        shiftOk=True
                        mask_ROI_0_binary_center_add[np.where(mask_ROI_0_binary_center_rotated_shift>0)]=1
                        mask_ROI_1_binary_center_add[np.where(mask_ROI_1_binary_center_rotated_shift>0)]=1
                        n_masks+=1
                        mask_ROI_0_binary_center_add_id[np.where(mask_ROI_0_binary_center_rotated_shift>0)]=1+n_masks
                        mask_ROI_1_binary_center_add_id[np.where(mask_ROI_1_binary_center_rotated_shift>0)]=1+n_masks
                        print('----- shifted masks ----- mask', n_masks, 'found at iteration', n_iters)
                        ### for debug: some values
                        # print(n_iters, '\t ROE_0_frac',np.round(px_outside_ROE_0_frac,1),'\t ROE_1_frac',np.round(px_outside_ROE_1_frac,1),'\t ROI_0_olap',np.round(px_olap_ROIshift_0_frac,1),'\t ROI_1_olap',np.round(px_olap_ROIshift_1_frac,1), '\t shift_ok', shiftOk)
                        # assign traj to the mask
                        print('----- shifted masks ----- mask', n_masks, ' ----- make mask')
                        AssigmentTable = interpolateMaskAndAssignTrajectories(
                            mask_0 = mask_ROI_0_binary_center_rotated_shift,
                            mask_1 = mask_ROI_1_binary_center_rotated_shift,
                            mask_0_frame_index = mask_0_frame_index,
                            mask_1_frame_index = mask_1_frame_index,
                            trajectory_file = trajectory_file,
                            outPrefix = outPrefix + '_shiftMask' + str(n_masks),
                            pixel_subsampling_factor = pixel_subsampling_factor,
                            save_table = False,
                            plot_mask = False,
                            pixel_size_um = pixel_size_um
                        )
                        # filter traj on rule
                        print('----- shifted masks ----- mask', n_masks, ' ----- assign trajectories')
                        trajs=FilterAsignedTrajectories(
                            trajectory_file = trajectory_file,
                            AssigmentTable=AssigmentTable,
                            olap_rule=olap_rule,
                            olap_fracMin = olap_fracMin,
                            olap_fracMax = olap_fracMax,
                            min_msrd=min_msrd,
                            outFile=outPrefix + '_shiftMask' + str(n_masks) + str(args.olap_rule) + "_Tracked.mat"
                        )
                        
                        # Mean Square Root Displacement of traj with >=2 points
                        print('----- shifted masks ----- mask', n_masks, ' ----- compute MSRD')
                        if len(trajs)==0:
                            traj_2p_MSRD_cat_list=['shift'+ str(n_masks),0,0,0,0]
                        else:
                            min_traj_len=2
                            trajs_2p = trajs[np.vectorize(len)(trajs[:,1])>=min_traj_len]
                            if len(trajs_2p)==0:
                                traj_2p_MSRD_cat_list=['shift'+ str(n_masks),0,0,0,0]
                            else:
                                traj_2p_MSRD_list=[]
                                for i in range(len(trajs_2p)):
                                    traj_2p_MSRD_list.append(pdistmean(trajs_2p[i][0]))
                                traj_2p_MSRD=np.array(traj_2p_MSRD_list)
                                traj_2p_MSRD_cat_list=['shift'+ str(n_masks),
                                                        len(traj_2p_MSRD[np.array(traj_2p_MSRD)>0.000]),
                                                        len(traj_2p_MSRD[np.array(traj_2p_MSRD)<=0.100]),
                                                        len(traj_2p_MSRD[(np.array(traj_2p_MSRD)>0.100) & (np.array(traj_2p_MSRD)<0.200)]),
                                                        len(traj_2p_MSRD[np.array(traj_2p_MSRD)>0.200])
                                ]
                        #print(traj_2p_MSRD_cat_list)
                        ## fraction of the difference
                        #DifFrac_000=(ori_traj_2p_MSRD_cat_list[1]-traj_2p_MSRD_cat_list[1])/np.max([ori_traj_2p_MSRD_cat_list[1],traj_2p_MSRD_cat_list[1]])
                        #DifFrac_inf100=(ori_traj_2p_MSRD_cat_list[2]-traj_2p_MSRD_cat_list[2])/np.max([ori_traj_2p_MSRD_cat_list[2],traj_2p_MSRD_cat_list[2]])
                        #DifFrac_100to200=(ori_traj_2p_MSRD_cat_list[3]-traj_2p_MSRD_cat_list[3])/np.max([ori_traj_2p_MSRD_cat_list[3],traj_2p_MSRD_cat_list[3]])
                        #DifFrac_sup200=(ori_traj_2p_MSRD_cat_list[4]-traj_2p_MSRD_cat_list[4])/np.max([ori_traj_2p_MSRD_cat_list[4],traj_2p_MSRD_cat_list[4]])
                        #DifFrac_list=[DifFrac_000,DifFrac_inf100,DifFrac_100to200,DifFrac_sup200]
                        ## convert to list and dataframe
                        #traj_2p_MSRD_cat_list_Norm=traj_2p_MSRD_cat_list + DifFrac_list
                        #traj_2p_MSRD_cat_df = pd.DataFrame([traj_2p_MSRD_cat_list_Norm], columns=['id','all','MSRDinf100nm','MSRD100to200nm','MSRDsup200nm','DiffRatioall','DiffRatioMSRDinf100nm','DiffRatioMSRD100to200nm','DiffRatioMSRDsup200nm'])
                        traj_2p_MSRD_cat_df = pd.DataFrame([traj_2p_MSRD_cat_list], columns=['id','all','MSRDinf100nm','MSRD100to200nm','MSRDsup200nm'])
                        ## add to main table
                        traj_2p_MSRD_cat_df_all=traj_2p_MSRD_cat_df_all.append(traj_2p_MSRD_cat_df, ignore_index=True)
            
    
    print('----- save combined table of MSRD')
    f= open(outPrefix + '_shifts_MSRD.csv',"w+")
    f.write('datasetName\tsample\tROE\tROI\tshiftID\tall\tMSRDinf100nm\tMSRD100to200nm\tMSRDsup200nm\n')
    for i in range(0,traj_2p_MSRD_cat_df_all.shape[0]):
        f.write("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(
            datasetName,
            sample,
            ROE,
            ROI,
            traj_2p_MSRD_cat_df_all['id'].tolist()[i],
            traj_2p_MSRD_cat_df_all['all'].tolist()[i],
            traj_2p_MSRD_cat_df_all['MSRDinf100nm'].tolist()[i],
            traj_2p_MSRD_cat_df_all['MSRD100to200nm'].tolist()[i],
            traj_2p_MSRD_cat_df_all['MSRDsup200nm'].tolist()[i]
        ))
    f.close()
    
    print('----- save combined table of mean MSRD for all shifted masks')
    #traj_2p_MSRD_cat_df_all_DiffRatioMean = np.array(traj_2p_MSRD_cat_df_all.iloc[1:,5:].mean())
    #traj_2p_MSRD_cat_df_all_trajMean = np.array(traj_2p_MSRD_cat_df_all.iloc[1:,1:6].mean())
    traj_2p_MSRD_cat_df_all_trajMean = np.array(traj_2p_MSRD_cat_df_all.iloc[1:,1:5].mean())
    
    masksZC=np.zeros(shape=(2,3,mask_ROI_0_binary_center_add_id.shape[0],mask_ROI_0_binary_center_add_id.shape[1]), dtype=np.uint32)
    masksZC[0,0,:,:] = mask_ROI_0_binary_center_add_id
    masksZC[1,0,:,:] = mask_ROI_1_binary_center_add_id
    masksZC[0,1,:,:] = mask_ROI_0_binary
    masksZC[1,1,:,:] = mask_ROI_1_binary
    masksZC[0,2,:,:] = mask_ROE_0_binary
    masksZC[1,2,:,:] = mask_ROE_1_binary
    masksZC
    skio.imsave(outPrefix  + '_masks.tiff', masksZC.astype(np.float32()), plugin='tifffile',imagej=True, append=False)
    
    
    print('----- save file' + outPrefix + '_shifts_MSRD_meanOfShifts.csv')
    f= open(outPrefix + '_shifts_MSRD_meanOfShifts.csv',"w+")
    f.write('datasetName\tsample\tROE\tROI\tshiftN\tall\tMSRDinf100nm\tMSRD100to200nm\tMSRDsup200nm\tshiftall\tshiftMSRDinf100nm\tshiftMSRD100to200nm\tshiftMSRDsup200nm\n')
    f.write("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(
        datasetName,
        sample,
        ROE,
        ROI,
        np.array(traj_2p_MSRD_cat_df_all).shape[0]-1,
        ori_traj_2p_MSRD_cat_list[1],
        ori_traj_2p_MSRD_cat_list[2],
        ori_traj_2p_MSRD_cat_list[3],
        ori_traj_2p_MSRD_cat_list[4],
        traj_2p_MSRD_cat_df_all_trajMean[0],
        traj_2p_MSRD_cat_df_all_trajMean[1],
        traj_2p_MSRD_cat_df_all_trajMean[2],
        traj_2p_MSRD_cat_df_all_trajMean[3]
    ))
    f.close()
