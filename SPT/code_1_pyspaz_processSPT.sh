analysisName="SPT_sc18c7_Polr2aHalo_Dox24h_PAJF646"
mainInDir=/home/scollomb/Lab_Heard/project_SC_XiComp/SPT/Polr2a/sc18c7/Dox24h_PAJF646
mainTmpDir=/home/scollomb/Lab_Heard/project_SC_XiComp/SPT/Polr2a/sc18c7/Dox24h_PAJF646
mainOutDir=/home/scollomb/Lab_Heard/project_SC_XiComp/SPT/Polr2a/sc18c7/Dox24h_PAJF646
env='#!/bin/bash'

cd ${mainInDir}

datasets=("191128_SPT_sc18c7_Dox24h_PAJF646_50nM_30min" "191129_SPT_sc18c7_Dox24h_PAJF646_50nM_30min" "191204_SPT_sc18c7_Dox24h_PAJF646_50nM_30min" "191205_SPT_sc18c7_Dox24h_PAJF646_50nM_30min" "191206_SPT_sc18c7_Dox24h_PAJF646_50nM_30min" "191207_SPT_sc18c7_Dox24h_PAJF646_50nM_30min")
pixelSize=0.106
timeBtwnFrame=0.005477

# # # # # ## create directory with SPT data (link) with file names as just posxx.nd2
# for datasetName in "${datasets[@]}"; do
#     rm -r $datasetName/SPTdata_shortName
#     mkdir $datasetName/SPTdata_shortName
#     for file in $datasetName/SPTdata/* ; do
#         oriName=$(basename "$file")
#         newName=$(sed 's|.*_pos|pos|g' <<< $oriName)
#         ln -s $mainTmpDir/$datasetName/SPTdata/$oriName $mainTmpDir/$datasetName/SPTdata_shortName/$newName
#     done
# done

# # # # # # # # remove some file/folders
for datasetName in "${datasets[@]}"; do
# # #         rm -r ${mainTmpDir}/${datasetName}/pyspaz
# #         rm ${mainTmpDir}/${datasetName}/pyspaz/log/*Tra*
        rm ${mainTmpDir}/${datasetName}/pyspaz/log/*ROE_any*
# #         rm ${mainTmpDir}/${datasetName}/pyspaz/log/*Pdf*
# #         rm -r ${mainTmpDir}/${datasetName}/pyspaz/SpotOn
# #         rm -r ${mainTmpDir}/${datasetName}/pyspaz/traj*        
done 
rm -r Pool_datasets

# # # process all files
for datasetName in "${datasets[@]}"; do
    echo $datasetName
    analysisName=$datasetName

    dataDescription=${mainInDir}/${datasetName}/ilastik/dataDescription.csv
    SptDataDir=${mainInDir}/${datasetName}/SPTdata_shortName
    maskDir=${mainInDir}/${datasetName}/ilastik/masks

    tmpDir=${mainTmpDir}/${datasetName}/pyspaz
    mkdir -p ${tmpDir}/std
    mkdir -p ${tmpDir}/log

    ## loop for each sample (ie position imaged)
    for line in `cat ${dataDescription} | sed 1d | cut -f 1-2 | sort -u | sed 's/\t/#/g'` ; do 
        array=(${line//#/ })
        sample=${array[1]}
        echo "---- sample" $sample
        outName=${sample}
        
        trackingFile=${SptDataDir}/${outName}.nd2
        ls $trackingFile
    
        while read var; do unset $var; done < <(( set -o posix ; set ) | grep "job.*_id*" | sed 's#=#\t#g' | cut -f 1)
################################################################################
### Localise and QC
################################################################################
        ## run localisation and tracking
        job=Localize_${outName}
        if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
            if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
            jobName=${job}...${outName}...${analysisName}
            jobLocalize=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 15 \
                --wrap="${env}
                    date
                    mkdir ${tmpDir}/Localize
                    source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT
                    pyspaz localize detect-and-localize-file -s 1 -t 20 --input_file ${trackingFile} -o ${tmpDir}/Localize/${sample}_Loc.txt
                        
                    if [ -s ${tmpDir}/Localize/${sample}_Loc.txt ] ; then 
                        echo ${job} done! > ${tmpDir}/log/${job}.done
                    else exit 1; fi; date"`
            jobLocalize_id=`echo :${jobLocalize##* }`; jobLocalize_name=${jobName}
            if [ -z ${job_all_id} ]; then job_all_id=${jobLocalize_id}; else job_all_id=${job_all_id}${jobLocalize_id}; fi    ### add job id to a variable listing all job ids
            sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobLocalize_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobLocalize_id#:}"
        fi
        job=Localize_QC_${outName}
        if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
            if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
            if [ -z ${jobLocalize_id} ]; then dep= ; else dep="--dependency=afterok${jobLocalize_id}" ; fi
            jobName=${job}...${outName}...${analysisName}
            jobLocalize_QC=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 4000 -t 15 ${dep} \
                --wrap="${env}
                    date
                    mkdir ${tmpDir}/Localize_QC
                    source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
                    pyspaz_SAM localize localisation-qc -i ${trackingFile} -l ${tmpDir}/Localize/${sample}_Loc.txt -o ${tmpDir}/Localize_QC/${sample}
                        
                    if [ -s ${tmpDir}/Localize_QC/${sample}_photonCount_vs_background.pdf ] ; then 
                        echo ${job} done! > ${tmpDir}/log/${job}.done
                    else exit 1; fi; date"`
            jobLocalize_QC_id=`echo :${jobLocalize_QC##* }`; jobLocalize_QC_name=${jobName}
            if [ -z ${job_all_id} ]; then job_all_id=${jobLocalize_QC_id}; else job_all_id=${job_all_id}${jobLocalize_QC_id}; fi    ### add job id to a variable listing all job ids
            sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobLocalize_QC_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobLocalize_QC_id#:}"
        fi

        job=Localize_QC_plotDensity_${outName}
        if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
            if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
            if [ -z ${jobLocalize_id} ]; then dep= ; else dep="--dependency=afterok${jobLocalize_id}" ; fi
            jobName=${job}...${outName}...${analysisName}
            jobLocalize_QC_plotDensity=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 4000 -t 15 ${dep} \
                --wrap="${env}
                    date
                    mkdir ${tmpDir}/Localize_QC
                    source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
                    pyspaz_SAM localize plot-localisation-density \
                        --cmap magma --convert_to_um False \
                        -i ${tmpDir}/Localize/${sample}_Loc.txt -o ${tmpDir}/Localize_QC/${sample}_density
                        
                    if [ -s ${tmpDir}/Localize_QC_plotDensity/${sample}_density.png ] ; then 
                        echo ${job} done! > ${tmpDir}/log/${job}.done
                    else exit 1; fi; date"`
            jobLocalize_QC_plotDensity_id=`echo :${jobLocalize_QC_plotDensity##* }`; jobLocalize_QC_plotDensity_name=${jobName}
            if [ -z ${job_all_id} ]; then job_all_id=${jobLocalize_QC_plotDensity_id}; else job_all_id=${job_all_id}${jobLocalize_QC_plotDensity_id}; fi    ### add job id to a variable listing all job ids
            sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobLocalize_QC_plotDensity_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobLocalize_QC_plotDensity_id#:}"
        fi


###############################################################################
## Track and QC
###############################################################################
        job=Track_${outName}
        if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
            if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
            if [ -z ${jobLocalize_id} ]; then dep= ; else dep="--dependency=afterok${jobLocalize_id}" ; fi
            jobName=${job}...${outName}...${analysisName}
            jobTrack=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 2000 -t 10 ${dep} \
                --wrap="${env}
                    date
                    mkdir ${tmpDir}/Tracking
                    source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT
                    pyspaz track track-locs --algorithm_type conservative --pixel_size_um ${pixelSize} -e 3 -dm 10 -db 0.1 -f ${timeBtwnFrame} -b 0 -i ${tmpDir}/Localize/${sample}_Loc.txt -o ${tmpDir}/Tracking/${sample}_Tracked.mat
                        
                    if [ -s ${tmpDir}/Tracking/${sample}_Tracked.mat ] ; then 
                        echo ${job} done! > ${tmpDir}/log/${job}.done
                    else exit 1; fi; date"`
            jobTrack_id=`echo :${jobTrack##* }`; jobTrack_name=${jobName}
            if [ -z ${job_all_id} ]; then job_all_id=${jobTrack_id}; else job_all_id=${job_all_id}${jobTrack_id}; fi    ### add job id to a variable listing all job ids
            sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobTrack_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobTrack_id#:}"
        fi

        job=Track_QC_${outName}
        if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
            if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
            if [ -z ${jobTrack_id} ]; then dep= ; else dep="--dependency=afterok${jobTrack_id}" ; fi
            jobName=${job}...${outName}...${analysisName}
            jobTrack_QC=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 2000 -t 10 ${dep} \
                --wrap="${env}
                    date
                    mkdir ${tmpDir}/Tracking_QC
                    source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
                    pyspaz_SAM track tracking-qc -i ${tmpDir}/Tracking/${sample}_Tracked.mat -o ${tmpDir}/Tracking_QC/${sample}
                        
                    if [ -s ${tmpDir}/Tracking_QC/${sample}_trackingFrameLength_distrib.pdf ] ; then 
                        echo ${job} done! > ${tmpDir}/log/${job}.done
                    else exit 1; fi; date"`
            jobTrack_QC_id=`echo :${jobTrack_QC##* }`; jobTrack_QC_name=${jobName}
            if [ -z ${job_all_id} ]; then job_all_id=${jobTrack_QC_id}; else job_all_id=${job_all_id}${jobTrack_QC_id}; fi    ### add job id to a variable listing all job ids
            sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobTrack_QC_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobTrack_QC_id#:}"
        fi

        job=Track_QC_plotTrajsPng_${outName}
        if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
            if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
            if [ -z ${jobTrack_id} ]; then dep= ; else dep="--dependency=afterok${jobTrack_id}" ; fi
            jobName=${job}...${outName}...${analysisName}
            jobTrack_QC_plotTrajsPng=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep} \
                --wrap="${env}
                    date
                    mkdir ${tmpDir}/Tracking_QC
                    source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
                    pyspaz_SAM track plot-trajectories \
                        --cap 10000 --alpha 1 --cmap magma --min_jumps 2 \
                        -t ${tmpDir}/Tracking/${sample}_Tracked.mat \
                        -o ${tmpDir}/Tracking_QC/${sample}_plotTrajectories.png

                    if [ -s ${tmpDir}/Tracking_QC/${sample}_plotTrajectories.png ] ; then 
                        echo ${job} done! > ${tmpDir}/log/${job}.done
                    else exit 1; fi; date"`
            jobTrack_QC_plotTrajsPng_id=`echo :${jobTrack_QC_plotTrajsPng##* }`; jobTrack_QC_plotTrajsPng_name=${jobName}
            if [ -z ${job_all_id} ]; then job_all_id=${jobTrack_QC_plotTrajsPng_id}; else job_all_id=${job_all_id}${jobTrack_QC_plotTrajsPng_id}; fi    ### add job id to a variable listing all job ids
            sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobTrack_QC_plotTrajsPng_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobTrack_QC_plotTrajsPng_id#:}"
        fi

        job=Tracking_QC_plotTrajsPdf_${outName}
        if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
            if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
            if [ -z ${jobTrack_id} ]; then dep= ; else dep="--dependency=afterok${jobTrack_id}" ; fi
            jobName=${job}...${outName}...${analysisName}
            jobTracking_QC_plotTrajsPdf=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep} \
                --wrap="${env}
                    date
                    mkdir ${tmpDir}/Tracking_QC
                    source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
                    pyspaz_SAM track plot-trajectories \
                        --cap 1000 --alpha 1 --cmap magma --min_jumps 2 \
                        -t ${tmpDir}/Tracking/${sample}_Tracked.mat \
                        -o ${tmpDir}/Tracking_QC/${sample}_plotTrajectories.pdf

                    if [ -s ${tmpDir}/Tracking_QC/${sample}_plotTrajectories.png ] ; then 
                        echo ${job} done! > ${tmpDir}/log/${job}.done
                    else exit 1; fi; date"`
            jobTracking_QC_plotTrajsPdf_id=`echo :${jobTracking_QC_plotTrajsPdf##* }`; jobTracking_QC_plotTrajsPdf_name=${jobName}
            if [ -z ${job_all_id} ]; then job_all_id=${jobTracking_QC_plotTrajsPdf_id}; else job_all_id=${job_all_id}${jobTracking_QC_plotTrajsPdf_id}; fi    ### add job id to a variable listing all job ids
            sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobTracking_QC_plotTrajsPdf_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobTracking_QC_plotTrajsPdf_id#:}"
        fi

        
################################################################################
### sub loop on each Region Of Ensemble (usually: nucleus) per sample
################################################################################
        for line in `cat ${dataDescription} | awk -v sample=${sample} '{if ($2==sample) print;}' | cut -f 1-3 | sort -u | sed 's/\t/#/g'` ; do 
            array=(${line//#/ })
            ROE=${array[2]}
            echo -e "---- sample" $sample "\t---- ROE" $ROE
            outName=${sample}_${ROE}
            
            ## assign trajectories to the ROE
            job=Traj_ROE_any_assign_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                if [ -z ${jobTrack_id} ]; then dep= ; else dep="--dependency=afterok${jobTrack_id}" ; fi
                jobName=${job}...${outName}...${analysisName}
                jobTraj_ROE_any_assign=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep} \
                    --wrap="${env}
                        date
                        mkdir ${tmpDir}/traj_ROE_any
                        source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
                        python ~/programs/SPTpy/interpolateMaskAndAssignTrajectories.py \
                            -i ${tmpDir}/Tracking/${sample}_Tracked.mat \
                            --mask_0_tif ${maskDir}/${sample}_BEFORE_${ROE}.tif \
                            --mask_1_tif ${maskDir}/${sample}_AFTER_${ROE}.tif \
                            --mask_0_frame_index 0 \
                            --mask_1_frame_index 30000 \
                            --olap_rule any \
                            --olap_fracMin 0 --olap_fracMax 1 --pixel_subsampling_factor 1 --pixel_size_um ${pixelSize} \
                            -o ${tmpDir}/traj_ROE_any/${sample}_${ROE}

                        if [ -s ${tmpDir}/traj_ROE_any/${sample}_${ROE}_any_Tracked.mat ] ; then 
                            echo ${job} done! > ${tmpDir}/log/${job}.done
                        else exit 1; fi; date"`
                jobTraj_ROE_any_assign_id=`echo :${jobTraj_ROE_any_assign##* }`; jobTraj_ROE_any_assign_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobTraj_ROE_any_assign_id}; else job_all_id=${job_all_id}${jobTraj_ROE_any_assign_id}; fi    ### add job id to a variable listing all job ids
                if [ -z ${jobTraj_ROE_any_assign_allids} ]; then jobTraj_ROE_any_assign_allids=${jobTraj_ROE_any_assign_id}; else jobTraj_ROE_any_assign_allids=${jobTraj_ROE_any_assign_allids}${jobTraj_ROE_any_assign_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobTraj_ROE_any_assign_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobTraj_ROE_any_assign_id#:}"
            fi

            ## simlink traj file in Pooled dataset directory
            ## this is done as a separate Job so the simlink is not created if the assignement did not work, so not broken link is created 
            job=Traj_ROE_any_copyForPoolDataset_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                if [ -z ${jobTraj_ROE_any_assign_id} ]; then dep= ; else dep="--dependency=afterok${jobTraj_ROE_any_assign_id}" ; fi
                jobName=${job}...${outName}...${analysisName}
                jobTraj_ROE_any_copyForPoolDataset=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep} \
                    --wrap="${env}
                        date
                            mkdir -p ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any
                            rm ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any/${datasetName}_${sample}_${ROE}_any_Tracked.mat
                            ln -s ${tmpDir}/traj_ROE_any/${sample}_${ROE}_any_Tracked.mat ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any/${datasetName}_${sample}_${ROE}_any_Tracked.mat
                            
                        if [ -s ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any/${datasetName}_${sample}_${ROE}_any_Tracked.mat ] ; then 
                            echo ${job} done! > ${tmpDir}/log/${job}.done
                        else exit 1; fi; date"`
                jobTraj_ROE_any_copyForPoolDataset_id=`echo :${jobTraj_ROE_any_copyForPoolDataset##* }`; jobTraj_ROE_any_copyForPoolDataset_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobTraj_ROE_any_copyForPoolDataset_id}; else job_all_id=${job_all_id}${jobTraj_ROE_any_copyForPoolDataset_id}; fi    ### add job id to a variable listing all job ids
                if [ -z ${jobTraj_ROE_any_copyForPoolDataset_allids} ]; then jobTraj_ROE_any_copyForPoolDataset_allids=${jobTraj_ROE_any_copyForPoolDataset_id}; else jobTraj_ROE_any_copyForPoolDataset_allids=${jobTraj_ROE_any_copyForPoolDataset_allids}${jobTraj_ROE_any_copyForPoolDataset_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobTraj_ROE_any_copyForPoolDataset_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobTraj_ROE_any_copyForPoolDataset_id#:}"
            fi

            job=Traj_ROE_any_plotPdf_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                if [ -z ${jobTraj_ROE_any_assign_id} ]; then dep= ; else dep="--dependency=afterok${jobTraj_ROE_any_assign_id}" ; fi
                jobName=${job}...${outName}...${analysisName}
                jobTraj_ROE_any_plotPdf=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep} \
                    --wrap="${env}
                        date
                        mkdir ${tmpDir}/Tracking_QC
                        source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
                        pyspaz_SAM track plot-trajectories \
                            --cap 1000 --alpha 1 --cmap magma --min_jumps 2 \
                            -t ${tmpDir}/traj_ROE_any/${sample}_${ROE}_any_Tracked.mat \
                            -o ${tmpDir}/Tracking_QC/${sample}_${ROE}_any_plotTrajectories.pdf

                        if [ -s ${tmpDir}/Tracking_QC/${sample}_${ROE}_any_plotTrajectories.png ] ; then 
                            echo ${job} done! > ${tmpDir}/log/${job}.done
                        else exit 1; fi; date"`
                jobTraj_ROE_any_plotPdf_id=`echo :${jobTraj_ROE_any_plotPdf##* }`; jobTraj_ROE_any_plotPdf_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobTraj_ROE_any_plotPdf_id}; else job_all_id=${job_all_id}${jobTraj_ROE_any_plotPdf_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobTraj_ROE_any_plotPdf_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobTraj_ROE_any_plotPdf_id#:}"
            fi


            ROI=Xi
            echo -e "---- sample" $sample "\t---- ROE" $ROE "\t---- ROI" $ROI
            outName=${sample}_${ROE}
            
            ## assign trajectories: in ROE but NOT in ROI
            job=Traj_ROE_any_ROI_none_assign_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                if [ -z ${jobTraj_ROE_any_assign_id} ]; then dep= ; else dep="--dependency=afterok${jobTraj_ROE_any_assign_id}" ; fi
                jobName=${job}...${outName}...${analysisName}
                jobTraj_ROE_any_ROI_none_assign=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep} \
                    --wrap="${env}
                        date
                        mkdir ${tmpDir}/traj_ROE_any_ROI_none
                        source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
                        python ~/programs/SPTpy/interpolateMaskAndAssignTrajectories.py \
                            -i ${tmpDir}/traj_ROE_any/${sample}_${ROE}_any_Tracked.mat \
                            --mask_0_tif ${maskDir}/${sample}_BEFORE_${ROE}_${ROI}.tif \
                            --mask_1_tif ${maskDir}/${sample}_AFTER_${ROE}_${ROI}.tif \
                            --mask_0_frame_index 0 \
                            --mask_1_frame_index 30000 \
                            --olap_rule none \
                            --pixel_subsampling_factor 1 --pixel_size_um ${pixelSize} \
                            -o ${tmpDir}/traj_ROE_any_ROI_none/${sample}_${ROE}_any_${ROI}

                        if [ -s ${tmpDir}/traj_ROE_any_ROI_none/${sample}_${ROE}_any_${ROI}_none_Tracked.mat ] ; then 
                            echo ${job} done! > ${tmpDir}/log/${job}.done
                        else exit 1; fi; date"`
                jobTraj_ROE_any_ROI_none_assign_id=`echo :${jobTraj_ROE_any_ROI_none_assign##* }`; jobTraj_ROE_any_ROI_none_assign_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobTraj_ROE_any_ROI_none_assign_id}; else job_all_id=${job_all_id}${jobTraj_ROE_any_ROI_none_assign_id}; fi    ### add job id to a variable listing all job ids
                if [ -z ${jobTraj_ROE_any_ROI_none_assign_allids} ]; then jobTraj_ROE_any_ROI_none_assign_allids=${jobTraj_ROE_any_ROI_none_assign_id}; else jobTraj_ROE_any_ROI_none_assign_allids=${jobTraj_ROE_any_ROI_none_assign_allids}${jobTraj_ROE_any_ROI_none_assign_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobTraj_ROE_any_ROI_none_assign_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobTraj_ROE_any_ROI_none_assign_id#:}"
            fi

            ## simlink traj file in Pooled dataset directory
            ## this is done as a separate Job so the simlink is not created if the assignement did not work, so not broken link is created 
            job=Traj_ROE_any_ROI_none_copyForPoolDataset_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                if [ -z ${jobTraj_ROE_any_ROI_none_assign_id} ]; then dep= ; else dep="--dependency=afterok${jobTraj_ROE_any_ROI_none_assign_id}" ; fi
                jobName=${job}...${outName}...${analysisName}
                jobTraj_ROE_any_ROI_none_copyForPoolDataset=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep} \
                    --wrap="${env}
                        date
                            mkdir -p ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_none
                            rm ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_none/${datasetName}_${sample}_${ROE}_any_${ROI}_none_Tracked.mat
                            ln -s ${tmpDir}/traj_ROE_any_ROI_none/${sample}_${ROE}_any_${ROI}_none_Tracked.mat ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_none/${datasetName}_${sample}_${ROE}_any_${ROI}_none_Tracked.mat
                            
                        if [ -s ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_none/${datasetName}_${sample}_${ROE}_any_${ROI}_none_Tracked.mat ] ; then 
                            echo ${job} done! > ${tmpDir}/log/${job}.done
                        else exit 1; fi; date"`
                jobTraj_ROE_any_ROI_none_copyForPoolDataset_id=`echo :${jobTraj_ROE_any_ROI_none_copyForPoolDataset##* }`; jobTraj_ROE_any_ROI_none_copyForPoolDataset_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobTraj_ROE_any_ROI_none_copyForPoolDataset_id}; else job_all_id=${job_all_id}${jobTraj_ROE_any_ROI_none_copyForPoolDataset_id}; fi    ### add job id to a variable listing all job ids
                if [ -z ${jobTraj_ROE_any_ROI_none_copyForPoolDataset_allids} ]; then jobTraj_ROE_any_ROI_none_copyForPoolDataset_allids=${jobTraj_ROE_any_ROI_none_copyForPoolDataset_id}; else jobTraj_ROE_any_ROI_none_copyForPoolDataset_allids=${jobTraj_ROE_any_ROI_none_copyForPoolDataset_allids}${jobTraj_ROE_any_ROI_none_copyForPoolDataset_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobTraj_ROE_any_ROI_none_copyForPoolDataset_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobTraj_ROE_any_ROI_none_copyForPoolDataset_id#:}"
            fi

            job=Traj_ROE_any_ROI_none_trajQC_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                if [ -z ${jobTraj_ROE_any_ROI_none_assign_id} ]; then dep= ; else dep="--dependency=afterok${jobTraj_ROE_any_ROI_none_assign_id}" ; fi
                jobName=${job}...${outName}...${analysisName}
                jobTraj_ROE_any_ROI_none_trajQC=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep} \
                    --wrap="${env}
                        date
                        mkdir ${tmpDir}/Tracking_QC
                        source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
                        pyspaz_SAM track plot-trajectories \
                            --cap 10000 --alpha 1 --cmap Blues --min_jumps 2 \
                            -t ${tmpDir}/traj_ROE_any_ROI_none/${sample}_${ROE}_any_${ROI}_none_Tracked.mat \
                            -o ${tmpDir}/Tracking_QC/${sample}_${ROE}_any_${ROI}_none_plotTrajectories.png

                        if [ -s ${tmpDir}/Tracking_QC/${sample}_${ROE}_any_${ROI}_none_plotTrajectories.png ] ; then 
                            echo ${job} done! > ${tmpDir}/log/${job}.done
                        else exit 1; fi; date"`
                jobTraj_ROE_any_ROI_none_trajQC_id=`echo :${jobTraj_ROE_any_ROI_none_trajQC##* }`; jobTraj_ROE_any_ROI_none_trajQC_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobTraj_ROE_any_ROI_none_trajQC_id}; else job_all_id=${job_all_id}${jobTraj_ROE_any_ROI_none_trajQC_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobTraj_ROE_any_ROI_none_trajQC_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobTraj_ROE_any_ROI_none_trajQC_id#:}"
            fi

            job=Traj_ROE_any_ROI_none_trajQC_plotPdf_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                if [ -z ${jobTraj_ROE_any_ROI_none_assign_id} ]; then dep= ; else dep="--dependency=afterok${jobTraj_ROE_any_ROI_none_assign_id}" ; fi
                jobName=${job}...${outName}...${analysisName}
                jobTraj_ROE_any_ROI_none_trajQC_plotPdf=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep} \
                    --wrap="${env}
                        date
                        mkdir ${tmpDir}/Tracking_QC
                        source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
                        pyspaz_SAM track plot-trajectories \
                            --cap 1000 --alpha 1 --cmap Blues --min_jumps 2 \
                            -t ${tmpDir}/traj_ROE_any_ROI_none/${sample}_${ROE}_any_${ROI}_none_Tracked.mat \
                            -o ${tmpDir}/Tracking_QC/${sample}_${ROE}_any_${ROI}_none_plotTrajectories.pdf

                        if [ -s ${tmpDir}/Tracking_QC/${sample}_${ROE}_any_${ROI}_none_plotTrajectories.pdf ] ; then 
                            echo ${job} done! > ${tmpDir}/log/${job}.done
                        else exit 1; fi; date"`
                jobTraj_ROE_any_ROI_none_trajQC_plotPdf_id=`echo :${jobTraj_ROE_any_ROI_none_trajQC_plotPdf##* }`; jobTraj_ROE_any_ROI_none_trajQC_plotPdf_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobTraj_ROE_any_ROI_none_trajQC_plotPdf_id}; else job_all_id=${job_all_id}${jobTraj_ROE_any_ROI_none_trajQC_plotPdf_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobTraj_ROE_any_ROI_none_trajQC_plotPdf_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobTraj_ROE_any_ROI_none_trajQC_plotPdf_id#:}"
            fi

            ## Assign trajectorie: in ROE and in ROI (any time point)
            job=Traj_ROE_any_ROI_any_assign_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                if [ -z ${jobTraj_ROE_any_assign_id} ]; then dep= ; else dep="--dependency=afterok${jobTraj_ROE_any_assign_id}" ; fi
                jobName=${job}...${outName}...${analysisName}
                jobTraj_ROE_any_ROI_any_assign=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep} \
                    --wrap="${env}
                        date
                        mkdir ${tmpDir}/traj_ROE_any_ROI_any
                        source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
                        python ~/programs/SPTpy/interpolateMaskAndAssignTrajectories.py \
                            -i ${tmpDir}/traj_ROE_any/${sample}_${ROE}_any_Tracked.mat \
                            --mask_0_tif ${maskDir}/${sample}_BEFORE_${ROE}_${ROI}.tif \
                            --mask_1_tif ${maskDir}/${sample}_AFTER_${ROE}_${ROI}.tif \
                            --mask_0_frame_index 0 \
                            --mask_1_frame_index 30000 \
                            --olap_rule any \
                            --pixel_subsampling_factor 1 --pixel_size_um ${pixelSize} \
                            -o ${tmpDir}/traj_ROE_any_ROI_any/${sample}_${ROE}_any_${ROI}

                        if [ -s ${tmpDir}/traj_ROE_any_ROI_any/${sample}_${ROE}_any_${ROI}_any_Tracked.mat ] ; then 
                            echo ${job} done! > ${tmpDir}/log/${job}.done
                        else exit 1; fi; date"`
                jobTraj_ROE_any_ROI_any_assign_id=`echo :${jobTraj_ROE_any_ROI_any_assign##* }`; jobTraj_ROE_any_ROI_any_assign_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobTraj_ROE_any_ROI_any_assign_id}; else job_all_id=${job_all_id}${jobTraj_ROE_any_ROI_any_assign_id}; fi    ### add job id to a variable listing all job ids
                if [ -z ${jobTraj_ROE_any_ROI_any_assign_allids} ]; then jobTraj_ROE_any_ROI_any_assign_allids=${jobTraj_ROE_any_ROI_any_assign_id}; else jobTraj_ROE_any_ROI_any_assign_allids=${jobTraj_ROE_any_ROI_any_assign_allids}${jobTraj_ROE_any_ROI_any_assign_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobTraj_ROE_any_ROI_any_assign_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobTraj_ROE_any_ROI_any_assign_id#:}"
            fi

            ## simlink traj file in Pooled dataset directory
            ## this is done as a separate Job so the simlink is not created if the assignement did not work, so not broken link is created 
            job=Traj_ROE_any_ROI_any_copyForPoolDataset_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                if [ -z ${jobTraj_ROE_any_ROI_any_assign_id} ]; then dep= ; else dep="--dependency=afterok${jobTraj_ROE_any_ROI_any_assign_id}" ; fi
                jobName=${job}...${outName}...${analysisName}
                jobTraj_ROE_any_ROI_any_copyForPoolDataset=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep} \
                    --wrap="${env}
                        date
                            mkdir -p ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any
                            rm ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any/${datasetName}_${sample}_${ROE}_any_${ROI}_any_Tracked.mat
                            ln -s ${tmpDir}/traj_ROE_any_ROI_any/${sample}_${ROE}_any_${ROI}_any_Tracked.mat ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any/${datasetName}_${sample}_${ROE}_any_${ROI}_any_Tracked.mat
                            
                        if [ -s ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any/${datasetName}_${sample}_${ROE}_any_${ROI}_any_Tracked.mat ] ; then 
                            echo ${job} done! > ${tmpDir}/log/${job}.done
                        else exit 1; fi; date"`
                jobTraj_ROE_any_ROI_any_copyForPoolDataset_id=`echo :${jobTraj_ROE_any_ROI_any_copyForPoolDataset##* }`; jobTraj_ROE_any_ROI_any_copyForPoolDataset_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobTraj_ROE_any_ROI_any_copyForPoolDataset_id}; else job_all_id=${job_all_id}${jobTraj_ROE_any_ROI_any_copyForPoolDataset_id}; fi    ### add job id to a variable listing all job ids
                if [ -z ${jobTraj_ROE_any_ROI_any_copyForPoolDataset_allids} ]; then jobTraj_ROE_any_ROI_any_copyForPoolDataset_allids=${jobTraj_ROE_any_ROI_any_copyForPoolDataset_id}; else jobTraj_ROE_any_ROI_any_copyForPoolDataset_allids=${jobTraj_ROE_any_ROI_any_copyForPoolDataset_allids}${jobTraj_ROE_any_ROI_any_copyForPoolDataset_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobTraj_ROE_any_ROI_any_copyForPoolDataset_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobTraj_ROE_any_ROI_any_copyForPoolDataset_id#:}"
            fi

            job=Traj_ROE_any_ROI_any_trajQC_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                if [ -z ${jobTraj_ROE_any_ROI_any_assign_id} ]; then dep= ; else dep="--dependency=afterok${jobTraj_ROE_any_ROI_any_assign_id}" ; fi
                jobName=${job}...${outName}...${analysisName}
                jobTraj_ROE_any_ROI_any_trajQC=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep} \
                    --wrap="${env}
                        date
                        mkdir ${tmpDir}/Tracking_QC
                        source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
                        pyspaz_SAM track plot-trajectories \
                            --cap 10000 --alpha 1 --cmap magma --min_jumps 2 \
                            -t ${tmpDir}/traj_ROE_any_ROI_any/${sample}_${ROE}_any_${ROI}_any_Tracked.mat \
                            -o ${tmpDir}/Tracking_QC/${sample}_${ROE}_any_${ROI}_any_plotTrajectories.png

                        if [ -s ${tmpDir}/Tracking_QC/${sample}_${ROE}_any_${ROI}_any_plotTrajectories.png ] ; then 
                            echo ${job} done! > ${tmpDir}/log/${job}.done
                        else exit 1; fi; date"`
                jobTraj_ROE_any_ROI_any_trajQC_id=`echo :${jobTraj_ROE_any_ROI_any_trajQC##* }`; jobTraj_ROE_any_ROI_any_trajQC_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobTraj_ROE_any_ROI_any_trajQC_id}; else job_all_id=${job_all_id}${jobTraj_ROE_any_ROI_any_trajQC_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobTraj_ROE_any_ROI_any_trajQC_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobTraj_ROE_any_ROI_any_trajQC_id#:}"
            fi

            job=Traj_ROE_any_ROI_any_trajQC_plotPdf_${outName}
            if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
                if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
                if [ -z ${jobTraj_ROE_any_ROI_any_assign_id} ]; then dep= ; else dep="--dependency=afterok${jobTraj_ROE_any_ROI_any_assign_id}" ; fi
                jobName=${job}...${outName}...${analysisName}
                jobTraj_ROE_any_ROI_any_trajQC_plotPdf=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep} \
                    --wrap="${env}
                        date
                        mkdir ${tmpDir}/Tracking_QC
                        source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
                        pyspaz_SAM track plot-trajectories \
                            --cap 1000 --alpha 1 --cmap Reds --min_jumps 2 \
                            -t ${tmpDir}/traj_ROE_any_ROI_any/${sample}_${ROE}_any_${ROI}_any_Tracked.mat \
                            -o ${tmpDir}/Tracking_QC/${sample}_${ROE}_any_${ROI}_any_plotTrajectories.pdf

                        if [ -s ${tmpDir}/Tracking_QC/${sample}_${ROE}_any_${ROI}_any_plotTrajectories.pdf ] ; then 
                            echo ${job} done! > ${tmpDir}/log/${job}.done
                        else exit 1; fi; date"`
                jobTraj_ROE_any_ROI_any_trajQC_plotPdf_id=`echo :${jobTraj_ROE_any_ROI_any_trajQC_plotPdf##* }`; jobTraj_ROE_any_ROI_any_trajQC_plotPdf_name=${jobName}
                if [ -z ${job_all_id} ]; then job_all_id=${jobTraj_ROE_any_ROI_any_trajQC_plotPdf_id}; else job_all_id=${job_all_id}${jobTraj_ROE_any_ROI_any_trajQC_plotPdf_id}; fi    ### add job id to a variable listing all job ids
                sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobTraj_ROE_any_ROI_any_trajQC_plotPdf_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobTraj_ROE_any_ROI_any_trajQC_plotPdf_id#:}"
            fi            

        # end loop per ROE
        done

    # end loop per sample
    done 
done

# SPOTON per dataset 
for datasetName in "${datasets[@]}"; do
    echo $datasetName
    analysisName=$datasetName

    dataDescription=${mainInDir}/${datasetName}/ilastik/dataDescription.csv
    SptDataDir=${mainInDir}/${datasetName}/SPTdata_shortName
    maskDir=${mainInDir}/${datasetName}/ilastik/masks

    tmpDir=${mainTmpDir}/${datasetName}/pyspaz
    mkdir -p ${tmpDir}/std
    mkdir -p ${tmpDir}/log

    job=SpotOnMerged_ROE_any_ROI_none_${datasetName}
    if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
        if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
        if [ -z ${jobTraj_ROE_any_ROI_none_assign_allids} ]; then dep= ; else dep="--dependency=afterok${jobTraj_ROE_any_ROI_none_assign_allids}" ; fi
        jobName=${job}...${analysisName}
        jobSpotOnMerged_ROE_any_ROI_none=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep} \
            --wrap="${env}
                date
                mkdir ${tmpDir}/SpotOn
                source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
                SpotOn fit-and-plot-2states \
                    --input_format mat --time_between_frames ${timeBtwnFrame} --gaps_allowed 0 --localisation_error 0.028 \
                    --weight_delta_t True --model_fit CDF --max_jump_length 2 --max_jumps_per_traj 4 --max_delta_t 4 --diffusion_bound_range 0,0.02 --diffusion_free_range 0.2,20 \
                    --y_max 0.08 --x_max=1 \
                    -i ${tmpDir}/traj_ROE_any_ROI_none \
                    -o ${tmpDir}/ROE_any_ROI_none

                if [ -s ${tmpDir}/ROE_any_ROI_none_fit_2states.pdf ] ; then 
                    echo ${job} done! > ${tmpDir}/log/${job}.done
                else exit 1; fi; date"`
        jobSpotOnMerged_ROE_any_ROI_none_id=`echo :${jobSpotOnMerged_ROE_any_ROI_none##* }`; jobSpotOnMerged_ROE_any_ROI_none_name=${jobName}
        if [ -z ${job_all_id} ]; then job_all_id=${jobSpotOnMerged_ROE_any_ROI_none_id}; else job_all_id=${job_all_id}${jobSpotOnMerged_ROE_any_ROI_none_id}; fi    ### add job id to a variable listing all job ids
        sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobSpotOnMerged_ROE_any_ROI_none_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobSpotOnMerged_ROE_any_ROI_none_id#:}"
    fi


    job=SpotOnMerged_ROE_any_ROI_any_${datasetName}
    if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
        if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
        if [ -z ${jobTraj_ROE_any_ROI_any_assign_allids} ]; then dep= ; else dep="--dependency=afterok${jobTraj_ROE_any_ROI_any_assign_allids}" ; fi
        jobName=${job}...${analysisName}
        jobSpotOnMerged_ROE_any_ROI_any=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep} \
            --wrap="${env}
                date
                mkdir ${tmpDir}/SpotOn
                source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
                SpotOn fit-and-plot-2states \
                    --input_format mat --time_between_frames ${timeBtwnFrame} --gaps_allowed 0 --localisation_error 0.028 \
                    --weight_delta_t True --model_fit CDF --max_jump_length 2 --max_jumps_per_traj 4 --max_delta_t 4 --diffusion_bound_range 0,0.02 --diffusion_free_range 0.2,20 \
                    --y_max 0.08 --x_max=1 \
                    -i ${tmpDir}/traj_ROE_any_ROI_any \
                    -o ${tmpDir}/ROE_any_ROI_any

                if [ -s ${tmpDir}/ROE_any_ROI_any_fit_2states.pdf ] ; then 
                    echo ${job} done! > ${tmpDir}/log/${job}.done
                else exit 1; fi; date"`
        jobSpotOnMerged_ROE_any_ROI_any_id=`echo :${jobSpotOnMerged_ROE_any_ROI_any##* }`; jobSpotOnMerged_ROE_any_ROI_any_name=${jobName}
        if [ -z ${job_all_id} ]; then job_all_id=${jobSpotOnMerged_ROE_any_ROI_any_id}; else job_all_id=${job_all_id}${jobSpotOnMerged_ROE_any_ROI_any_id}; fi    ### add job id to a variable listing all job ids
        sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobSpotOnMerged_ROE_any_ROI_any_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobSpotOnMerged_ROE_any_ROI_any_id#:}"
    fi

done 

# # 
# 
# 
# source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
# pyspaz_SAM track tracking-qc -i Pool_datasets/allTraj/traj_ROE_any_ROI_any -o test_ROI_any
# pyspaz_SAM track tracking-qc -i Pool_datasets/allTraj/traj_ROE_any_ROI_none -o test_ROI_none


