import time
import lmfit
import numpy as np
from scipy.special import erfc
import os  
import glob as glob
import numpy as np
import fastspt.fastspt as fastspt
import fastspt.fastSPT_plot as fastSPT_plot
import fastspt.jumpsAngle as fastSPT_jumpsAngle
from scipy import io as sio
from pyspaz import spazio 
import math as math
import random as random

def pdistmean(m):
    import numpy as np 
    dists=[]
    for n in range(len(m)-1):
        dists.append(((m[n,0]-m[n+1,0])**2+(m[n,1]-m[n+1,1])**2)**0.5)
    return np.mean(dists)

def subsample_trajs(
    input_dirs,
    out_prefix,
    input_format='mat',
    min_traj_len=1,
    min_msrd=0,
    bootstrap_n=50,
    subsampling_n=3000,
    subsampling_f=0.5
):
    """
    """
    if os.path.isdir(input_dirs):  
        file_list =[item for sublist in [glob.glob(str("%s/*" + input_format) %  input_dir) for input_dir in str(input_dirs).split(',')] for item in sublist]  
        print('----- Loading trajectory file {}'.format(file_list[0]), flush=True)
        trajs, metadata, traj_cols = spazio.load_trajs(file_list[0])
        for file in file_list[1:] :
            print('----- Loading trajectory file {}'.format(file), flush=True)
            trajs_n, metadata, traj_cols = spazio.load_trajs(file)
            trajs = np.vstack((trajs, trajs_n))
    elif os.path.isfile(input_dirs):  
        print('----- Loading trajectory file {}'.format(input_dirs), flush=True)
        trajs, metadata, traj_cols = spazio.load_trajs(input_dirs)
    
    trajs_minLength = trajs[np.vectorize(len)(trajs[:,1])>=min_traj_len]
    trajs_minLength_MSRD_list=[]
    for i in range(len(trajs_minLength)):
        trajs_minLength_MSRD_list.append(pdistmean(trajs_minLength[i][0]))
    
    trajs_minLength_MSRD200= trajs_minLength[np.array(trajs_minLength_MSRD_list)>=min_msrd]
    
    if math.floor(len(trajs_minLength_MSRD200)*subsampling_f) < subsampling_n :
        subsampling_n = math.floor(len(trajs_minLength_MSRD200)*subsampling_f) 

    for i in range(1,bootstrap_n+1):
        trajs_minLength_MSRD200_sub = trajs_minLength_MSRD200[random.sample(range(len(trajs_minLength_MSRD200)), subsampling_n)]
        
        spazio.save_trajs(str(out_prefix)+'_sub' + str(i) + '_Tracked.mat', trajs_minLength_MSRD200_sub, metadata, traj_cols)
