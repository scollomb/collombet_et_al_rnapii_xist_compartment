import click

### order subcommand in help by their order in the code, not alphabetically 
class NaturalOrderGroup(click.Group):
    # def __init__(self, name=None, commands=None, **attrs):
    #     if commands is None:
    #         commands = OrderedDict()
    #     elif not isinstance(commands, OrderedDict):
    #         commands = OrderedDict(commands)
    #     click.Group.__init__(self, name=name,
    #                          commands=commands,
    #                          **attrs)
    
    def list_commands(self, ctx):
        return self.commands.keys()




@click.group(cls=NaturalOrderGroup)
def main():
    '''
    Spot-On is xxx .

    '''
def pdistmean(m):
    import numpy as np 
    dists=[]
    for n in range(len(m)-1):
        dists.append(((m[n,0]-m[n+1,0])**2+(m[n,1]-m[n+1,1])**2)**0.5)
    return np.mean(dists)


@main.command()
@click.option('-i', '--input_dirs', type = str, required=True, help = 'Multiple directories can be defined , separated by commas (no space). All with input_format <input_format> will be pooled.')
@click.option('-s', '--input_format', type = str, default = '.mat', help = ' All with this input_format in directorie(s) <input_dirs> will be pooled. default ".mat"')
@click.option('-o', '--out_prefix', type = str, default = None, help = 'default None')
@click.option('--max_jumps_per_traj', type = int, default = 3, help = 'default 3')
@click.option('--gaps_allowed', type = int, default = 0, help = 'default 0')
@click.option('--max_delta_t', type = int, default = 7, help = 'default 7')
@click.option('--max_jump_length', type = float, default = 3.0, help = 'default 3')
@click.option('--bin_width', type = float, default = 0.01, help = 'default 0.01')
@click.option('--plot_type', type = str, default = 'notScaledY', help = 'default notScaledY')
def plot_jumps_length(
    input_dirs,
    input_format,
    out_prefix,
    max_jumps_per_traj,
    gaps_allowed,
    max_delta_t,
    max_jump_length,
    bin_width,
    plot_type
):
    '''
    .     
    '''
    import os  
    import glob as glob
    import numpy as np
    import fastspt.fastspt as fastspt
    import fastspt.fastSPT_plot as fastSPT_plot
    import fastspt.jumpsLength as fastSPT_jumpsLength
    from scipy import io as sio
    
    if os.path.isdir(input_dirs):  
        file_list =[item for sublist in [glob.glob(str("%s/*" + input_format) %  input_dir) for input_dir in str(input_dirs).split(',')] for item in sublist]  
        print('----- Loading trajectory file {}'.format(file_list[0]), flush=True)
        trajs=sio.loadmat(file_list[0])['trackedPar'][:, [0,2,1]]
        
        for file in file_list[1:] :
            print('----- Loading trajectory file {}'.format(file), flush=True)
            trajs_n = sio.loadmat(file)['trackedPar']
            trajs_n = trajs_n[:, [0,2,1]]
            trajs = np.vstack((trajs, trajs_n))
    elif os.path.isfile(input_dirs):  
        print('----- Loading trajectory file {}'.format(input_dirs), flush=True)
        trajs=sio.loadmat(input_dirs)['trackedPar'][:, [0,2,1]]

    print('----- Computing histogram of jump length', flush=True)
    TransLengths = fastSPT_jumpsLength.compute_jump_lengths(trajs, useEntireTraj=False, maxDeltaT=max_delta_t, GapsAllowed=gaps_allowed, JumpsToConsider=max_jumps_per_traj)

    h1 = fastSPT_jumpsLength.compute_jump_length_distribution(TransLengths, maxDeltaT=max_delta_t, MaxJump=max_jump_length, BinWidth=bin_width)

    if ('notScaledY' in str(plot_type).split(',')) or ('all' in str(plot_type).split(',')):
        fastSPT_plot.plot_histogram_jumpsLengthCounts(jumpsLengthObject=h1,out_plot_file=str(out_prefix)+'_jumpsLengthDistrib.pdf', scaleY=False)
    if ('scaledY' in str(plot_type).split(',')) or ('all' in str(plot_type).split(',')):
        fastSPT_plot.plot_histogram_jumpsLengthCounts(jumpsLengthObject=h1,out_plot_file=str(out_prefix)+'_jumpsLengthDistrib_scaleY.pdf', scaleY=True)



@main.command()
@click.option('-i', '--input_dirs', type = str, required=True, help = 'Multiple directories can be defined , separated by commas (no space). All with input_format <input_format> will be pooled.')
@click.option('-o', '--out_prefix', type = str, default = None, help = 'default None')
@click.option('--input_format', type = str, default = 'mat', help = 'default mat')
@click.option('--max_jumps_per_traj', type = int, default = 3, help = 'default 3')
@click.option('--gaps_allowed', type = int, default = 0, help = 'default 0')
@click.option('--max_delta_t', type = int, default = 7, help = 'default 7')
@click.option('--min_1dt_jump_length', type = float, default = 0.1, help = 'default 0.1')
@click.option('--max_1dt_jump_length', type = float, default = 3, help = 'default 3')
@click.option('--bin_width', type = float, default = 10, help = 'default 10 degrees')
@click.option('--plot_type', type = str, default = 'notScaledY', help = 'default notScaledY')
def plot_jumps_angle(
    input_dirs,
    input_format,
    out_prefix,
    max_jumps_per_traj,
    min_1dt_jump_length,
    max_1dt_jump_length,
    gaps_allowed,
    max_delta_t,
    bin_width,
    plot_type
):
    '''
    .     
    '''
    import os  
    import glob as glob
    import numpy as np
    import fastspt.fastspt as fastspt
    import fastspt.fastSPT_plot as fastSPT_plot
    import fastspt.jumpsAngle as fastSPT_jumpsAngle
    from scipy import io as sio
    from pyspaz import spazio 
    
    if os.path.isdir(input_dirs):  
        file_list =[item for sublist in [glob.glob(str("%s/*" + input_format) %  input_dir) for input_dir in str(input_dirs).split(',')] for item in sublist]  
        print('----- Loading trajectory file {}'.format(file_list[0]), flush=True)
        trajs, metadata, traj_cols = spazio.load_trajs(file_list[0])
        for file in file_list[1:] :
            print('----- Loading trajectory file {}'.format(file), flush=True)
            trajs_n, metadata, traj_cols = spazio.load_trajs(file)
            trajs = np.vstack((trajs, trajs_n))
    elif os.path.isfile(input_dirs):  
        print('----- Loading trajectory file {}'.format(input_dirs), flush=True)
        trajs, metadata, traj_cols = spazio.load_trajs(input_dirs)


    ## filter trajs for MSRD > min_1dt_jump_length (used to get free trajectories)
    min_traj_len=3
    trajs_2jumps = trajs[np.vectorize(len)(trajs[:,1])>=min_traj_len]
    trajs_2jumps_MSRD_list=[]
    for i in range(len(trajs_2jumps)):
        trajs_2jumps_MSRD_list.append(pdistmean(trajs_2jumps[i][0]))
    
    trajs_2jumps_MSRD200= trajs_2jumps[np.array(trajs_2jumps_MSRD_list)>=min_1dt_jump_length]
    trajs=trajs_2jumps_MSRD200

    print('----- Computing histogram of jump angle', flush=True)
    TransAngles = fastSPT_jumpsAngle.compute_jump_angles(trajs, min_1dt_jump_length=min_1dt_jump_length, max_1dt_jump_length=max_1dt_jump_length, maxDeltaT=max_delta_t, GapsAllowed=gaps_allowed, JumpsToConsider=max_jumps_per_traj)

    h1 = fastSPT_jumpsAngle.compute_jump_angles_distribution(TransAngles, maxDeltaT=max_delta_t, BinWidth=bin_width)

    fastSPT_jumpsAngle.plot_histogram_jumpsAnglesDistrib(jumpsAngleObject=h1,BinWidth=bin_width,out_plot_file=str(out_prefix)+'_jumpsAngleDistrib_scaleY.pdf', scaleY=False)




@main.command()
@click.option('-i', '--input_dirs', type = str, required=True, help = 'Multiple directories can be defined , separated by commas (no space). All with input_format <input_format> will be pooled.')
@click.option('-o', '--out_prefix', type = str, default = None, help = 'default None')
@click.option('--input_format', type = str, default = 'mat', help = 'default mat')
@click.option('--max_jumps_per_traj', type = int, default = 3, help = 'default 3')
@click.option('--gaps_allowed', type = int, default = 0, help = 'default 0')
@click.option('--delta_t', type = int, default = 1, help = 'default 1')
@click.option('--min_1dt_jump_length', type = float, default = 0.1, help = 'default 0.1')
@click.option('--max_1dt_jump_length', type = float, default = 3, help = 'default 3')
@click.option('--bin_width', type = float, default = 10, help = 'default 10 degrees')
@click.option('--plot_type', type = str, default = 'notScaledY', help = 'default notScaledY')
def plot_jumps_angle_circular(
    input_dirs,
    input_format,
    out_prefix,
    max_jumps_per_traj,
    min_1dt_jump_length,
    max_1dt_jump_length,
    gaps_allowed,
    delta_t,
    bin_width,
    plot_type
):
    '''
    .     
    '''
    import os  
    import glob as glob
    import numpy as np
    import fastspt.fastspt as fastspt
    import fastspt.fastSPT_plot as fastSPT_plot
    import fastspt.jumpsAngle as fastSPT_jumpsAngle
    from scipy import io as sio
    from pyspaz import spazio 
    
    if os.path.isdir(input_dirs):  
        file_list =[item for sublist in [glob.glob(str("%s/*" + input_format) %  input_dir) for input_dir in str(input_dirs).split(',')] for item in sublist]  
        print('----- Loading trajectory file {}'.format(file_list[0]), flush=True)
        trajs, metadata, traj_cols = spazio.load_trajs(file_list[0])
        for file in file_list[1:] :
            print('----- Loading trajectory file {}'.format(file), flush=True)
            trajs_n, metadata, traj_cols = spazio.load_trajs(file)
            trajs = np.vstack((trajs, trajs_n))
    elif os.path.isfile(input_dirs):  
        print('----- Loading trajectory file {}'.format(input_dirs), flush=True)
        trajs, metadata, traj_cols = spazio.load_trajs(input_dirs)

    ## filter trajs for MSRD > min_1dt_jump_length (used to get free trajectories)
    min_traj_len=3
    trajs_2jumps = trajs[np.vectorize(len)(trajs[:,1])>=min_traj_len]
    trajs_2jumps_MSRD_list=[]
    for i in range(len(trajs_2jumps)):
        trajs_2jumps_MSRD_list.append(pdistmean(trajs_2jumps[i][0]))
    
    trajs_2jumps_MSRD200= trajs_2jumps[np.array(trajs_2jumps_MSRD_list)>=min_1dt_jump_length]
    trajs=trajs_2jumps_MSRD200
    
    print('----- Number of treajectories with 3 jumps and MSRD>' + str(min_1dt_jump_length)+ " : " + str(len(trajs)), flush=True)

    print('----- Computing histogram of jump angle', flush=True)
    TransAngles = fastSPT_jumpsAngle.compute_jump_angles_singledt(trajs, min_1dt_jump_length=min_1dt_jump_length, max_1dt_jump_length=max_1dt_jump_length, dt=1, GapsAllowed=gaps_allowed, JumpsToConsider=max_jumps_per_traj)

    h1 = fastSPT_jumpsAngle.compute_jump_angles_distribution_singledt(TransAngles, BinWidth=bin_width)
    
    fastSPT_jumpsAngle.plot_histogram_jumpsAnglesDistrib_circular(jumpsAngleObject=h1,BinWidth=bin_width,out_plot_file=str(out_prefix)+'_jumpsAngle.pdf', scaleY=False)
    
    f= open(str(out_prefix)+'_jumpsAngles.csv',"w+")
    f.write("angle\tdensity\n")
    for j in range(1, h1[2].shape[1]): 
        f.write("{}\t{}\n".format(
            h1[0][j],
            h1[2][0,j-1]
        ))
    f.close

@main.command()
@click.option('-i', '--input_dirs', type = str, required=True, help = 'Multiple directories can be defined , separated by commas (no space). All with input_format <input_format> will be pooled.')
@click.option('-o', '--out_prefix', type = str, default = None, help = 'default None')
@click.option('--input_format', type = str, default = 'mat', help = 'default mat')
@click.option('--max_jumps_per_traj', type = int, default = 3, help = 'default 3')
@click.option('--gaps_allowed', type = int, default = 0, help = 'default 0')
@click.option('--delta_t', type = int, default = 1, help = 'default 1')
@click.option('--min_1dt_jump_length', type = float, default = 0.1, help = 'default 0.1')
@click.option('--max_1dt_jump_length', type = float, default = 3, help = 'default 3')
@click.option('--bin_width', type = float, default = 10, help = 'default 10 degrees')
def jumps_angle_circular_countOnly(
    input_dirs,
    input_format,
    out_prefix,
    max_jumps_per_traj,
    min_1dt_jump_length,
    max_1dt_jump_length,
    gaps_allowed,
    delta_t,
    bin_width
):
    '''
    .     
    '''
    import os  
    import glob as glob
    import numpy as np
    import fastspt.fastspt as fastspt
    import fastspt.fastSPT_plot as fastSPT_plot
    import fastspt.jumpsAngle as fastSPT_jumpsAngle
    from scipy import io as sio
    from pyspaz import spazio 
    
    if os.path.isdir(input_dirs):  
        file_list =[item for sublist in [glob.glob(str("%s/*" + input_format) %  input_dir) for input_dir in str(input_dirs).split(',')] for item in sublist]  
        print('----- Loading trajectory file {}'.format(file_list[0]), flush=True)
        trajs, metadata, traj_cols = spazio.load_trajs(file_list[0])
        for file in file_list[1:] :
            print('----- Loading trajectory file {}'.format(file), flush=True)
            trajs_n, metadata, traj_cols = spazio.load_trajs(file)
            trajs = np.vstack((trajs, trajs_n))
    elif os.path.isfile(input_dirs):  
        print('----- Loading trajectory file {}'.format(input_dirs), flush=True)
        trajs, metadata, traj_cols = spazio.load_trajs(input_dirs)
    
    ## filter trajs for MSRD > min_1dt_jump_length (used to get free trajectories)
    min_traj_len=3
    trajs_2jumps = trajs[np.vectorize(len)(trajs[:,1])>=min_traj_len]
    trajs_2jumps_MSRD_list=[]
    for i in range(len(trajs_2jumps)):
        trajs_2jumps_MSRD_list.append(pdistmean(trajs_2jumps[i][0]))
    
    trajs_2jumps_MSRD200= trajs_2jumps[np.array(trajs_2jumps_MSRD_list)>=min_1dt_jump_length]
    trajs=trajs_2jumps_MSRD200

    print('----- Computing histogram of jump angle', flush=True)
    TransAngles = fastSPT_jumpsAngle.compute_jump_angles_singledt(trajs, min_1dt_jump_length=min_1dt_jump_length, max_1dt_jump_length=max_1dt_jump_length, dt=1, GapsAllowed=gaps_allowed, JumpsToConsider=max_jumps_per_traj)

    h1 = fastSPT_jumpsAngle.compute_jump_angles_distribution_singledt(TransAngles,dt=1, BinWidth=bin_width)

    f= open(str(out_prefix)+'_jumpsAngles.csv',"w+")
    f.write("angle\tdensity\n")
    for j in range(1, h1[2].shape[1]): 
        f.write("{}\t{}\n".format(
            h1[0][j],
            h1[2][0,j-1]
        ))
    f.close

@main.command()
@click.option('-i', '--input_dirs', type = str, required=True, help = 'Multiple directories can be defined , separated by commas (no space). All with input_format <input_format> will be pooled.')
@click.option('-o', '--out_prefix', type = str, default = None, help = 'default None')
@click.option('--input_format', type = str, default = 'mat', help = 'default mat')
@click.option('--max_jumps_per_traj', type = int, default = 3, help = 'default 3')
@click.option('--gaps_allowed', type = int, default = 0, help = 'default 0')
@click.option('--max_delta_t', type = int, default = 7, help = 'default 7')
@click.option('--max_jump_length', type = float, default = 2, help = 'default 2')
@click.option('--model_fit', type = str, default = 'CDF', help = 'CDF or PDF. default CDF')
@click.option('--bin_width', type = float, default = 0.01, help = 'default 0.01')
@click.option('--time_between_frames', type = float, default = 0.005477, help = 'default 00.5477')
@click.option('--diffusion_bound_range', type = str, default = '0, 0.05', help = 'default 0, 0.05')
@click.option('--diffusion_free_range', type = str, default = '0.2, 20', help = 'default 0.2, 10')
@click.option('--localisation_error', type = float, default = None, help = 'default None')
@click.option('--weight_delta_t', type = bool, default = True, help = 'default True')
@click.option('--figure_title', type = str, default = None, help = 'default None')
@click.option('--cmap', type = str, default ='viridis', help = 'default viridis')
@click.option('--y_max', type = float, default = None, help = 'default None')
@click.option('--x_max', type = float, default = None, help = 'default None')
def fit_and_plot_2states(
    input_dirs,
    input_format,
    out_prefix,
    max_jumps_per_traj,
    gaps_allowed,
    max_delta_t,
    max_jump_length,
    model_fit,
    bin_width,
    time_between_frames,
    localisation_error,
    diffusion_bound_range,
    diffusion_free_range,
    weight_delta_t,
    figure_title,
    cmap,
    y_max,
    x_max
):
    '''
    .     
    '''
    import os  
    import glob as glob
    import numpy as np
    import fastspt.fastspt as fastspt
    import fastspt.fastSPT_plot as fastSPT_plot
    import fastspt.jumpsLength as fastSPT_jumpsLength
    import fastspt.simulateAndFit as fastSPT_simulateAndFit
    from scipy import io as sio
    from matplotlib import pyplot as plt
    import pandas as pd
    from pyspaz import spazio
    
    if os.path.isdir(input_dirs):  
        file_list =[item for sublist in [glob.glob(str("%s/*" + input_format) %  input_dir) for input_dir in str(input_dirs).split(',')] for item in sublist]  
        print('----- Loading trajectory file {}'.format(file_list[0]), flush=True)
        if (input_format=='mat'):
            trajs=sio.loadmat(file_list[0])['trackedPar'][:, [0,2,1]]
        elif (input_format=='4DN'):
            trajs=spazio.load_trajs_4DN(file_list[0])
        for file in file_list[1:] :
            print('----- Loading trajectory file {}'.format(file), flush=True)
            if (input_format=='mat'):
                trajs_n=sio.loadmat(file)['trackedPar'][:, [0,2,1]]
            elif (input_format=='4DN'):
                trajs_n=spazio.load_trajs_4DN(file)
            trajs = np.vstack((trajs, trajs_n))
    elif os.path.isfile(input_dirs):  
        print('----- Loading trajectory file {}'.format(input_dirs), flush=True)
        if (input_format=='mat'):
            trajs=sio.loadmat(input_dirs)['trackedPar'][:, [0,2,1]]
        elif (input_format=='4DN'):
            trajs=spazio.load_trajs_4DN(input_dirs)
    
    print('----- Computing jump length', flush=True)
    TransLengths = fastSPT_jumpsLength.compute_jump_lengths(trajs, useEntireTraj=False, maxDeltaT=max_delta_t, GapsAllowed=gaps_allowed, JumpsToConsider=max_jumps_per_traj)

    print('----- Computing histogram', flush=True)
    h1 = fastSPT_jumpsLength.compute_jump_length_distribution(TransLengths, maxDeltaT=max_delta_t, MaxJump=max_jump_length, BinWidth=bin_width)

    Frac_Bound = [0, 1]
    D_Free = [float(x) for x in str(diffusion_free_range).split(',')]
    D_Bound = [float(x) for x in str(diffusion_bound_range).split(',')]
    sigma_bound = [0.005, 0.1]                                   ## This line change
    LB = [D_Free[0], D_Bound[0], Frac_Bound[0], sigma_bound[0]]  ## This line too
    UB = [D_Free[1], D_Bound[1], Frac_Bound[1], sigma_bound[1]]  ## And this line

    params = {'UB': UB,
            'LB': LB,
            'iterations': 3, # Manually input the desired number of fitting iterations:
            'dT': time_between_frames, # Time between frames in seconds
            'dZ': 0.700, # The axial illumination slice: measured to be roughly 700 nm
            'ModelFit': model_fit,
            'LocError': None, # Manually input the localization error in um: 35 nm = 0.035 um.
            'fitSigma': True,
            'a': 0.15716,
            'b': 0.20811,
            'useZcorr': True,
            'weight_delta_t': weight_delta_t
    }
    if localisation_error != None:
          params['LocError']  = localisation_error
          params['fitSigma']  = False
    
    ## Perform the fit
    print('----- Fitting model', flush=True)
    fit = fastSPT_simulateAndFit.fit_2states_model(jumpsLengthObject=h1, **params)
    
    print('----- simulating model histogram', flush=True)
    y = fastSPT_simulateAndFit.generate_jump_length_distribution(h1, fit.params,
                                                LocError = fit.params['sigma'].value,
                                                dT = params['dT'], dZ = params['dZ'],
                                                a = params['a'], b = params['b'], fit2states=True, norm=True, useZcorr=params['useZcorr'])
    
    y*=float(len(h1[3]))/float(len(h1[0]))
    

    print('----- plotting data '+str(out_prefix)+'_fit_2states.pdf', flush=True)
    fastSPT_plot.plot_histogram_jumpsLengthPDF_Fit(
        jumpsLengthObject=h1, 
        modelHistogramObject=y,
        cmap=cmap,
        out_plot_file=str(out_prefix)+'_fit_2states.pdf', 
        TimeGap=time_between_frames,
        title="Bound fraction: {}% \n Bound diffusion coefficient: {} um2/s \n Free diffusion coefficient: {} um2/s \n Localisation error: {} \n Mean trajectory length: {} \n Trajectories with at leat 2 jumps: {}".format(
        round(fit.params['F_bound'].value*100,1),
        round(fit.params['D_bound'].value, 2),
        round(fit.params['D_free'].value,2),
        round(fit.params['sigma'].value,3),
        round(np.mean(np.array([len(x[1][0]) for x in trajs]).astype(int)),2),
        np.sum(np.array([len(x[1][0])>2 for x in trajs]).astype(int))),
        y_max=y_max, x_max=x_max
    )
    
    f= open(str(out_prefix)+'_fit_2states_params.csv',"w+")
    f.write("BoundFraction\tDiffusionCoefficientBound\tDiffusionCoefficientFree\tLocalisationError\tMeanTrajLength\tNbTrajMin3\n")
    f.write("{}\t{}\t{}\t{}\t{}\t{}\n".format(
        fit.params['F_bound'].value,
        fit.params['D_bound'].value,
        fit.params['D_free'].value,
        fit.params['sigma'].value,
        round(np.mean(np.vectorize(len)(trajs[:,1])),2),
        np.sum((np.vectorize(len)(trajs[:,1])>2).astype(int))
    ))
    f.close



@main.command()
@click.option('-i', '--input_dirs', type = str, required=True, help = 'Multiple directories can be defined , separated by commas (no space). All with input_format <input_format> will be pooled.')
@click.option('-o', '--out_prefix', type = str, default = None, help = 'default None')
@click.option('--input_format', type = str, default = 'mat', help = 'default mat')
@click.option('--max_jumps_per_traj', type = int, default = 3, help = 'default 3')
@click.option('--gaps_allowed', type = int, default = 0, help = 'default 0')
@click.option('--max_delta_t', type = int, default = 7, help = 'default 7')
@click.option('--max_jump_length', type = float, default = 2, help = 'default 2')
@click.option('--model_fit', type = str, default = 'CDF', help = 'CDF or PDF. default CDF')
@click.option('--bin_width', type = float, default = 0.01, help = 'default 0.01')
@click.option('--time_between_frames', type = float, default = 0.005477, help = 'default 00.5477')
@click.option('--diffusion_bound_range', type = str, default = '0, 0.05', help = 'default 0, 0.05')
@click.option('--diffusion_free_range', type = str, default = '0.2, 20', help = 'default 0.2, 10')
@click.option('--localisation_error', type = float, default = None, help = 'default None')
@click.option('--weight_delta_t', type = bool, default = True, help = 'default True')
def fit_2states(
    input_dirs,
    input_format,
    out_prefix,
    max_jumps_per_traj,
    gaps_allowed,
    max_delta_t,
    max_jump_length,
    model_fit,
    bin_width,
    time_between_frames,
    localisation_error,
    diffusion_bound_range,
    diffusion_free_range,
    weight_delta_t,
):
    '''
    .     
    '''
    import os  
    import glob as glob
    import numpy as np
    import fastspt.fastspt as fastspt
    import fastspt.fastSPT_plot as fastSPT_plot
    import fastspt.jumpsLength as fastSPT_jumpsLength
    import fastspt.simulateAndFit as fastSPT_simulateAndFit
    from scipy import io as sio
    from matplotlib import pyplot as plt
    import pandas as pd
    from pyspaz import spazio
    
    if os.path.isdir(input_dirs):  
        file_list =[item for sublist in [glob.glob(str("%s/*" + input_format) %  input_dir) for input_dir in str(input_dirs).split(',')] for item in sublist]  
        print('----- Loading trajectory file {}'.format(file_list[0]), flush=True)
        if (input_format=='mat'):
            trajs, metadata, traj_cols = spazio.load_trajs(file_list[0])
        elif (input_format=='4DN'):
            trajs=spazio.load_trajs_4DN(file_list[0])
        for file in file_list[1:] :
            print('----- Loading trajectory file {}'.format(file), flush=True)
            if (input_format=='mat'):
                trajs_n, metadata, traj_cols = spazio.load_trajs(file)
            elif (input_format=='4DN'):
                trajs_n=spazio.load_trajs_4DN(file)
            trajs = np.vstack((trajs, trajs_n))
    elif os.path.isfile(input_dirs):  
        print('----- Loading trajectory file {}'.format(input_dirs), flush=True)
        if (input_format=='mat'):
            trajs, metadata, traj_cols = spazio.load_trajs(input_dirs)
        elif (input_format=='4DN'):
            trajs=spazio.load_trajs_4DN(input_dirs)
    
    print('----- Computing jump length', flush=True)
    TransLengths = fastSPT_jumpsLength.compute_jump_lengths(trajs, useEntireTraj=False, maxDeltaT=max_delta_t, GapsAllowed=gaps_allowed, JumpsToConsider=max_jumps_per_traj)

    print('----- Computing histogram', flush=True)
    h1 = fastSPT_jumpsLength.compute_jump_length_distribution(TransLengths, maxDeltaT=max_delta_t, MaxJump=max_jump_length, BinWidth=bin_width)

    Frac_Bound = [0, 1]
    D_Free = [float(x) for x in str(diffusion_free_range).split(',')]
    D_Bound = [float(x) for x in str(diffusion_bound_range).split(',')]
    sigma_bound = [0.005, 0.1]                                   ## This line change
    LB = [D_Free[0], D_Bound[0], Frac_Bound[0], sigma_bound[0]]  ## This line too
    UB = [D_Free[1], D_Bound[1], Frac_Bound[1], sigma_bound[1]]  ## And this line

    params = {'UB': UB,
            'LB': LB,
            'iterations': 3, # Manually input the desired number of fitting iterations:
            'dT': time_between_frames, # Time between frames in seconds
            'dZ': 0.700, # The axial illumination slice: measured to be roughly 700 nm
            'ModelFit': model_fit,
            'LocError': None, # Manually input the localization error in um: 35 nm = 0.035 um.
            'fitSigma': True,
            'a': 0.15716,
            'b': 0.20811,
            'useZcorr': True,
            'weight_delta_t': weight_delta_t
    }
    if localisation_error != None:
          params['LocError']  = localisation_error
          params['fitSigma']  = False
    
    ## Perform the fit
    print('----- Fitting model', flush=True)
    fit = fastSPT_simulateAndFit.fit_2states_model(jumpsLengthObject=h1, **params)
    
    print('----- Savign results to table', flush=True)
    
    f= open(str(out_prefix)+'_fit_2states_params.csv',"w+")
    f.write("BoundFraction\tDiffusionCoefficientBound\tDiffusionCoefficientFree\tLocalisationError\tMeanTrajLength\tNbTrajMin3\n")
    f.write("{}\t{}\t{}\t{}\t{}\t{}\n".format(
        fit.params['F_bound'].value,
        fit.params['D_bound'].value,
        fit.params['D_free'].value,
        fit.params['sigma'].value,
        round(np.mean(np.vectorize(len)(trajs[:,1])),2),
        np.sum((np.vectorize(len)(trajs[:,1])>2).astype(int))
    ))
    f.close




@main.command()
@click.option('-i', '--input_dirs', type = str, required=True, help = 'Multiple directories can be defined , separated by commas (no space). All with input_format <input_format> will be pooled.')
@click.option('-s', '--input_format', type = str, default = '.mat', help = ' All with this input_format in directorie(s) <input_dirs> will be pooled. default ".mat"')
@click.option('-o', '--out_prefix', type = str, default = None, help = 'default None')
@click.option('--max_jumps_per_traj', type = int, default = 3, help = 'default 3')
@click.option('--gaps_allowed', type = int, default = 0, help = 'default 0')
@click.option('--max_delta_t', type = int, default = 7, help = 'default 7')
@click.option('--max_jump_length', type = float, default = 2, help = 'default 2')
@click.option('--bin_width', type = float, default = 0.01, help = 'default 0.01')
@click.option('--time_between_frames', type = float, default = 0.005477, help = 'default 00.5477')
@click.option('--diffusion_bound_range', type = str, default = '0, 0.001', help = 'default 0, 0.001')
@click.option('--diffusion_med_range', type = str, default = '0.15, 10', help = 'default 0.15, 10')
@click.option('--diffusion_fast_range', type = str, default = '0.15, 25', help = 'default 0.15, 25')
@click.option('--localisation_error', type = float, default = None, help = 'default None')
@click.option('--figure_title', type = str, default = None, help = 'default None')
def fit_and_plot_3states(
    input_dirs,
    input_format,
    out_prefix,
    max_jumps_per_traj,
    gaps_allowed,
    max_delta_t,
    max_jump_length,
    bin_width,
    time_between_frames,
    localisation_error,
    diffusion_bound_range,
    diffusion_med_range,
    diffusion_fast_range,
    figure_title
):
    '''
    .     
    '''
    import os  
    import glob as glob
    import numpy as np
    import fastspt.fastspt as fastspt
    import fastspt.fastSPT_plot as fastSPT_plot
    import fastspt.jumpsLength as fastSPT_jumpsLength
    import fastspt.simulateAndFit as fastSPT_simulateAndFit
    from scipy import io as sio
    from matplotlib import pyplot as plt
    
    if os.path.isdir(input_dirs):  
        file_list =[item for sublist in [glob.glob(str("%s/*" + input_format) %  input_dir) for input_dir in str(input_dirs).split(',')] for item in sublist]  
        print('----- Loading trajectory file {}'.format(file_list[0]), flush=True)
        trajs=sio.loadmat(file_list[0])['trackedPar'][:, [0,2,1]]
        
        for file in file_list[1:] :
            print('----- Loading trajectory file {}'.format(file), flush=True)
            trajs_n = sio.loadmat(file)['trackedPar']
            trajs_n = trajs_n[:, [0,2,1]]
            trajs = np.vstack((trajs, trajs_n))
    elif os.path.isfile(input_dirs):  
        print('----- Loading trajectory file {}'.format(input_dirs), flush=True)
        trajs=sio.loadmat(input_dirs)['trackedPar'][:, [0,2,1]]
    
    
    print('----- Computing jump length', flush=True)
    TransLengths = fastSPT_jumpsLength.compute_jump_lengths(trajs, useEntireTraj=False, maxDeltaT=max_delta_t, GapsAllowed=gaps_allowed, JumpsToConsider=max_jumps_per_traj)

    print('----- Computing histogram', flush=True)
    h1 = fastSPT_jumpsLength.compute_jump_length_distribution(TransLengths, maxDeltaT=max_delta_t, MaxJump=max_jump_length, BinWidth=bin_width)

    Frac_Bound = [0, 1]
    Frac_Fast = [0, 1]
    D_Fast = [float(x) for x in str(diffusion_fast_range).split(',')]
    D_Med = [float(x) for x in str(diffusion_med_range).split(',')]
    D_Bound = [float(x) for x in str(diffusion_bound_range).split(',')]
    sigma_bound = [0.005, 0.1]                                   ## This line change
    LB = [D_Fast[0], D_Med[0], D_Bound[0], Frac_Fast[0], Frac_Bound[0], sigma_bound[0]]  ## This line too
    UB = [D_Fast[1], D_Med[1], D_Bound[1], Frac_Fast[1], Frac_Bound[1], sigma_bound[1]]  ## And this line

    params = {'UB': UB,
            'LB': LB,
            'iterations': 3, # Manually input the desired number of fitting iterations:
            'dT': time_between_frames, # Time between frames in seconds
            'dZ': 0.700, # The axial illumination slice: measured to be roughly 700 nm
            'ModelFit': 1,
            'LocError': None, # Manually input the localization error in um: 35 nm = 0.035 um.
            'fitSigma': True,
            'a': 0.15716,
            'b': 0.20811,
            'useZcorr': True
    }
    if localisation_error != None:
          params['LocError']  = localisation_error
          params['fitSigma']  = False
    
    ## Perform the fit
    print('----- Fitting model', flush=True)
    fit = fastSPT_simulateAndFit.fit_3states_model(jumpsLengthObject=h1, **params)
    
    print('----- simulating model histogram', flush=True)
    y = fastSPT_simulateAndFit.generate_jump_length_distribution(h1, fit.params,
                                                LocError = fit.params['sigma'].value,
                                                dT = params['dT'], dZ = params['dZ'],
                                                a = params['a'], b = params['b'], fit2states=False, norm=True, useZcorr=params['useZcorr'])
    
    y*=float(len(h1[3]))/float(len(h1[0]))
    

    print('----- plotting data', flush=True)
    fastSPT_plot.plot_histogram_jumpsLengthPDF_Fit(
        jumpsLengthObject=h1, 
        modelHistogramObject=y, 
        out_plot_file=str(out_prefix)+'_fit_2states.pdf', 
        TimeGap=time_between_frames,
        title="Bound fraction: {}% \n med-free fraction: {}% \n Fast-free fraction: {}% \n Bound diffusion coefficient: {} um2/s \n med-Free diffusion coefficient: {} um2/s \n fast-Free diffusion coefficient: {} um2/s \n Localisation error: {} \n Mean trajectory length: {} \n Trajectories with at leat 2 points: {}".format(
        round(fit.params['F_bound'].value*100,1),
        100-(round(fit.params['F_bound'].value*100,1)+round(fit.params['F_fast'].value*100,1)),
        round(fit.params['F_fast'].value*100,1),
        round(fit.params['D_bound'].value, 2),
        round(fit.params['D_med'].value,2),
        round(fit.params['D_fast'].value,2),
        round(fit.params['sigma'].value,3),
        round(np.mean(np.array([len(x[1][0]) for x in trajs]).astype(int)),2),
        np.sum(np.array([len(x[1][0])>1 for x in trajs]).astype(int)))
    )
    
    f= open(str(out_prefix)+'_fit_2states_params.csv',"w+")
    f.write("BoundFraction\tDiffusionCoefficientBound\tDiffusionCoefficientMed\tDiffusionCoefficientFast\tLocalisationError\n")
    f.write("{}\t{}\t{}\t{}".format(
        fit.params['F_bound'].value,
        fit.params['D_bound'].value,
        round(fit.params['D_med'].value,2),
        fit.params['D_fast'].value,
        fit.params['sigma'].value
    ))
    f.close




@main.command()
@click.option('-i', '--input_dirs', type = str, required=True, help = 'Multiple directories can be defined , separated by commas (no space). All with input_format <input_format> will be pooled.')
@click.option('-s', '--input_format', type = str, default = '.mat', help = ' All with this input_format in directorie(s) <input_dirs> will be pooled. default ".mat"')
@click.option('-o', '--out_prefix', type = str, default = None, help = 'default None')
@click.option('--min_traj_len', type = int, default = 1, help = 'default 1')
@click.option('--min_msrd', type = float, default = 0, help = 'default 0')
@click.option('--bootstrap_n', type = int, default = 50, help = 'default 50')
@click.option('--subsampling_n', type = int, default = 3000, help = 'default 3000')
@click.option('--subsampling_f', type = float, default = 0.5, help = 'default 0.5')
def subsample_trajs(
    input_dirs,
    input_format,
    out_prefix,
    min_traj_len,
    min_msrd,
    bootstrap_n,
    subsampling_n,
    subsampling_f,

):
    '''
    .     
    '''
    import fastspt.bootstrap as fastSPT_bootstrap
    fastSPT_bootstrap.subsample_trajs(
    input_dirs,
    out_prefix,
    input_format,
    min_traj_len,
    min_msrd,
    bootstrap_n,
    subsampling_n,
    subsampling_f
    )

    
