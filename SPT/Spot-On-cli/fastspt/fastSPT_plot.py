# A packaged version of the fastSPT code by Anders Sejr Hansen, Feb. 2016
# Python rewriting by MW, March 2017
#
# In this module we put all the plotting functions
# 
# History: For the history of the script see the related CHANGELOG file.

## ==== Imports
import matplotlib.pyplot as plt
import numpy as np


def plot_histogram_jumpsLengthCounts(
    jumpsLengthObject,
    out_plot_file,
    TimeGap=None,
    scaleY=False
):
    """Function that plots an empirical histogram of jump lengths,
    with an optional overlay of simulated/theoretical histogram of 
    jump lengths"""
    
    histogram_spacer = 0.055
    number = jumpsLengthObject[1].shape[0]
    cmap = plt.get_cmap('viridis')
    colour = [cmap(i) for i in np.linspace(0, 1, number)]
    
    if scaleY: 
        fig, axs = plt.subplots(jumpsLengthObject[1].shape[0], figsize=(5,10), sharex=True, sharey =True)
    else :
        fig, axs = plt.subplots(jumpsLengthObject[1].shape[0], figsize=(5,10), sharex=True)
    
    # for each delta t
    for i in range(jumpsLengthObject[1].shape[0]-1, -1, -1): 
        colour_element = colour[i] #colour[round(i/size(jumpsLengthObject[1],1)*size(colour,1)),:]
        # we use plt.fill and loop through each bin to control exactly the shape of each bar
        for j in range(1, jumpsLengthObject[1].shape[1]): 
            x1 = jumpsLengthObject[0][j-1]
            x2 = jumpsLengthObject[0][j]
            y1 = 0
            y2 = jumpsLengthObject[1][i,j-1]
            axs[i].fill([x1, x1, x2, x2], [y1, y2, y2, y1], color=colour_element)
        
        if scaleY :
            axs[i].set_ylim(0, 1.1*max([max(x) for x in jumpsLengthObject[1]]))
            midY=0.5*max([max(x) for x in jumpsLengthObject[1]])
        else: 
            axs[i].set_ylim(bottom=0)
            midY=0.5*max(jumpsLengthObject[1][i])
        
        if TimeGap != None:
            axs[i].text(0.5*max(jumpsLengthObject[0]), midY, '$\Delta t$ : {} ms ({} jumps)'.format(TimeGap*(i+1), int(sum(jumpsLengthObject[1][i,:]))) )
        else:
            axs[i].text(0.5*max(jumpsLengthObject[0]), midY, '${}\Delta t$ ({} jumps)'.format(i+1, int(sum(jumpsLengthObject[1][i,:]))) )
        
        axs[i].set_xlim(0,jumpsLengthObject[0].max())
    
    plt.ylabel('Count')
    plt.xlabel('jump length ($\mu m$)')
    
    fig.subplots_adjust(hspace=0.1)
    fig.savefig(out_plot_file)
    plt.close(fig)



def plot_histogram_jumpsLengthPDF_Fit(
    jumpsLengthObject,
    modelHistogramObject,
    out_plot_file,
    title=None,
    title_font_size=8, 
    TimeGap=None,
    cmap='viridis',
    y_max=None,
    x_max=None
):
    """Function that plots an empirical histogram of jump lengths,
    with an optional overlay of simulated/theoretical histogram of 
    jump lengths"""
    
    histogram_spacer = 0.055
    number = jumpsLengthObject[1].shape[0]
    cmap = plt.get_cmap(cmap)
    colour = [cmap(i) for i in np.linspace(0.2, 0.9, number)]
    
    fig, axs = plt.subplots(jumpsLengthObject[1].shape[0], figsize=(5,10), sharex=True)
    
    # for each delta t
    for i in range(jumpsLengthObject[1].shape[0]-1, -1, -1): 
        colour_element = colour[i] #colour[round(i/size(jumpsLengthObject[1],1)*size(colour,1)),:]
        # we use plt.fill and loop through each bin to control exactly the shape of each bar
        for j in range(1, jumpsLengthObject[1].shape[1]): 
            x1 = jumpsLengthObject[0][j-1]
            x2 = jumpsLengthObject[0][j]
            y1 = 0
            y2 = jumpsLengthObject[2][i,j-1]
            axs[i].fill([x1, x1, x2, x2], [y1, y2, y2, y1], color=colour_element)
        
        axs[i].plot(jumpsLengthObject[3][:], modelHistogramObject[i][0][:], linestyle='-', linewidth=2, color='w', alpha=0.5)
        axs[i].plot(jumpsLengthObject[3][:], modelHistogramObject[i][1][:], linestyle='-', linewidth=2, color='w', alpha=0.5)
        axs[i].plot(jumpsLengthObject[3][:], modelHistogramObject[i][2][:], linestyle='-', linewidth=2, color='w', alpha=0.5)
        if len(modelHistogramObject[i])==4: 
            axs[i].plot(jumpsLengthObject[3][:], modelHistogramObject[i][2][:], linestyle='-', linewidth=2, color='w', alpha=0.5)
        
        axs[i].plot(jumpsLengthObject[3][:], modelHistogramObject[i][0][:], linestyle='-', linewidth=1, color='k')
        axs[i].plot(jumpsLengthObject[3][:], modelHistogramObject[i][1][:], linestyle=':', linewidth=1, color='k')
        axs[i].plot(jumpsLengthObject[3][:], modelHistogramObject[i][2][:], linestyle='--', linewidth=1, color='k')
        if len(modelHistogramObject[i])==4: 
            axs[i].plot(jumpsLengthObject[3][:], modelHistogramObject[i][2][:], linestyle='-.', linewidth=1, color='k')
        
        if y_max != None:
            axs[i].set_ylim(0, y_max)
            midY=0.5*y_max
        else :
            axs[i].set_ylim(bottom=0)
            midY=0.5*max(jumpsLengthObject[1][i])
        if x_max != None:
            axs[i].set_xlim(0, x_max)
        else:
            x_max=max(jumpsLengthObject[0])
            axs[i].set_xlim(0,jumpsLengthObject[0].max())

        if TimeGap != None:
            axs[i].text(0.5*x_max, midY, '$\Delta t$ : {} ms ({} jumps)'.format(round(1000*TimeGap*(i+1),1), int(sum(jumpsLengthObject[1][i,:]))) )
        else:
            axs[i].text(0.5*x_max, midY, '${}\Delta t$ ({} jumps)'.format(i+1, int(sum(jumpsLengthObject[1][i,:]))) )
        
    
    plt.xlabel('jump length ($\mu m$)')
    
    fig.suptitle(title, fontsize=title_font_size)
    
    fig.subplots_adjust(hspace=0.1, left=0.2)
    fig.text(0.01, 0.5, 'Jump length probability', va='center', rotation='vertical', fontsize=12)
    fig.savefig(out_plot_file)
    plt.close(fig)



def plot_histogram_jumpsLengthCDF_Fit(
    jumpsLengthObject,
    modelHistogramObject,
    out_plot_file,
    title=None,
    title_font_size=8, 
    TimeGap=None,
    y_max=None,
    x_max=None
):
    """Function that plots an empirical histogram of jump lengths,
    with an optional overlay of simulated/theoretical histogram of 
    jump lengths"""
    
    histogram_spacer = 0.055
    number = jumpsLengthObject[1].shape[0]
    cmap = plt.get_cmap('viridis')
    colour = [cmap(i) for i in np.linspace(0, 1, number)]
    
    fig, axs = plt.subplots(jumpsLengthObject[1].shape[0], figsize=(5,10), sharex=True)
    
    # for each delta t
    for i in range(jumpsLengthObject[1].shape[0]-1, -1, -1): 
        colour_element = colour[i] #colour[round(i/size(jumpsLengthObject[1],1)*size(colour,1)),:]
        # we use plt.fill and loop through each bin to control exactly the shape of each bar
        for j in range(1, jumpsLengthObject[1].shape[1]): 
            x1 = jumpsLengthObject[3][j-1]
            x2 = jumpsLengthObject[3][j]
            y1 = 0
            y2 = jumpsLengthObject[2][i,j-1]
            axs[i].fill([x1, x1, x2, x2], [y1, y2, y2, y1], color=colour_element)
        
        axs[i].plot(jumpsLengthObject[3][:], modelHistogramObject[i][0][:], linestyle='-', linewidth=2, color='w', alpha=0.5)
        axs[i].plot(jumpsLengthObject[3][:], modelHistogramObject[i][1][:], linestyle='-', linewidth=2, color='w', alpha=0.5)
        axs[i].plot(jumpsLengthObject[3][:], modelHistogramObject[i][2][:], linestyle='-', linewidth=2, color='w', alpha=0.5)
        if len(modelHistogramObject[i])==4: 
            axs[i].plot(jumpsLengthObject[3][:], modelHistogramObject[i][2][:], linestyle='-', linewidth=2, color='w', alpha=0.5)
        
        axs[i].plot(jumpsLengthObject[3][:], modelHistogramObject[i][0][:], linestyle='-', linewidth=1, color='k')
        axs[i].plot(jumpsLengthObject[3][:], modelHistogramObject[i][1][:], linestyle=':', linewidth=1, color='k')
        axs[i].plot(jumpsLengthObject[3][:], modelHistogramObject[i][2][:], linestyle='--', linewidth=1, color='k')
        if len(modelHistogramObject[i])==4: 
            axs[i].plot(jumpsLengthObject[3][:], modelHistogramObject[i][2][:], linestyle='-.', linewidth=1, color='k')
        
        if y_max != None:
            axs[i].set_ylim(0, y_max)
            midY=0.5*y_max
        else :
            axs[i].set_ylim(bottom=0)
            midY=0.5*max(jumpsLengthObject[1][i])
        if x_max != None:
            axs[i].set_xlim(0, x_max)
        else:
            x_max=max(jumpsLengthObject[3])
            axs[i].set_xlim(0,jumpsLengthObject[3].max())

        if TimeGap != None:
            axs[i].text(0.5*x_max, midY, '$\Delta t$ : {} ms ({} jumps)'.format(round(1000*TimeGap*(i+1),1), int(sum(jumpsLengthObject[1][i,:]))) )
        else:
            axs[i].text(0.5*x_max, midY, '${}\Delta t$ ({} jumps)'.format(i+1, int(sum(jumpsLengthObject[1][i,:]))) )
        
    
    plt.xlabel('jump length ($\mu m$)')
    
    fig.suptitle(title, fontsize=title_font_size)
    
    fig.subplots_adjust(hspace=0.1, left=0.2)
    fig.text(0.01, 0.5, 'Jump length probability', va='center', rotation='vertical', fontsize=12)
    fig.savefig(out_plot_file)
    plt.close(fig)




#def plot_histogram(jumpsLengthObject[0], jumpsLengthObject[1], jumpsLengthObject[0]CDF=None, sim_hist=None,
                   #TimeGap=None, SampleName=None, CellNumb=None,
                   #len_trackedPar=None, Min3Traj=None, CellLocs=None,
                   #CellFrames=None, CellJumps=None, ModelFit=None,
                   #D_free=None, D_bound=None, F_bound=None, title=None):
    #"""Function that plots an empirical histogram of jump lengths,
    #with an optional overlay of simulated/theoretical histogram of 
    #jump lengths"""

    ### Parameter parsing for text labels
    #if CellLocs != None and CellFrames != None:
        #locs_per_frame = round(CellLocs/CellFrames*1000)/1000
    #else:
        #locs_per_frame = 'na'    
    #if SampleName == None:
        #SampleName = 'na'
    #if CellNumb == None:
        #CellNumb = 'na'
    #if len_trackedPar == None:
        #len_trackedPar = 'na'
    #if Min3Traj == None:
        #Min3Traj = 'na'
    #if CellLocs == None:
        #CellLocs = 'na'
    #if CellFrames == None:
        #CellFrames = 'na'
    #if CellJumps == None:
        #CellJumps = 'na'
    #if ModelFit == None:
        #ModelFit = 'na'
    #if D_free == None:
        #D_free = 'na'
    #if D_bound == None:
        #D_bound = 'na'
    #if F_bound == None:
        #F_bound = 'na'

    ### Do something
    #jumpsLengthObject[1] = jumpsLengthObject[1]
    #scaled_y = sim_hist
    
    #histogram_spacer = 0.055
    #number = jumpsLengthObject[1].shape[0]
    #cmap = plt.get_cmap('viridis')
    #colour = [cmap(i) for i in np.linspace(0, 1, number)]

    #for i in range(jumpsLengthObject[1].shape[0]-1, -1, -1):
        #new_level = (i)*histogram_spacer
        #colour_element = colour[i] #colour[round(i/size(jumpsLengthObject[1],1)*size(colour,1)),:]
        #plt.plot(jumpsLengthObject[0], (new_level)*np.ones(jumpsLengthObject[0].shape[0]), 'k-', linewidth=1)
        #for j in range(1, jumpsLengthObject[1].shape[1]): ## Looks like we are manually building an histogram. Why so?
            #x1 = jumpsLengthObject[0][j-1]
            #x2 = jumpsLengthObject[0][j]
            #y1 = new_level
            #y2 = jumpsLengthObject[1][i,j-1]+new_level
            #plt.fill([x1, x1, x2, x2], [y1, y2, y2, y1], color=colour_element) # /!\ TODO MW: Should use different colours
        #if type(sim_hist) != type(None): ## jumpsLengthObject[0]CDF should also be provided
            #plt.plot(jumpsLengthObject[0]CDF, scaled_y[i,:]+new_level, 'k-', linewidth=2)
        #if TimeGap != None:
            #plt.text(0.6*max(jumpsLengthObject[0]), new_level+0.3*histogram_spacer, '$\Delta t$ : {} ms'.format(TimeGap*(i+1)))
        #else:
            #plt.text(0.6*max(jumpsLengthObject[0]), new_level+0.3*histogram_spacer, '${} \Delta t$'.format(i+1))

    #plt.xlim(0,jumpsLengthObject[0].max())
    #plt.ylabel('Probability')
    #plt.xlabel('jump length ($\mu m$)')
    #if type(sim_hist) != type(None):
        ##plt.title('{}; Cell number {}; Fit Type = {}; Dfree = {}; Dbound = {}; FracBound = {}, Total trajectories: {}; => Length 3 trajectories: {}, \nLocs = {}, Locs/Frame = {}; jumps: {}'
          ##.format(
              ##SampleName, CellNumb, ModelFit,
              ##D_free, D_bound, F_bound,
              ##len_trackedPar, Min3Traj, CellLocs,
              ##locs_per_frame,
              ##CellJumps))
        #plt.title(title)
    #else:
        ##plt.title('{}; Cell number {}; Total trajectories: {}; => Length 3 trajectories: {}, \nLocs = {}, Locs/Frame = {}; jumps: {}'
          ##.format(
              ##SampleName, CellNumb,
              ##len_trackedPar, Min3Traj, CellLocs,
              ##locs_per_frame,
              ##CellJumps))
        #plt.title(title)
    #plt.yticks([])
