## ==== Imports
import time
import lmfit
import numpy as np
from scipy.special import erfc
from pyspaz import spazio 
import math as math
import matplotlib.pyplot as plt


    

## Calculate distance between 2 points
def pdistmean(m):
    dists=[]
    for n in range(len(m)-1):
        dists.append(((m[n,0]-m[n+1,0])**2+(m[n,1]-m[n+1,1])**2)**0.5)
    return np.mean(dists)

def pdist3min(m):
    """Function returns the 2D distance between two points. Matrix form has:
       x1 y1
       x2 y2
       x3 y3"""
    return min( ((m[0,0]-m[1,0])**2+(m[0,1]-m[1,1])**2)**0.5 , ((m[1,0]-m[2,0])**2+(m[1,1]-m[2,1])**2)**0.5 )

def pangle(m):
    """Function returns the angle between two jumps. Matrix form has:
       x1 y1
       x2 y2
       x3 y3"""
    ab = m[1,:] - m[0,:]
    bc = m[2,:] - m[1,:]
    angle = np.math.atan2(np.linalg.det([ab,bc]),np.dot(ab,bc))
    angle_degree =math.degrees(angle)
    return angle_degree


def compute_jump_angles(
    trajs,
    min_1dt_jump_length=0.1,
    max_1dt_jump_length=3,
    maxDeltaT=4,
    GapsAllowed=1, 
    JumpsToConsider=1000
):
    """Function that takes a series of single particle trajectories and computes jump lengths for different deltaT
    
    Arguments:
    - trackedPar: an object containing the trajectories
    - useEntireTraj (bool): True if we should use all trajectories to compute the histogram. This can lead to an overestimate of the bound fraction (see paper), but useful for troubleshooting
    - maxDeltaT (int): how many jump lengths to use for the fitting: 3 timepoints, yields 2 jumps
    - GapsAllowed (int): number of missing frames that are allowed in a single trajectory
    - JumpsToConsider (int): if `UseAllTraj` is False, then use no more than 3 jumps. 
    - TimeGap (float): time between frames in milliseconds;
    - 180 (float): for PDF fitting and plotting
    - BinWidth (float): for PDF fitting and plotting
    
    Returns:
    - An array TransAngles of length <maxDeltaT> with TransAngles[DeltaT-1][0] is just a string mentioning the deltaT and TransAngles[DeltaT-1][1] is the list of all jump length for the corresponding deltaT.
    """
    
    tic = time.time() # Start the timer
    
    # Find total frames using a slight ad-hoc way: find the last frame with
    # a localization and round it. This is not an elegant solution, but it
    # works for your particle density:
    
    ## /!\ TODO MW: check this critical part of the code
    trackedPar = trajs[np.vectorize(len)(trajs[:,1])>2]
    
    TempLastFrame = np.max([np.max(i[2]) for i in trajs]) #TempLastFrame = max(trackedPar(1,end).Frame)
    CellFrames = 100*round(TempLastFrame/100)
    CellLocs = sum([len(i[2]) for i in trackedPar]) # for counting the total number of localizations
    
    print("Number of frames: {}, number of localizations: {}".format(CellFrames, CellLocs))
    
    ##
    ## ==== Compile jump lengths for each deltaT
    ##
    Min3Traj = 0   #for counting number of min3 trajectories;
    CellJumps = 0  # for counting the total number of jumps
    
    TransAngles = np.array([ "deltaT=1", [] ])
    
    for i in range(1,maxDeltaT): # Initialize TransAngles
        TransAngles = np.vstack((TransAngles, np.array([ "deltaT=" + str(i+1), [] ]) ))
    
    for i in range(len(trackedPar)): #1:length(trackedPar)
        CurrTrajLength =trackedPar[i][0].shape[0]#size(trackedPar(i).xy,1);
        
        # Loop through the trajectory. If it is a short trajectory, you 
        # need to make sure that you do not overshoot. So first figure out 
        # how many jumps you can consider.
        
        # calculate number of Dt that we can compute for this trajectory
        maxDtInTraj = min([maxDeltaT, math.floor(CurrTrajLength/2)])
        ## for each possible Dt
        for Dt in range(1,maxDtInTraj+1):
            ## how many jump of Dt can be made in thsi trajectory
            maxDtJumpInTraj=min(JumpsToConsider,CurrTrajLength-2*Dt)
            ## last first frame for this Dt
            FrameToStop = maxDtJumpInTraj-1
            ## for each starting frame
            for frame in range(0,FrameToStop): #1:HowManyFrames
                ## omly comptue angle if the average 1dt jump length is higher than min_1dt_jump_length
                if pdistmean(trackedPar[i][0][range(frame,frame+(2*Dt)),:]) >= min_1dt_jump_length:
                    if pdistmean(trackedPar[i][0][range(frame,frame+(2*Dt)),:]) <= max_1dt_jump_length: 
                        frame_Dt_3points = np.vstack((trackedPar[i][0][frame,:],trackedPar[i][0][frame+Dt,:],trackedPar[i][0][frame+(2*Dt),:]))
                        frame_Dt_3points_angle = abs(pangle(frame_Dt_3points)  )
                        TransAngles[Dt-1][1].append(frame_Dt_3points_angle)
    return(TransAngles)



def compute_jump_angles_distribution(
    TransAngles,
    maxDeltaT=7,
    BinWidth=10,
):
    """Function that takes a series of single particle trajectories and computes jump lengths for different deltaT
    
    Arguments:
    - TransAngles: an object containing the jumps length (from function compute_jump_lengths)
    - maxDeltaT (int): how many jump lengths to use for the fitting: 3 timepoints, yields 2 jumps
    - BinWidth (float): for PDF fitting and plotting
    
    Returns:
    - A List containing HistVecJumps, JumpCount, JumpProb, HistVecJumpsCDF, JumpProbCDF, JumpCountCD .
    """
    
    ## Calculate the PDF histograms (required for CDF)
    
    HistVecJumps = np.arange(0, 180+BinWidth, BinWidth)  # jump lengths in micrometers
    JumpProb = np.zeros((maxDeltaT, len(HistVecJumps))) ## second -1 added when converting to Python due to inconsistencies between the histc and histogram function
    # TODO MW: investigate those differences
    for i in range(JumpProb.shape[0]):
        JumpProb[i,:] = np.float_(
            np.histogram(TransAngles[i][1],
                         bins=np.hstack((HistVecJumps, HistVecJumps[-1])) )[0])/len(TransAngles[i][1])
    ## Calculate the jumps length count 
    JumpCount = np.zeros((maxDeltaT, len(HistVecJumps))) ## second -1 added when converting to Python due to inconsistencies between the histc and histogram function
    # TODO MW: investigate those differences
    for i in range(JumpCount.shape[0]):
        JumpCount[i,:] = np.float_(
            np.histogram(TransAngles[i][1],
                         bins=np.hstack((HistVecJumps, HistVecJumps[-1])) )[0])
    
    # Calculate the jumps length CDF
    BinWidthCDF = 1
    HistVecJumpsCDF = np.arange(0, 180+BinWidthCDF, BinWidthCDF)  # jump lengths in micrometers
    JumpProbFine = np.zeros((maxDeltaT, len(HistVecJumpsCDF)))
    for i in range(JumpProb.shape[0]):
        JumpProbFine[i,:] = np.float_(
            np.histogram(TransAngles[i][1],
                         bins=np.hstack((HistVecJumpsCDF, HistVecJumpsCDF[-1]) ))[0])/len(TransAngles[i][1])
    JumpProbCDF = np.zeros((maxDeltaT, len(HistVecJumpsCDF))) ## second -1 added when converting to Python due to inconsistencies between the histc and histogram function
    for i in range(JumpProbCDF.shape[0]): #1:size(JumpProbCDF,1)
        for j in range(2,JumpProbCDF.shape[1]+1): #=2:size(JumpProbCDF,2)
            JumpProbCDF[i,j-1] = sum(JumpProbFine[i,:j])
    
    ## Calculate the jumps length CD
    BinWidthCDF = 1
    HistVecJumpsCDF = np.arange(0, 180+BinWidthCDF, BinWidthCDF)  # jump lengths in micrometers
    JumpCountFine = np.zeros((maxDeltaT, len(HistVecJumpsCDF)))
    for i in range(JumpProb.shape[0]):
        JumpCountFine[i,:] = np.float_(
            np.histogram(TransAngles[i][1],
                         bins=np.hstack((HistVecJumpsCDF, HistVecJumpsCDF[-1]) ))[0])
    JumpCountCD = np.zeros((maxDeltaT, len(HistVecJumpsCDF))) ## second -1 added when converting to Python due to inconsistencies between the histc and histogram function
    for i in range(JumpCountCD.shape[0]): #1:size(JumpCountCD,1)
        for j in range(2,JumpCountCD.shape[1]+1): #=2:size(JumpCountCD,2)
            JumpCountCD[i,j-1] = sum(JumpCountFine[i,:j])
    
    return [HistVecJumps, JumpCount, JumpProb, HistVecJumpsCDF, JumpProbCDF, JumpCountCD]


def plot_histogram_jumpsAnglesDistrib(
    jumpsAngleObject,
    out_plot_file,
    TimeGap=None,
    BinWidth=10,
    scaleY=True
):
    """Function that plots an empirical histogram of jump lengths,
    with an optional overlay of simulated/theoretical histogram of 
    jump lengths"""
    
    histogram_spacer = 0.055
    number = jumpsAngleObject[2].shape[0]
    cmap = plt.get_cmap('viridis')
    colour = [cmap(i) for i in np.linspace(0, 1, number)]
    
    if scaleY: 
        fig, axs = plt.subplots(jumpsAngleObject[2].shape[0], figsize=(5,10), sharex=True, sharey =True)
    else :
        fig, axs = plt.subplots(jumpsAngleObject[2].shape[0], figsize=(5,10), sharex=True)
    
    # for each delta t
    for i in range(jumpsAngleObject[2].shape[0]-1, -1, -1): 
        colour_element = colour[i] #colour[round(i/size(jumpsAngleObject[2],1)*size(colour,1)),:]
        # we use plt.fill and loop through each bin to control exactly the shape of each bar
        for j in range(1, jumpsAngleObject[2].shape[1]): 
            x1 = jumpsAngleObject[0][j-1]
            x2 = jumpsAngleObject[0][j]
            y1 = 0
            y2 = jumpsAngleObject[2][i,j-1]
            axs[i].fill([x1, x1, x2, x2], [y1, y2, y2, y1], color=colour_element)
        
        axs[i].hlines(y=1/(180/BinWidth), xmin=0, xmax=180, linestyles='dashed')
        
        
        if scaleY :
            axs[i].set_ylim(0, 1.1*max([max(x) for x in jumpsAngleObject[2]]))
            midY=0.9*max([max(x) for x in jumpsAngleObject[2]])
        else: 
            ymin=0.75/(180/BinWidth)
            ymax=1.5/(180/BinWidth)
            axs[i].set_ylim(ymin, ymax)
            midY=0.9*ymax
        
        if TimeGap != None:
            axs[i].text(0.01*max(jumpsAngleObject[0]), midY, '$\Delta t$ : {} ms ({} jumps)'.format(TimeGap*(i+1), int(sum(jumpsAngleObject[1][i,:]))) )
        else:
            axs[i].text(0.01*max(jumpsAngleObject[0]), midY, '${}\Delta t$ ({} jumps)'.format(i+1, int(sum(jumpsAngleObject[1][i,:]))) )
        
        axs[i].set_xlim(0,jumpsAngleObject[0].max())
        
    
    plt.ylabel('Count')
    plt.xlabel('abs jump angle (degrees)')
    
    fig.subplots_adjust(hspace=0.1)
    fig.savefig(out_plot_file)
    plt.close(fig)












def compute_jump_angles_singledt(
    trajs,
    min_1dt_jump_length=0.2,
    max_1dt_jump_length=3,
    dt=1,
    GapsAllowed=0, 
    JumpsToConsider=1000
):
    """Function that takes a series of single particle trajectories and computes jump lengths for different deltaT
    
    Arguments:
    - trackedPar: an object containing the trajectories
    - useEntireTraj (bool): True if we should use all trajectories to compute the histogram. This can lead to an overestimate of the bound fraction (see paper), but useful for troubleshooting
    - maxDeltaT (int): how many jump lengths to use for the fitting: 3 timepoints, yields 2 jumps
    - GapsAllowed (int): number of missing frames that are allowed in a single trajectory
    - JumpsToConsider (int): if `UseAllTraj` is False, then use no more than 3 jumps. 
    - TimeGap (float): time between frames in milliseconds;
    - 180 (float): for PDF fitting and plotting
    - BinWidth (float): for PDF fitting and plotting
    
    Returns:
    - An array TransAngles of length <maxDeltaT> with TransAngles[DeltaT-1][0] is just a string mentioning the deltaT and TransAngles[DeltaT-1][1] is the list of all jump length for the corresponding deltaT.
    """
    
    tic = time.time() # Start the timer
    
    # Find total frames using a slight ad-hoc way: find the last frame with
    # a localization and round it. This is not an elegant solution, but it
    # works for your particle density:
    
    ## /!\ TODO MW: check this critical part of the code
    trackedPar = trajs[np.vectorize(len)(trajs[:,1])>2]
    
    TempLastFrame = np.max([np.max(i[2]) for i in trajs]) #TempLastFrame = max(trackedPar(1,end).Frame)
    CellFrames = 100*round(TempLastFrame/100)
    CellLocs = sum([len(i[2]) for i in trackedPar]) # for counting the total number of localizations
    
    print("Number of frames: {}, number of localizations: {}".format(CellFrames, CellLocs))
    
    ##
    ## ==== Compile jump lengths for each deltaT
    ##
    Min3Traj = 0   #for counting number of min3 trajectories;
    CellJumps = 0  # for counting the total number of jumps
    
    TransAngles = [] 
        
    for i in range(len(trackedPar)): #1:length(trackedPar)
        CurrTrajLength =trackedPar[i][0].shape[0]#size(trackedPar(i).xy,1);
        
        # Loop through the trajectory. If it is a short trajectory, you 
        # need to make sure that you do not overshoot. So first figure out 
        # how many jumps you can consider.
        
        # calculate number of dt that we can compute for this trajectory
        maxdtInTraj = min([dt, math.floor(CurrTrajLength/2)])
        ## for each possible dt
        ## how many jump of dt can be made in thsi trajectory
        maxdtJumpInTraj=min(JumpsToConsider,CurrTrajLength-2*dt)
        ## last first frame for this dt
        #FrameToStop = maxdtJumpInTraj-1
        ## for each starting frame
        for frame in range(0,maxdtJumpInTraj): #1:HowManyFrames
            ## omly comptue angle if the average 1dt jump length is higher than min_1dt_jump_length
            if pdistmean(trackedPar[i][0][range(frame,frame+(2*dt)+1),:]) >= min_1dt_jump_length:
                if pdistmean(trackedPar[i][0][range(frame,frame+(2*dt)+1),:]) <= max_1dt_jump_length: 
                    frame_dt_3points = np.vstack((trackedPar[i][0][frame,:],trackedPar[i][0][frame+dt,:],trackedPar[i][0][frame+(2*dt),:]))
                    frame_dt_3points_angle = abs(pangle(frame_dt_3points))
                    TransAngles.append(frame_dt_3points_angle)
    return(TransAngles)



def compute_jump_angles_distribution_singledt(
    TransAngles,
    dt=1,
    BinWidth=10,
):
    """Function that takes a series of single particle trajectories and computes jump lengths for different deltaT
    
    Arguments:
    - TransAngles: an object containing the jumps length (from function compute_jump_lengths)
    - BinWidth (float): for PDF fitting and plotting
    
    Returns:
    - A List containing HistVecJumps, JumpCount, JumpProb, HistVecJumpsCDF, JumpProbCDF, JumpCountCD .
    """
    
    ## Calculate the PDF histograms (required for CDF)
    
    HistVecJumps = np.arange(1, 180+BinWidth, BinWidth)  # jump lengths in micrometers
    JumpProb = np.zeros((1, len(HistVecJumps))) ## second -1 added when converting to Python due to inconsistencies between the histc and histogram function
    # TODO MW: investigate those differences
    for i in range(JumpProb.shape[0]):
        JumpProb[i,:] = np.float_(
            np.histogram(TransAngles,
                         bins=np.hstack((HistVecJumps, HistVecJumps[-1])) )[0])/len(TransAngles)
    ## Calculate the jumps length count 
    JumpCount = np.zeros((1, len(HistVecJumps))) ## second -1 added when converting to Python due to inconsistencies between the histc and histogram function
    # TODO MW: investigate those differences
    for i in range(JumpCount.shape[0]):
        JumpCount[i,:] = np.float_(
            np.histogram(TransAngles,
                         bins=np.hstack((HistVecJumps, HistVecJumps[-1])) )[0])
    
    # Calculate the jumps length CDF
    BinWidthCDF = 1
    HistVecJumpsCDF = np.arange(0, 180+BinWidthCDF, BinWidthCDF)  # jump lengths in micrometers
    JumpProbFine = np.zeros((1, len(HistVecJumpsCDF)))
    for i in range(JumpProb.shape[0]):
        JumpProbFine[i,:] = np.float_(
            np.histogram(TransAngles,
                         bins=np.hstack((HistVecJumpsCDF, HistVecJumpsCDF[-1]) ))[0])/len(TransAngles)
    JumpProbCDF = np.zeros((1, len(HistVecJumpsCDF))) ## second -1 added when converting to Python due to inconsistencies between the histc and histogram function
    for i in range(JumpProbCDF.shape[0]): #1:size(JumpProbCDF,1)
        for j in range(2,JumpProbCDF.shape[1]+1): #=2:size(JumpProbCDF,2)
            JumpProbCDF[i,j-1] = sum(JumpProbFine[i,:j])
    
    ## Calculate the jumps length CD
    BinWidthCDF = 1
    HistVecJumpsCDF = np.arange(0, 180+BinWidthCDF, BinWidthCDF)  # jump lengths in micrometers
    JumpCountFine = np.zeros((1, len(HistVecJumpsCDF)))
    for i in range(JumpProb.shape[0]):
        JumpCountFine[i,:] = np.float_(
            np.histogram(TransAngles,
                         bins=np.hstack((HistVecJumpsCDF, HistVecJumpsCDF[-1]) ))[0])
    JumpCountCD = np.zeros((1, len(HistVecJumpsCDF))) ## second -1 added when converting to Python due to inconsistencies between the histc and histogram function
    for i in range(JumpCountCD.shape[0]): #1:size(JumpCountCD,1)
        for j in range(2,JumpCountCD.shape[1]+1): #=2:size(JumpCountCD,2)
            JumpCountCD[i,j-1] = sum(JumpCountFine[i,:j])
    
    return [HistVecJumps, JumpCount, JumpProb, HistVecJumpsCDF, JumpProbCDF, JumpCountCD]


def plot_histogram_jumpsAnglesDistrib_circular(
    jumpsAngleObject,
    out_plot_file,
    TimeGap=None,
    BinWidth=10,
    dt=1,
    scaleY=True
):
    """Function that plots an empirical histogram of jump lengths,
    with an optional overlay of simulated/theoretical histogram of 
    jump lengths"""
    
    histogram_spacer = 0.055
    number = jumpsAngleObject[2].shape[0]
    cmap = plt.get_cmap('viridis')
    colour = [cmap(i) for i in np.linspace(0, 1, number)]
    
    fig = plt.figure(figsize=(5,5))
    axs = plt.subplot(111, polar=True)
    #axs = plt.subplot(111)
    # for each delta t
    i = dt-1
    colour_element = colour[i] #colour[round(i/size(jumpsAngleObject[2],1)*size(colour,1)),:]
    # we use plt.fill and loop through each bin to control exactly the shape of each bar
    barplot_x=[]
    barplot_y=[]
    for j in range(1, jumpsAngleObject[2].shape[1]): 
        barplot_x.append(np.mean([jumpsAngleObject[0][j],jumpsAngleObject[0][j-1]]))
        barplot_y.append(jumpsAngleObject[2][i,j-1])
    for j in range(1, jumpsAngleObject[2].shape[1]): 
        barplot_x.append(np.mean([180+jumpsAngleObject[0][j],180+jumpsAngleObject[0][j-1]]))
        barplot_y.append(0)
    
    axs.bar(np.linspace(0.0+2*np.pi/72, 2*np.pi+2*np.pi/72, 36, endpoint=False), barplot_y, width=2*np.pi/36, bottom=0.0)
    axs.plot(np.linspace(0, 2*np.pi, 100), np.ones(100)/(180/BinWidth), linestyle='dashed')
    axs.set_ylim(0, 0.07)
    axs.set_rlabel_position(270)
    fig.savefig(out_plot_file)
    plt.close(fig)
