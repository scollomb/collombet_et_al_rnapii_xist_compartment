analysisName="SPT_sc18c7_Polr2aHalo_Dox24h_PAJF646"
mainInDir=/home/scollomb/Lab_Heard/project_SC_XiComp/SPT/Polr2a/sc18c7/Dox24h_PAJF646
mainTmpDir=/home/scollomb/Lab_Heard/project_SC_XiComp/SPT/Polr2a/sc18c7/Dox24h_PAJF646
mainOutDir=/home/scollomb/Lab_Heard/project_SC_XiComp/SPT/Polr2a/sc18c7/Dox24h_PAJF646
env='#!/bin/bash'

cd ${mainInDir}

datasets=("191128_SPT_sc18c7_Dox24h_PAJF646_50nM_30min" "191129_SPT_sc18c7_Dox24h_PAJF646_50nM_30min" "191204_SPT_sc18c7_Dox24h_PAJF646_50nM_30min" "191205_SPT_sc18c7_Dox24h_PAJF646_50nM_30min" "191206_SPT_sc18c7_Dox24h_PAJF646_50nM_30min" "191207_SPT_sc18c7_Dox24h_PAJF646_50nM_30min")
pixelSize=0.106
timeBtwnFrame=0.005477

# for datasetName in "${datasets[@]}"; do
#     echo $datasetName
#     analysisName=$datasetName
# 
#     dataDescription=${mainInDir}/${datasetName}/ilastik/dataDescription.csv
#     maskDir=${mainInDir}/${datasetName}/ilastik/masks
#     tmpDir=${mainTmpDir}/${datasetName}/pyspaz
# 
#     mkdir -p ${tmpDir}/std
#     mkdir -p ${tmpDir}/log
#     
#     ## loop for each sample (ie position imaged)
#     for line in `cat ${dataDescription} | sed 1d | cut -f 1-2 | sort -u | sed 's/\t/#/g'` ; do 
#         array=(${line//#/ })
#         file=${array[0]}
#         sample=${array[1]}
#         outName=${sample}
#         while read var; do unset $var; done < <(( set -o posix ; set ) | grep "job.*_id*" | sed 's#=#\t#g' | cut -f 1)
# 
#         for line in `cat ${dataDescription} | awk -v sample=${sample} '{if ($2==sample) print;}' | cut -f 1-3 | sort -u | sed 's/\t/#/g'` ; do 
#             array=(${line//#/ })
#             ROE=${array[2]}
#             echo -e "---- sample" $sample "\t---- ROE" $ROE
#             outName=${sample}_${ROE}
#             ROI='Xi'
# 
#             job=Traj_ROE_any_ROI_any_ShiftMask_${outName}
#             rm ${tmpDir}/log/${job}.done
#             if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
#                 if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
#                 if [ -z ${jobTraj_ROE_any_ROI_any_assign_id} ]; then dep= ; else dep="--dependency=afterok${jobTraj_ROE_any_ROI_any_assign_id}" ; fi
#                 jobName=${job}...${outName}...${analysisName}
#                 jobTraj_ROE_any_ROI_any_ShiftMask=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 120 ${dep} \
#                     --wrap="${env}
#                         date
#                         mkdir ${tmpDir}/traj_ROE_any_ROI_any_shiftMask
#                         mkdir -p ${mainTmpDir}/analysis_shiftXi/traj_ROE_any_ROI_any_shiftMask_MSRD_meanOfShifts
#                         mkdir -p ${mainTmpDir}/analysis_shiftXi/traj_ROE_any_ROI_any_shiftMask_MSRD
#                         source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
#                         python ~/programs/SPTpy/interpolateMaskAndAssignTrajectories_moveMask.py \
#                             -i ${tmpDir}/traj_ROE_any/${sample}_${ROE}_any_Tracked.mat \
#                             --mask_ROE_0_file ${maskDir}/${sample}_BEFORE_${ROE}.tif \
#                             --mask_ROE_1_file ${maskDir}/${sample}_AFTER_${ROE}.tif \
#                             --mask_ROI_0_file ${maskDir}/${sample}_BEFORE_${ROE}_${ROI}.tif \
#                             --mask_ROI_1_file ${maskDir}/${sample}_AFTER_${ROE}_${ROI}.tif \
#                             --mask_0_frame_index 0 \
#                             --mask_1_frame_index 30000 \
#                             --olap_rule any \
#                             --maxMasksOlap 0.1 --maxMaskFracOutsideROE 0.0 --minMaskDistRoeDifFrac -0.5 --maxMaskDistRoeDifFrac 0.5 \
#                             --maxiters=500000 --maxmasks 10 \
#                             --dataset ${datasetName} --sample ${sample} --ROE ${ROE} --ROI ${ROI} \
#                             --olap_fracMin 0 --olap_fracMax 1 --pixel_subsampling_factor 1 --pixel_size_um ${pixelSize} \
#                             -o ${tmpDir}/traj_ROE_any_ROI_any_shiftMask/${sample}_${ROE}_${ROI}
#                             
#                         sed 1d ${tmpDir}/traj_ROE_any_ROI_any_shiftMask/${sample}_${ROE}_${ROI}_shifts_MSRD_meanOfShifts.csv > ${mainTmpDir}/analysis_shiftXi/traj_ROE_any_ROI_any_shiftMask_MSRD_meanOfShifts/${datasetName}_${sample}_${ROE}_${ROI}_shifts_MSRD_meanOfShifts.csv
#                         sed 1d  ${tmpDir}/traj_ROE_any_ROI_any_shiftMask/${sample}_${ROE}_${ROI}_shifts_MSRD.csv > ${mainTmpDir}/analysis_shiftXi/traj_ROE_any_ROI_any_shiftMask_MSRD/${datasetName}_${sample}_${ROE}_${ROI}_shifts_MSRD.csv
# 
#                         if [ -s ${tmpDir}/traj_ROE_any_ROI_any_shiftMask/${sample}_${ROE}_${ROI}_shifts_MSRD_meanOfShifts.csv ] ; then 
#                             echo ${job} done! > ${tmpDir}/log/${job}.done
#                         else exit 1; fi; date"`
#                 jobTraj_ROE_any_ROI_any_ShiftMask_id=`echo :${jobTraj_ROE_any_ROI_any_ShiftMask##* }`; jobTraj_ROE_any_ROI_any_ShiftMask_name=${jobName}
#                 if [ -z ${job_all_id} ]; then job_all_id=${jobTraj_ROE_any_ROI_any_ShiftMask_id}; else job_all_id=${job_all_id}${jobTraj_ROE_any_ROI_any_ShiftMask_id}; fi    ### add job id to a variable listing all job ids
#                 sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobTraj_ROE_any_ROI_any_ShiftMask_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobTraj_ROE_any_ROI_any_ShiftMask_id#:}"
#             fi
# 
#         done
#     done
# done


# ## copy all shifted files ; somehow I do not manage to make it work in a job style
# rm -r ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any
# rm -r ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_shiftMask
# mkdir ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any
# mkdir ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_shiftMask
# for datasetName in "${datasets[@]}"; do
#     tmpDir=${mainTmpDir}/${datasetName}/pyspaz
#     for Track in ${tmpDir}/traj_ROE_any_ROI_any/*_Tracked.mat ; do
#         fileName=$(basename $Track)
#         ln -s $Track  ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any/${datasetName}_${fileName}
#     done
#     for shiftedTrack in ${tmpDir}/traj_ROE_any_ROI_any_shiftMask/*_Tracked.mat ; do
#         fileName=$(basename $shiftedTrack)
#         ln -s $shiftedTrack  ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_shiftMask/${datasetName}_${fileName}
#     done
# done

# ### copy all shifted files ; somehow I do not manage to make it work in a job style
# rm -r ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_enter
# rm -r ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_enter_shiftMask
# mkdir ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_enter
# mkdir ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_enter_shiftMask
# for datasetName in "${datasets[@]}"; do
#     tmpDir=${mainTmpDir}/${datasetName}/pyspaz
#     for Track in ${tmpDir}/traj_ROE_any_ROI_enter/*_Tracked.mat ; do
#         fileName=$(basename $Track)
#         ln -s $Track  ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_enter/${datasetName}_${fileName}
#     done
#     for shiftedTrack in ${tmpDir}/traj_ROE_any_ROI_enter_shiftMask/*_Tracked.mat ; do
#         fileName=$(basename $shiftedTrack)
#         ln -s $shiftedTrack  ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_enter_shiftMask/${datasetName}_${fileName}
#     done
# done
# 

tmpDir=${mainTmpDir}/SpotOn
# # # rm -r ${tmpDir}
# mkdir -p ${tmpDir}/std
# mkdir -p ${tmpDir}/log
# 
# job=SpotOnMerged_ROE_any_ROI_any_shiftMask
# if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
#     if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
#     jobName=${job}...${analysisName}
#     jobSpotOnMerged_ROE_any_ROI_any_shiftMask=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 10 \
#         --wrap="${env}
#             date
#             mkdir -p ${mainTmpDir}/SpotOn
#             source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
#             SpotOn fit-and-plot-2states \
#                 --input_format mat --time_between_frames ${timeBtwnFrame} --gaps_allowed 0 --localisation_error 0.028 \
#                 --weight_delta_t True --model_fit CDF --max_jump_length 2 --max_jumps_per_traj 3 --max_delta_t 6 --diffusion_bound_range 0,0.02 --diffusion_free_range 0.1,20 \
#                 --y_max 0.09 --x_max=0.6 --cmap Blues \
#                 -i ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_shiftMask \
#                 -o ${tmpDir}/ROE_any_ROI_any_shiftMask
# 
#             if [ -s ${tmpDir}/ROE_any_ROI_any_shiftMask_fit_2states.pdf ] ; then 
#                 echo ${job} done! > ${tmpDir}/log/${job}.done
#             else exit 1; fi; date"`
#     jobSpotOnMerged_ROE_any_ROI_any_shiftMask_id=`echo :${jobSpotOnMerged_ROE_any_ROI_any_shiftMask##* }`; jobSpotOnMerged_ROE_any_ROI_any_shiftMask_name=${jobName}
#     if [ -z ${job_all_id} ]; then job_all_id=${jobSpotOnMerged_ROE_any_ROI_any_shiftMask_id}; else job_all_id=${job_all_id}${jobSpotOnMerged_ROE_any_ROI_any_shiftMask_id}; fi    ### add job id to a variable listing all job ids
#     sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobSpotOnMerged_ROE_any_ROI_any_shiftMask_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobSpotOnMerged_ROE_any_ROI_any_shiftMask_id#:}"
# fi
# 
# job=Merged_ROE_any_ROI_any_shiftMask_bootstrap
# if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
#     if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
#     if [ -z ${jobTraj_ROE_any_ROI_any_shiftMask_copyForMergedSpotOn_allids} ]; then dep= ; else dep="--dependency=afterok${jobTraj_ROE_any_ROI_any_shiftMask_copyForMergedSpotOn_allids}" ; fi
#     jobName=${job}...${analysisName}
#     jobMerged_ROE_any_ROI_any_shiftMask_bootstrap=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 \
#         --wrap="${env}
#             date
#             mkdir -p ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_shiftMask_bootstrap
#             source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
#             SpotOn subsample-trajs \
#                 --input_format mat --min_traj_len 2 --bootstrap_n 50 --subsampling_n 3000 --subsampling_f 0.5 \
#                 -i ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_shiftMask \
#                 -o ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_shiftMask_bootstrap/traj_ROE_any_ROI_any_shiftMask_bootstrap
# 
#             if [ -s ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_shiftMask_bootstrap/traj_ROE_any_ROI_any_shiftMask_bootstrap_sub50_Tracked.mat ] ; then 
#                 echo ${job} done! > ${tmpDir}/log/${job}.done
#             else exit 1; fi; date"`
#     jobMerged_ROE_any_ROI_any_shiftMask_bootstrap_id=`echo :${jobMerged_ROE_any_ROI_any_shiftMask_bootstrap##* }`; jobMerged_ROE_any_ROI_any_shiftMask_bootstrap_name=${jobName}
#     if [ -z ${job_all_id} ]; then job_all_id=${jobMerged_ROE_any_ROI_any_shiftMask_bootstrap_id}; else job_all_id=${job_all_id}${jobMerged_ROE_any_ROI_any_shiftMask_bootstrap_id}; fi    ### add job id to a variable listing all job ids
#     sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobMerged_ROE_any_ROI_any_shiftMask_bootstrap_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobMerged_ROE_any_ROI_any_shiftMask_bootstrap_id#:}"
# fi
# 
# for i in {1..50}; do 
#     job=SpotOnMerged_ROE_any_ROI_any_shiftMask_bootstrap_sub${i}
#     if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
#         if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
#         if [ -z ${jobMerged_ROE_any_ROI_any_shiftMask_bootstrap_id} ]; then dep= ; else dep="--dependency=afterok${jobMerged_ROE_any_ROI_any_shiftMask_bootstrap_id}" ; fi
#         jobName=${job}...${analysisName}
#         jobSpotOnMerged_ROE_any_ROI_any_shiftMask_bootstrap_sub=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep}\
#             --wrap="${env}
#                 date
#                 mkdir -p ${mainTmpDir}/SpotOn/traj_ROE_any_ROI_any_shiftMask_jumpLength_bootstrap
#                 source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
#                 SpotOn fit-2states \
#                     --input_format mat --time_between_frames ${timeBtwnFrame} --gaps_allowed 0 --localisation_error 0.028 \
#                     --weight_delta_t True --model_fit CDF --max_jump_length 2 --max_jumps_per_traj 3 --max_delta_t 6 --diffusion_bound_range 0,0.02 --diffusion_free_range 0.1,20  \
#                     -i ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_shiftMask_bootstrap/traj_ROE_any_ROI_any_shiftMask_bootstrap_sub${i}_Tracked.mat \
#                     -o ${mainTmpDir}/SpotOn/traj_ROE_any_ROI_any_shiftMask_jumpLength_bootstrap/traj_ROE_any_ROI_any_shiftMask_bootstrap_sub${i}
# 
#                 if [ -s ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_shiftMask_bootstrap/traj_ROE_any_ROI_any_shiftMask_bootstrap_sub50_fit_2states_params.csv ] ; then 
#                     echo ${job} done! > ${tmpDir}/log/${job}.done
#                 else exit 1; fi; date"`
#         jobSpotOnMerged_ROE_any_ROI_any_shiftMask_bootstrap_sub_id=`echo :${jobSpotOnMerged_ROE_any_ROI_any_shiftMask_bootstrap_sub##* }`; jobSpotOnMerged_ROE_any_ROI_any_shiftMask_bootstrap_sub_name=${jobName}
#         if [ -z ${job_all_id} ]; then job_all_id=${jobSpotOnMerged_ROE_any_ROI_any_shiftMask_bootstrap_sub_id}; else job_all_id=${job_all_id}${jobSpotOnMerged_ROE_any_ROI_any_shiftMask_bootstrap_sub_id}; fi    ### add job id to a variable listing all job ids
#         sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobSpotOnMerged_ROE_any_ROI_any_shiftMask_bootstrap_sub_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobSpotOnMerged_ROE_any_ROI_any_shiftMask_bootstrap_sub_id#:}"
#     fi
# done







    
### Analysis on pooled datasets
####################################################################################
############ Jumps Angles
####################################################################################
tmpDir=${mainTmpDir}/SpotOn
rm $tmpDir/log/*ROI_any*ngles*
rm $tmpDir/log/*ROI_any*msrd*

job=SpotOnMerged_ROE_any_ROI_any_angles_MSRDmin200
if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
    if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
    if [ -z ${jobTraj_ROE_any_ROI_anycopyForMergedSpotOn_allids} ]; then dep= ; else dep="--dependency=afterok${jobTraj_ROE_any_ROI_anycopyForMergedSpotOn_allids}" ; fi
    jobName=${job}...${analysisName}
    jobSpotOnMerged_ROE_any_ROI_anyangles_MSRDmin200=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 \
        --wrap="${env}
            date
            mkdir -p ${mainTmpDir}/SpotOn
            source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
            SpotOn plot-jumps-angle-circular \
                --input_format mat --gaps_allowed 0 --min_1dt_jump_length 0.2 --max_1dt_jump_length 3 \
                --max_jumps_per_traj 100 --delta_t 1 --bin_width 10 \
                -i ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any \
                -o ${mainTmpDir}/SpotOn/ROE_any_ROI_any_1dt_MSRDmin200

            if [ -s ${mainInDir}/SpotOn/ROE_any_ROI_any_jumpsAnglesDistrib_scaleY.pdf ] ; then 
                echo ${job} done! > ${tmpDir}/log/${job}.done
            else exit 1; fi; date"`
    jobSpotOnMerged_ROE_any_ROI_anyangles_MSRDmin200_id=`echo :${jobSpotOnMerged_ROE_any_ROI_anyangles_MSRDmin200##* }`; jobSpotOnMerged_ROE_any_ROI_anyangles_MSRDmin200_name=${jobName}
    if [ -z ${job_all_id} ]; then job_all_id=${jobSpotOnMerged_ROE_any_ROI_anyangles_MSRDmin200_id}; else job_all_id=${job_all_id}${jobSpotOnMerged_ROE_any_ROI_anyangles_MSRDmin200_id}; fi    ### add job id to a variable listing all job ids
    sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobSpotOnMerged_ROE_any_ROI_anyangles_MSRDmin200_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobSpotOnMerged_ROE_any_ROI_anyangles_MSRDmin200_id#:}"
fi

job=Merged_ROE_any_ROI_any_msrd200_bootstrap
if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
    if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
    if [ -z ${jobTraj_ROE_any_ROI_any_copyForMergedSpotOn_allids} ]; then dep= ; else dep="--dependency=afterok${jobTraj_ROE_any_ROI_any_copyForMergedSpotOn_allids}" ; fi
    jobName=${job}...${analysisName}
    jobMerged_ROE_any_ROI_any_msrd200_bootstrap=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 \
        --wrap="${env}
            date
            mkdir -p ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_msrd200_bootstrap
            source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
            SpotOn subsample-trajs \
                --input_format mat --min_traj_len 3 --min_msrd 0.2 --bootstrap_n 50 --subsampling_n 500 --subsampling_f 0.5 \
                -i ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any \
                -o ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_msrd200_bootstrap/traj_ROE_any_ROI_any_msrd200_bootstrap

            if [ -s ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_msrd200_bootstrap/traj_ROE_any_ROI_any_msrd200_bootstrap_sub50_Tracked.mat ] ; then 
                echo ${job} done! > ${tmpDir}/log/${job}.done
            else exit 1; fi; date"`
    jobMerged_ROE_any_ROI_any_msrd200_bootstrap_id=`echo :${jobMerged_ROE_any_ROI_any_msrd200_bootstrap##* }`; jobMerged_ROE_any_ROI_any_msrd200_bootstrap_name=${jobName}
    if [ -z ${job_all_id} ]; then job_all_id=${jobMerged_ROE_any_ROI_any_msrd200_bootstrap_id}; else job_all_id=${job_all_id}${jobMerged_ROE_any_ROI_any_msrd200_bootstrap_id}; fi    ### add job id to a variable listing all job ids
    sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobMerged_ROE_any_ROI_any_msrd200_bootstrap_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobMerged_ROE_any_ROI_any_msrd200_bootstrap_id#:}"
fi

for i in {1..50}; do 
    job=Merged_ROE_any_ROI_any_msrd200_angles_MSRDmin200_bootstrap_sub${i}
    if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
        if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
        if [ -z ${jobMerged_ROE_any_ROI_any_msrd200_bootstrap_id} ]; then dep= ; else dep="--dependency=afterok${jobMerged_ROE_any_ROI_any_msrd200_bootstrap_id}" ; fi
        jobName=${job}...${analysisName}
        jobMerged_ROE_any_ROI_any_msrd200_angles_MSRDmin200_bootstrap_sub=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep}\
            --wrap="${env}
                date
                mkdir -p ${mainTmpDir}/SpotOn/traj_ROE_any_ROI_any_msrd200_angles_bootstrap
                source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
                SpotOn jumps-angle-circular-countonly \
                    --input_format mat --gaps_allowed 0 --min_1dt_jump_length 0.2 --max_1dt_jump_length 3 \
                    --max_jumps_per_traj 100 --delta_t 1 --bin_width 10 \
                    -i ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_msrd200_bootstrap/traj_ROE_any_ROI_any_msrd200_bootstrap_sub${i}_Tracked.mat \
                    -o ${mainTmpDir}/SpotOn/traj_ROE_any_ROI_any_msrd200_angles_bootstrap/traj_ROE_any_ROI_any_msrd200_bootstrap_sub${i}

                if [ -s ${mainInDir}/SpotOn/traj_ROE_any_ROI_any_msrd200_angles_bootstrap/traj_ROE_any_ROI_any_msrd200_bootstrap_sub${i}_jumpsAngles.csv ] ; then 
                    echo ${job} done! > ${tmpDir}/log/${job}.done
                else exit 1; fi; date"`
        jobMerged_ROE_any_ROI_any_msrd200_angles_MSRDmin200_bootstrap_sub_id=`echo :${jobMerged_ROE_any_ROI_any_msrd200_angles_MSRDmin200_bootstrap_sub##* }`; jobMerged_ROE_any_ROI_any_msrd200_angles_MSRDmin200_bootstrap_sub_name=${jobName}
        if [ -z ${job_all_id} ]; then job_all_id=${jobMerged_ROE_any_ROI_any_msrd200_angles_MSRDmin200_bootstrap_sub_id}; else job_all_id=${job_all_id}${jobMerged_ROE_any_ROI_any_msrd200_angles_MSRDmin200_bootstrap_sub_id}; fi    ### add job id to a variable listing all job ids
        sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobMerged_ROE_any_ROI_any_msrd200_angles_MSRDmin200_bootstrap_sub_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobMerged_ROE_any_ROI_any_msrd200_angles_MSRDmin200_bootstrap_sub_id#:}"
    fi
done


job=SpotOnMerged_ROE_any_ROI_any_shiftMask_angles_MSRDmin200
if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
    if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
    if [ -z ${jobTraj_ROE_any_ROI_any_shiftMaskcopyForMergedSpotOn_allids} ]; then dep= ; else dep="--dependency=afterok${jobTraj_ROE_any_ROI_any_shiftMaskcopyForMergedSpotOn_allids}" ; fi
    jobName=${job}...${analysisName}
    jobSpotOnMerged_ROE_any_ROI_any_shiftMaskangles_MSRDmin200=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 \
        --wrap="${env}
            date
            mkdir -p ${mainTmpDir}/SpotOn
            source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
            SpotOn plot-jumps-angle-circular \
                --input_format mat --gaps_allowed 0 --min_1dt_jump_length 0.2 --max_1dt_jump_length 3 \
                --max_jumps_per_traj 100 --delta_t 1 --bin_width 10 \
                -i ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_shiftMask \
                -o ${mainTmpDir}/SpotOn/ROE_any_ROI_any_shiftMask_1dt_MSRDmin200

            if [ -s ${mainInDir}/SpotOn/ROE_any_ROI_any_shiftMask_jumpsAnglesDistrib_scaleY.pdf ] ; then 
                echo ${job} done! > ${tmpDir}/log/${job}.done
            else exit 1; fi; date"`
    jobSpotOnMerged_ROE_any_ROI_any_shiftMaskangles_MSRDmin200_id=`echo :${jobSpotOnMerged_ROE_any_ROI_any_shiftMaskangles_MSRDmin200##* }`; jobSpotOnMerged_ROE_any_ROI_any_shiftMaskangles_MSRDmin200_name=${jobName}
    if [ -z ${job_all_id} ]; then job_all_id=${jobSpotOnMerged_ROE_any_ROI_any_shiftMaskangles_MSRDmin200_id}; else job_all_id=${job_all_id}${jobSpotOnMerged_ROE_any_ROI_any_shiftMaskangles_MSRDmin200_id}; fi    ### add job id to a variable listing all job ids
    sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobSpotOnMerged_ROE_any_ROI_any_shiftMaskangles_MSRDmin200_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobSpotOnMerged_ROE_any_ROI_any_shiftMaskangles_MSRDmin200_id#:}"
fi

job=Merged_ROE_any_ROI_any_shiftmask_msrd200_bootstrap
if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
    if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
    if [ -z ${jobTraj_ROE_any_ROI_any_shiftMask_copyForMergedSpotOn_allids} ]; then dep= ; else dep="--dependency=afterok${jobTraj_ROE_any_ROI_any_shiftMask_copyForMergedSpotOn_allids}" ; fi
    jobName=${job}...${analysisName}
    jobMerged_ROE_any_ROI_any_shiftmask_msrd200_bootstrap=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 \
        --wrap="${env}
            date
            mkdir -p ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_shiftmask_msrd200_bootstrap
            source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
            SpotOn subsample-trajs \
                --input_format mat --min_traj_len 3 --min_msrd 0.2 --bootstrap_n 50 --subsampling_n 500 --subsampling_f 0.5 \
                -i ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_shiftMask \
                -o ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_shiftmask_msrd200_bootstrap/traj_ROE_any_ROI_any_shiftmask_msrd200_bootstrap

            if [ -s ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_shiftmask_msrd200_bootstrap/traj_ROE_any_ROI_any_shiftmask_msrd200_bootstrap_sub50_Tracked.mat ] ; then 
                echo ${job} done! > ${tmpDir}/log/${job}.done
            else exit 1; fi; date"`
    jobMerged_ROE_any_ROI_any_shiftmask_msrd200_bootstrap_id=`echo :${jobMerged_ROE_any_ROI_any_shiftmask_msrd200_bootstrap##* }`; jobMerged_ROE_any_ROI_any_shiftmask_msrd200_bootstrap_name=${jobName}
    if [ -z ${job_all_id} ]; then job_all_id=${jobMerged_ROE_any_ROI_any_shiftmask_msrd200_bootstrap_id}; else job_all_id=${job_all_id}${jobMerged_ROE_any_ROI_any_shiftmask_msrd200_bootstrap_id}; fi    ### add job id to a variable listing all job ids
    sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobMerged_ROE_any_ROI_any_shiftmask_msrd200_bootstrap_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobMerged_ROE_any_ROI_any_shiftmask_msrd200_bootstrap_id#:}"
fi


for i in {1..50}; do 
    job=Merged_ROE_any_ROI_any_shiftmask_msrd200_angles_MSRDmin200_bootstrap_sub${i}
    if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
        if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
        if [ -z ${jobMerged_ROE_any_ROI_any_shiftmask_msrd200_bootstrap_id} ]; then dep= ; else dep="--dependency=afterok${jobMerged_ROE_any_ROI_any_shiftmask_msrd200_bootstrap_id}" ; fi
        jobName=${job}...${analysisName}
        jobMerged_ROE_any_ROI_any_shiftmask_msrd200_angles_MSRDmin200_bootstrap_sub=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 20 ${dep}\
            --wrap="${env}
                date
                mkdir -p ${mainTmpDir}/SpotOn/traj_ROE_any_ROI_any_shiftmask_msrd200_angles_bootstrap
                source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_SPT_SAM
                SpotOn jumps-angle-circular-countonly \
                    --input_format mat --gaps_allowed 0 --min_1dt_jump_length 0.2 --max_1dt_jump_length 3 \
                    --max_jumps_per_traj 100 --delta_t 1 --bin_width 10 \
                    -i ${mainTmpDir}/Pool_datasets/allTraj/traj_ROE_any_ROI_any_shiftmask_msrd200_bootstrap/traj_ROE_any_ROI_any_shiftmask_msrd200_bootstrap_sub${i}_Tracked.mat \
                    -o ${mainTmpDir}/SpotOn/traj_ROE_any_ROI_any_shiftmask_msrd200_angles_bootstrap/traj_ROE_any_ROI_any_shiftmask_msrd200_bootstrap_sub${i}

                if [ -s ${mainInDir}/SpotOn/traj_ROE_any_ROI_any_shiftmask_msrd200_angles_bootstrap/traj_ROE_any_ROI_any_shiftmask_msrd200_bootstrap_sub${i}_jumpsAngles.csv ] ; then 
                    echo ${job} done! > ${tmpDir}/log/${job}.done
                else exit 1; fi; date"`
        jobMerged_ROE_any_ROI_any_shiftmask_msrd200_angles_MSRDmin200_bootstrap_sub_id=`echo :${jobMerged_ROE_any_ROI_any_shiftmask_msrd200_angles_MSRDmin200_bootstrap_sub##* }`; jobMerged_ROE_any_ROI_any_shiftmask_msrd200_angles_MSRDmin200_bootstrap_sub_name=${jobName}
        if [ -z ${job_all_id} ]; then job_all_id=${jobMerged_ROE_any_ROI_any_shiftmask_msrd200_angles_MSRDmin200_bootstrap_sub_id}; else job_all_id=${job_all_id}${jobMerged_ROE_any_ROI_any_shiftmask_msrd200_angles_MSRDmin200_bootstrap_sub_id}; fi    ### add job id to a variable listing all job ids
        sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobMerged_ROE_any_ROI_any_shiftmask_msrd200_angles_MSRDmin200_bootstrap_sub_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobMerged_ROE_any_ROI_any_shiftmask_msrd200_angles_MSRDmin200_bootstrap_sub_id#:}"
    fi
done




# 
# ### cat all results tables
# echo -e 'datasetName\tsample\tROE\tROI\tshiftN\tall\tMSRDinf100nm\tMSRD100to200nm\tMSRDsup200nm\tshiftall\tshiftMSRDinf100nm\tshiftMSRD100to200nm\tshiftMSRDsup200nm' > ${mainTmpDir}/analysis_shiftXi/traj_ROE_any_ROI_enter_shiftMask_MSRD_meanOfShifts_all.tsv
# cat ${mainTmpDir}/analysis_shiftXi/traj_ROE_any_ROI_enter_shiftMask_MSRD_meanOfShifts/* >> ${mainTmpDir}/analysis_shiftXi/traj_ROE_any_ROI_enter_shiftMask_MSRD_meanOfShifts_all.tsv
# echo -e 'datasetName\tsample\tROE\tROI\tID\tall\tMSRDinf100nm\tMSRD100to200nm\tMSRDsup200nm' > ${mainTmpDir}/analysis_shiftXi/traj_ROE_any_ROI_enter_shiftMask_MSRD_all.tsv
# cat ${mainTmpDir}/analysis_shiftXi/traj_ROE_any_ROI_enter_shiftMask_MSRD/* >> ${mainTmpDir}/analysis_shiftXi/traj_ROE_any_ROI_enter_shiftMask_MSRD_all.tsv

