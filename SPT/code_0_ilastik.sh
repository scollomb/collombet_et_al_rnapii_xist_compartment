analysisName="SPT_sc18c7_Polr2aHalo_Dox24h_PAJF646"
mainInDir=/home/scollomb/Lab_Heard/project_SC_XiComp/SPT/Polr2a/sc18c7/Dox24h_PAJF646
mainTmpDir=/home/scollomb/Lab_Heard/project_SC_XiComp/SPT/Polr2a/sc18c7/Dox24h_PAJF646
mainOutDir=/home/scollomb/Lab_Heard/project_SC_XiComp/SPT/Polr2a/sc18c7/Dox24h_PAJF646
env='#!/bin/bash'

cd ${mainInDir}

datasets=("191128_SPT_sc18c7_Dox24h_PAJF646_50nM_30min" "191129_SPT_sc18c7_Dox24h_PAJF646_50nM_30min" "191204_SPT_sc18c7_Dox24h_PAJF646_50nM_30min" "191205_SPT_sc18c7_Dox24h_PAJF646_50nM_30min" "191206_SPT_sc18c7_Dox24h_PAJF646_50nM_30min" "191207_SPT_sc18c7_Dox24h_PAJF646_50nM_30min")
pixelSize=0.106
timeBtwnFrame=0.005477

## combine snapshot before and after SPT in one TIFF for ilastik
# source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_IA
# for datasetName in "${datasets[@]}"; do
#     echo $datasetName
#     snapDir=${mainInDir}/${datasetName}/snapshots
#     snapDirNew=${mainInDir}/${datasetName}/snapshots_combAsTime
#     rm $snapDirNew/*
#     
#     mkdir $snapDirNew
#     for filePath in ${snapDir}/*Xist*BEFORE*; do
#         fileBefore=$(basename "$filePath")
#         fileAfter=$(sed 's|BEFORE|AFTER|g' <<< $fileBefore)
#         file2=$(sed 's|.*_pos|pos|g' <<< $fileBefore)
#         outName=$(sed 's|.tif||g' <<< $file2)
#         ls ${snapDir}/$fileBefore 
#         ls ${snapDir}/$fileAfter
#         python /home/scollomb/programs/ImageAnalysis/tif_openAndResave.py tifcombinetwoastimeseries \
#             -i1 ${snapDir}/$fileBefore -i2 ${snapDir}/$fileAfter \
#             -o ${snapDirNew}/${outName}.tif
#     done
# done

for datasetName in "${datasets[@]}"; do
    echo $datasetName
    tmpDir=${mainInDir}/${datasetName}/ilastik
    snapDir=${mainInDir}/${datasetName}/snapshots_combAsTime

#     rm -r $tmpDir
    rm ${tmpDir}/dataDescription.csv

    mkdir -p $tmpDir/std
    mkdir -p $tmpDir/log
    
    for filePath in ${snapDir}/* ; do
        file=$(basename "$filePath") 
        outName=$(sed 's|.tif||g' <<< $file)
        echo $outName
        
        job=Ilastik_nuc_proba_${outName}
        if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
            if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
            if [ -z ${jobSplitChannels_1n3_id} ]; then dep= ; else dep="--dependency=afterok${jobSplitChannels_1n3_id}" ; fi
            jobName=${job}...${outName}...${analysisName}
            jobIlastik_nuc_proba=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 15 ${dep} \
                --wrap="${env}
                    date
                    mkdir -p ${tmpDir}/proba_nuc
                    source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_IA
                    /home/scollomb/programs/ilastik-1.3.3post3-Linux/run_ilastik.sh --headless \
                        --readonly --input_axes=tyx \
                        --output_format='hdf5' --export_source 'Probabilities Stage 2' --output_axis_order=tcyx --export_dtype='float32' \
                        --project=${mainInDir}/ilastik/ilastik_sc18c7_maskXist_time_proba_nuc.ilp \
                        --output_filename_format=${tmpDir}/proba_nuc/{nickname}_ilastik_nuc_proba.h5 \
                        ${snapDir}/${outName}.tif
                    
                    if [ -s ${tmpDir}/proba_nuc/${outName}_ilastik_nuc_proba.h5 ] ; then 
                        echo ${job} done! > ${tmpDir}/log/${job}.done
                    else exit 1; fi; date"`
            jobIlastik_nuc_proba_id=`echo :${jobIlastik_nuc_proba##* }`; jobIlastik_nuc_proba_name=${jobName}
            if [ -z ${job_all_id} ]; then job_all_id=${jobIlastik_nuc_proba_id}; else job_all_id=${job_all_id}${jobIlastik_nuc_proba_id}; fi    ### add job id to a variable listing all job ids
            sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobIlastik_nuc_proba_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobIlastik_nuc_proba_id#:}"
        fi

        job=Ilastik_nucXi_proba_${outName}
        if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
            if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
            if [ -z ${jobSplitChannels_1n3_id} ]; then dep= ; else dep="--dependency=afterok${jobSplitChannels_1n3_id}" ; fi
            jobName=${job}...${outName}...${analysisName}
            jobIlastik_nucXi_proba=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 15 ${dep} \
                --wrap="${env}
                    date
                    mkdir -p ${tmpDir}/proba_nucXi
                    source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_IA
                    /home/scollomb/programs/ilastik-1.3.3post3-Linux/run_ilastik.sh --headless \
                        --readonly --input_axes=tyx \
                        --output_format='hdf5' --export_source 'Probabilities Stage 2' --output_axis_order=tcyx --export_dtype='float32' \
                        --project=${mainInDir}/ilastik/ilastik_sc18c7_maskXist_time_proba_nucXi.ilp \
                        --output_filename_format=${tmpDir}/proba_nucXi/{nickname}_ilastik_nucXi_proba.h5 \
                        ${snapDir}/${outName}.tif
                    
                    if [ -s ${tmpDir}/proba_nucXi/${outName}_ilastik_nucXi_proba.h5 ] ; then 
                        echo ${job} done! > ${tmpDir}/log/${job}.done
                    else exit 1; fi; date"`
            jobIlastik_nucXi_proba_id=`echo :${jobIlastik_nucXi_proba##* }`; jobIlastik_nucXi_proba_name=${jobName}
            if [ -z ${job_all_id} ]; then job_all_id=${jobIlastik_nucXi_proba_id}; else job_all_id=${job_all_id}${jobIlastik_nucXi_proba_id}; fi    ### add job id to a variable listing all job ids
            sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobIlastik_nucXi_proba_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobIlastik_nucXi_proba_id#:}"
        fi

        job=Ilastik_nuc_obj_${outName}
        if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
            if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
            if [ -z ${jobIlastik_nuc_proba_id} ]; then dep= ; else dep="--dependency=afterok${jobIlastik_nuc_proba_id}" ; fi
            jobName=${job}...${outName}...${analysisName}
            jobIlastik_nuc_obj=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 15 ${dep} \
                --wrap="${env}
                    date
                    mkdir -p ${tmpDir}/obj_nuc
                    source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_IA
                    /home/scollomb/programs/ilastik-1.3.3post3-Linux/run_ilastik.sh --headless \
                        --readonly --project=${mainInDir}/ilastik/ilastik_sc18c7_maskXist_time_track_nuc.ilp \
                        --output_filename_format=${tmpDir}/obj_nuc/{nickname}_ilastik_nuc_obj.h5 \
                        --input_axes=tyx \
                        --output_format='hdf5' --output_axis_order=tcyx --export_dtype='float32' \
                        --raw_data=${snapDir}/${outName}.tif \
                        --prediction_maps=${tmpDir}/proba_nuc/${outName}_ilastik_nuc_proba.h5 
                    
                    if [ -s ${tmpDir}/obj_nuc/${outName}_ilastik_nuc_obj.h5 ] ; then 
                        echo ${job} done! > ${tmpDir}/log/${job}.done
                    else exit 1; fi; date"`
            jobIlastik_nuc_obj_id=`echo :${jobIlastik_nuc_obj##* }`; jobIlastik_nuc_obj_name=${jobName}
            if [ -z ${job_all_id} ]; then job_all_id=${jobIlastik_nuc_obj_id}; else job_all_id=${job_all_id}${jobIlastik_nuc_obj_id}; fi    ### add job id to a variable listing all job ids
            sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobIlastik_nuc_obj_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobIlastik_nuc_obj_id#:}"
        fi

        job=Ilastik_nucXi_obj_${outName}
        if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
            if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
            if [ -z ${jobIlastik_nucXi_proba_id} ]; then dep= ; else dep="--dependency=afterok${jobIlastik_nucXi_proba_id}" ; fi
            jobName=${job}...${outName}...${analysisName}
            jobIlastik_nucXi_obj=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 15 ${dep} \
                --wrap="${env}
                    date
                    mkdir -p ${tmpDir}/obj_nucXi
                    source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_IA
                    /home/scollomb/programs/ilastik-1.3.3post3-Linux/run_ilastik.sh --headless \
                        --readonly --project=${mainInDir}/ilastik/ilastik_sc18c7_maskXist_time_track_nucXi.ilp \
                        --output_filename_format=${tmpDir}/obj_nucXi/{nickname}_ilastik_nucXi_obj.h5 \
                        --input_axes=tyx \
                        --output_format='hdf5' --output_axis_order=tcyx --export_dtype='float32' \
                        --raw_data=${snapDir}/${outName}.tif \
                        --prediction_maps=${tmpDir}/proba_nucXi/${outName}_ilastik_nucXi_proba.h5 

                    if [ -s ${tmpDir}/obj_nucXi/${outName}_ilastik_nucXi_obj.h5 ] ; then 
                        echo ${job} done! > ${tmpDir}/log/${job}.done
                    else exit 1; fi; date"`
            jobIlastik_nucXi_obj_id=`echo :${jobIlastik_nucXi_obj##* }`; jobIlastik_nucXi_obj_name=${jobName}
            if [ -z ${job_all_id} ]; then job_all_id=${jobIlastik_nucXi_obj_id}; else job_all_id=${job_all_id}${jobIlastik_nucXi_obj_id}; fi    ### add job id to a variable listing all job ids
            sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobIlastik_nucXi_obj_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobIlastik_nucXi_obj_id#:}"
        fi
        
        job=Ilastik_extractMasks_${outName}
        if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
            if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
            if [ -z ${jobIlastik_nuc_obj_id} ] || [ -z ${jobIlastik_nucXi_obj_id} ] ; then dep= ; else dep="--dependency=afterok${jobIlastik_nuc_obj_id}${jobIlastik_nucXi_obj_id}" ; fi
            jobName=${job}...${outName}...${analysisName}
            jobIlastik_extractMasks=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 15 ${dep} \
                --wrap="${env}
                    date
                    mkdir -p ${tmpDir}/masks
                    rm ${tmpDir}/masks/${outName}.csv
                    source /g/heard/scollomb/miniconda2/bin/activate ~/programs/condaEnv/py3.6_IA
                    python /home/scollomb/Lab_Heard/project_SC_XiComp/SPT/Polr2a/sc18c7/Dox24h_PAJF646/code_0.1_ilastik_extractNucXi.py ilastik-filter-nucxi \
                        --input_ilastik_nuc_obj ${tmpDir}/obj_nuc/${outName}_ilastik_nuc_obj.h5 \
                        --input_ilastik_xi_obj ${tmpDir}/obj_nucXi/${outName}_ilastik_nucXi_obj.h5 \
                        --dataset $datasetName --sample $outName \
                        -o ${tmpDir}/masks/$outName

                    if [ -s ${tmpDir}/masks/${outName}.csv ] ; then 
                        echo ${job} done! > ${tmpDir}/log/${job}.done
                    else exit 1; fi; date"`
            jobIlastik_extractMasks_id=`echo :${jobIlastik_extractMasks##* }`; jobIlastik_extractMasks_name=${jobName}
            if [ -z ${job_all_id} ]; then job_all_id=${jobIlastik_extractMasks_id}; else job_all_id=${job_all_id}${jobIlastik_extractMasks_id}; fi    ### add job id to a variable listing all job ids
            sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobIlastik_extractMasks_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobIlastik_extractMasks_id#:}"
        fi

        job=Ilastik_catcsv_${outName}
        if [ -s ${tmpDir}/log/${job}.done ] || [ -s ${finalOutDir}/log/${job}.done ] ; then echo ${job} already done! ; else
            if [ -s ${tmpDir}/std/${job}.std ]; then rm ${tmpDir}/std/${job}.std ;fi
            if [ -z ${jobIlastik_extractMasks_id} ] ; then dep= ; else dep="--dependency=afterok${jobIlastik_extractMasks_id}" ; fi
            jobName=${job}...${outName}...${analysisName}
            jobIlastik_catcsv=`sbatch -J ${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes -c 1 --mem 6000 -t 15 ${dep} \
                --wrap="${env}
                    date
                    cat ${tmpDir}/masks/${outName}.csv >> ${tmpDir}/dataDescription.csv

                    echo ${job} done! > ${tmpDir}/log/${job}.done
                    date"`
            jobIlastik_catcsv_id=`echo :${jobIlastik_catcsv##* }`; jobIlastik_catcsv_name=${jobName}
            if [ -z ${job_all_id} ]; then job_all_id=${jobIlastik_catcsv_id}; else job_all_id=${job_all_id}${jobIlastik_catcsv_id}; fi    ### add job id to a variable listing all job ids
            sbatch -J stat_${jobName} -o ${tmpDir}/std/${job}.std -e ${tmpDir}/std/${job}.std --kill-on-invalid-dep=yes --open-mode=append -c 1 --mem 10 -t 1 --dependency=afterok${jobIlastik_catcsv_id} --wrap="sacct -o reqmem,maxrss,averss,elapsed  -j ${jobIlastik_catcsv_id#:}"
        fi
    done
done

